import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import DrawerNavigator from './src/navigation/DrawerNavigator';
import { CompassProvider } from './src/context/BlogContext';
import * as Font from 'expo-font';
import { PacmanIndicator } from 'react-native-indicators';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import * as Permissions  from 'expo-permissions';
import * as Location from 'expo-location';
import * as TaskManager from 'expo-task-manager';

// global.niz;

const LOCATION_TASK_NAME = 'background-location-task'

TaskManager.defineTask(LOCATION_TASK_NAME, ({ data, error }) => {
  if (error) {
    // debugger

    // Error occurred - check `error.message` for more details.
    return;
  }
  if (data) {
    const { locations } = data;
    // debugger
    // global.niz = data;
    
    // console.log({ locations })
    // do something with the locations captured in the background
  }
  
})



export default function App() {
  const[fontLoaded, setFontLoaded] = useState(false)
    // console.log('aa',niz)

    useEffect(() => {
        _loadFont()
        _getLocationAsync()
        // console.log('task manager',TaskManager)

    },[])

    const _getLocationAsync = async () => {
      let isRegistred = await TaskManager.isTaskRegisteredAsync(LOCATION_TASK_NAME)
      console.log(isRegistred)
        if (!isRegistred) {
          await Location.startLocationUpdatesAsync(LOCATION_TASK_NAME, {
          accuracy: Location.Accuracy.BestForNavigation,
          timeInterval: 5000,
          distanceInterval: 1
        });
    };
  }
  

    _loadFont = async() => {
        await Font.loadAsync({
            'Myriad-Pro-Bold-Condensed': require('./assets/fonts/Myriad-Pro-Bold-Condensed.ttf'),
            'Myriad-Pro-Condensed' : require('./assets/fonts/Myriad-Pro-Condensed.ttf'),
            'credit' : require('./assets/fonts/CREDC___.ttf')
        });            
        setFontLoaded(true);
        } 
  return (
    <SafeAreaProvider>
      <CompassProvider>
        {fontLoaded
        ?<DrawerNavigator />
        :<PacmanIndicator size={40} color="#e9ce00"  style={{justifyContent:'center'}}/>
        }
      </CompassProvider>
      </SafeAreaProvider>
  );
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
