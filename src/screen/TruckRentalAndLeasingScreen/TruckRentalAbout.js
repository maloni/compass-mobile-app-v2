import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, SafeAreaView, ScrollView,Platform, Linking} from 'react-native';
import  TRLlogo  from '../../components/TRLlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import LoveCompass from '../../components/LoveCompass';
import {Feather, MaterialIcons} from '@expo/vector-icons';

const TruckRentalAbout = ({ navigation }) => {


    return(
    <SafeAreaView style={{width:'100%', height:'100%'}}>
    <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
        <ScrollView      
            contentContainerStyle={styles.container}           
            automaticallyAdjustContentInsets={true}
            showsVerticalScrollIndicator={false}
            bounces={true}
        >
                <TRLlogo />
                <Text style={{...styles.txt, marginTop:hp('3%')}}>Compass Truck Rental & Leasing is your partner in transportation. We offer flexible truck rental & leasing solutions that cater around your needs. We work with both start up and established companies. At Compass we know trucking, and we can help you navigate through the many challenges it takes to run a successful business</Text>
            <View style={{flexDirection:'row', justifyContent:'space-between', marginTop:hp('5%')}}>
                <TouchableOpacity
                style={{width:wp('30%'), alignItems:'center', marginHorizontal:wp('5%')}}
                onPress={() => Linking.openURL('tel:+1-630-286-7240')}
                >
                    <Feather 
                    name='phone-call'
                    color='#da212f'
                    size={wp('10%')}
                    />
                    <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:wp('7%'), color:'#484848'}}>Call Us</Text>
                </TouchableOpacity>
                <TouchableOpacity
                style={{width:wp('30%'), alignItems:'center',  marginHorizontal:wp('5%')}}
                onPress={() => Linking.openURL('mailto:info@compasstruckrental.com?subject=mail from CTRL app')}
                >
                    <MaterialIcons 
                    name='mail-outline'
                    color='#da212f'
                    size={wp('10%')}
                    />
                    <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:wp('7%'), color:'#484848'}}>Email Us!</Text>
                </TouchableOpacity>
            </View>
                <View style={{justifyContent:'flex-end', alignSelf:'center', width:'80%', marginTop:hp('12%'), position:'absolute', bottom:hp('4%') }}>
                    <LoveCompass />
                </View>
                    
            </ScrollView>
    </KeyboardAwareView>
    </SafeAreaView>
    )
};



const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        paddingBottom: wp('7%'),
        height:'100%'
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center',
        padding:hp('2%'),
        textAlign:'justify'
    },
    btn:{
        marginVertical:hp('1%'),
    width:'80%',
    alignSelf:'center',
    borderWidth:3, 
    borderColor:'#da212f', 
    borderRadius:10,
    backgroundColor:'white',
    shadowOffset: {
      width: 10,
      height: 6,
    },
    shadowOpacity: 0.30,
    shadowRadius: 12,
    shadowColor:'#484848',
    elevation: 8,
    
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default TruckRentalAbout;