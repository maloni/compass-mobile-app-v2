import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, SafeAreaView, ScrollView, Dimensions, TouchableOpacity, Linking, TextInput, KeyboardAvoidingView } from 'react-native';
import CompassInsuranceLogo from '../../components/CompassInsuranceLogo';
import Constants from 'expo-constants';
import InsuranceInput from '../../components/InsuranceInput';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import validator from 'validator';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import TRLlogo from '../../components/TRLlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const TruckRentalLease = () => {
    const [companyName, setCompanyName] = useState('');
    const [mcNumber, setMcNumber] = useState('');
    const [state, setState] = useState('');
    const [yearsInBusiness, setYearsInBusiness] = useState('');
    const [contactPerson, setContactPerson] = useState('');
    const [businessPhone, setBusinessPhone] = useState('');
    const [email, setEmail] = useState('');
    const [checkMail, setCheckMail] = useState('');

    var content ="Company Name : " + companyName + "<br/> MC Number : " + mcNumber + "<br/> State : " + state + "<br/> How long in Business : " + yearsInBusiness + "<br/> Contact Person : " + contactPerson + "<br/> Business Phone : " + businessPhone + "<br/> Email : " + email;

    return(
      <SafeAreaView style={{width:'100%', height:'100%'}}>
      <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
          <ScrollView
          contentContainerStyle={styles.container}           
          automaticallyAdjustContentInsets={true}
          showsVerticalScrollIndicator={false}
          bounces={true}
          >
              <TRLlogo />
              <Text style={{...styles.text, fontSize:wp('5%')}}>Why Lease with Compass?</Text>
              <Text style={styles.text}>
              &#10004; Service locations around the country {"\n"}
              &#10004; Better fit for small and medium companies {"\n"}
              &#10004; Shorter lease terms {"\n"}
              &#10004; Reasonable return conditions {"\n"}
              &#10004; Use only virgin brand tires {"\n"}
              &#10004; Fast responding billing team {"\n"}
              &#10004; No CPI related price increase {"\n"}
              &#10004; Fixed operating costs {"\n"}
              </Text>
              <Text style={{...styles.text, fontSize:wp('5%'), marginBottom:hp('2%')}}>Contact us about leasing a truck</Text>
              <InsuranceInput 
              onChangeText={(Text) => setCompanyName(Text)}
              value={companyName}
              placeholder='Company Name'
              icon='office-building'
              color='#e30035'
              validate={false}
              />
              <InsuranceInput 
              onChangeText={(Text) => setMcNumber(Text)}
              value={mcNumber}
              placeholder='MC Number'
              icon='truck'
              color='#e30035'
              validate={false}
              />
              <InsuranceInput 
              onChangeText={(Text) => setState(Text)}
              value={state}
              placeholder='State'
              icon='flag-variant'
              color='#e30035'
              validate={false}
              />
              <InsuranceInput 
              onChangeText={(Text) => setYearsInBusiness(Text)}
              value={yearsInBusiness}
              placeholder='Years in Business'
              icon='calendar-question'
              color='#e30035'
              validate={false}
              />
              <InsuranceInput 
              onChangeText={(Text) => setContactPerson(Text)}
              value={contactPerson}
              placeholder='Contact Person'
              icon='account-circle'
              color='#e30035'
              validate={false}
              />
              <InsuranceInput 
              onChangeText={(Text) => setBusinessPhone(Text)}
              value={businessPhone}
              placeholder='Business Phone'
              icon='phone'
              color='#e30035'
              validate={false}
              />
              <InsuranceInput 
              onChangeText={(Text) => setEmail(Text)}
              value={email}
              placeholder='Email'
              icon='email'
              color='#e30035'
              validate={false}
              />
              <TouchableOpacity
              style={{backgroundColor:'#e30035', width:wp('40%'), alignSelf:'center', borderRadius:10, marginTop:hp('2%')}}
              onPress={() => Linking.openURL('mailto:contactus@compassholding.net?subject=Lease a truck aplication from app&body='+content)}
              >
                <Text style={{padding:wp('1.5%'), fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:wp('6%'), alignSelf:'center', color:'white'}}>SUBMIT</Text>
              </TouchableOpacity>
          </ScrollView>
      </KeyboardAwareView>
  </SafeAreaView>
  );
  }
    


const styles = StyleSheet.create({
  container: {
    width:'90%',
    marginHorizontal:'5%',
    marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
    paddingBottom: wp('7%')
},
  text: {
    fontSize: wp('4%'),
    fontFamily:'Myriad-Pro-Bold-Condensed',
    color:'#484848',
    marginBottom:20,
    alignSelf:'center'
  },
  inputContainer:{
    backgroundColor:'white',
    borderWidth:1, 
    borderRadius:10, 
    borderColor:'#e30035', 
    marginBottom:hp('1.5%'),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  }});

export default TruckRentalLease;