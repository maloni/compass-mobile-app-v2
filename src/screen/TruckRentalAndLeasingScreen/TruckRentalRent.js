import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, SafeAreaView, ScrollView, TouchableOpacity, TextInput, Linking, } from 'react-native';
import TRLlogo from '../../components/TRLlogo';
import Constants from 'expo-constants';
import InsuranceInput from '../../components/InsuranceInput';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import TrailerList from '../../components/TrailerList';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


const TruckRentalRent = () => {
    const [companyName, setCompanyName] = useState('');
    const [mcNumber, setMcNumber] = useState('');
    const [state, setState] = useState('');
    const [yearsInBusiness, setYearsInBusiness] = useState('');
    const [contactPerson, setContactPerson] = useState('');
    const [businessPhone, setBusinessPhone] = useState('');
    const [email, setEmail] = useState('');
    const [checkMail, setCheckMail] = useState('');

    var content ="Company Name : " + companyName + "<br/> MC Number : " + mcNumber + "<br/> State : " + state + "<br/> How long in Business : " + yearsInBusiness + "<br/> Contact Person : " + contactPerson + "<br/> Business Phone : " + businessPhone + "<br/> Email : " + email;

    return(
        <SafeAreaView style={{width:'100%', height:'100%'}}>
        <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
            <ScrollView
            contentContainerStyle={styles.container}           
            automaticallyAdjustContentInsets={true}
            showsVerticalScrollIndicator={false}
            bounces={true}
            >
                <TRLlogo />
                <Text style={{...styles.text, fontSize:wp('5%')}}>Why Rent with Compass?</Text>
                <Text style={styles.text}>
                &#10004; No overhead {"\n"}
                &#10004; Newer equipment {"\n"}
                &#10004; Help address peak season {"\n"}
                &#10004; No maintenance expense {"\n"}
                &#10004; 24/7 call center {"\n"}
                &#10004; Always virgin tires {"\n"}
                &#10004; Access to national accounts {"\n"}
                &#10004; Better driver retention {"\n"}
                </Text>
                <Text style={{...styles.text, fontSize:wp('5%')}}>Contact us about renting a truck</Text>
                <InsuranceInput 
                onChangeText={(Text) => setCompanyName(Text)}
                value={companyName}
                placeholder='Company Name'
                icon='office-building'
                color='#e30035'
                validate={false}
                />
                <InsuranceInput 
                onChangeText={(Text) => setMcNumber(Text)}
                value={mcNumber}
                placeholder='MC Number'
                icon='truck'
                color='#e30035'
                validate={false}
                />
                <InsuranceInput 
                onChangeText={(Text) => setState(Text)}
                value={state}
                placeholder='State'
                icon='flag-variant'
                color='#e30035'
                validate={false}
                />
                <InsuranceInput 
                onChangeText={(Text) => setYearsInBusiness(Text)}
                value={yearsInBusiness}
                placeholder='Years in Business'
                icon='calendar-question'
                color='#e30035'
                validate={false}
                />
                <InsuranceInput 
                onChangeText={(Text) => setContactPerson(Text)}
                value={contactPerson}
                placeholder='Contact Person'
                icon='account-circle'
                color='#e30035'
                validate={false}
                />
                <InsuranceInput 
                onChangeText={(Text) => setBusinessPhone(Text)}
                value={businessPhone}
                placeholder='Business Phone'
                icon='phone'
                color='#e30035'
                validate={false}
                />
                <InsuranceInput 
                onChangeText={(Text) => setEmail(Text)}
                value={email}
                placeholder='Email'
                icon='email'
                color='#e30035'
                validate={false}
                />
                <TouchableOpacity
                style={{backgroundColor:'#e30035', width:wp('40%'), alignSelf:'center', borderRadius:10, marginTop:hp('2%')}}
                onPress={() => Linking.openURL('mailto:contactus@compassholding.net?subject=Rent a truck aplication from app&body='+content)}
                >
                  <Text style={{padding:wp('1.5%'), fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:wp('6%'), alignSelf:'center', color:'white'}}>SUBMIT</Text>
                </TouchableOpacity>
            </ScrollView>
        </KeyboardAwareView>
    </SafeAreaView>
    );
  }
    


const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        paddingBottom: wp('7%')
    },
      text: {
        fontSize: wp('4%'),
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        marginBottom:20,
        alignSelf:'center'
      },
      inputContainer:{
        backgroundColor:'white',
        borderWidth:1, 
        borderRadius:10, 
        borderColor:'#e30035', 
        marginBottom:hp('1.5%'),
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
      }
});

export default TruckRentalRent;