import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, SafeAreaView, ScrollView, Dimensions, TouchableOpacity, Linking, TextInput, KeyboardAvoidingView } from 'react-native';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import { FontAwesome, Ionicons, MaterialCommunityIcons } from '@expo/vector-icons';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import TRLlogo from '../../components/TRLlogo';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import LoveCompass from '../../components/LoveCompass';

const TruckRentalLocations = () => {

  return (
    <SafeAreaView style={{ width: '100%', height: '100%' }}>
      <KeyboardAwareView behavior={"padding"} enabled style={{ width: '100%', height: '100%' }} animated={true}>
        <ScrollView
          contentContainerStyle={styles.container}
          automaticallyAdjustContentInsets={true}
          showsVerticalScrollIndicator={false}
          bounces={true}
        >
          <TRLlogo />
          <View style={{ alignSelf: 'center', alignItems: 'flex-start', padding: hp('2%'), backgroundColor: '#F8F8F8', borderRadius: 10, marginTop: hp('3%') }}>
            <MapView
              provider={PROVIDER_GOOGLE}
              style={{ width: wp('80%'), height: hp('25%') }}
              initialRegion={{
                latitude: 41.752291,
                longitude: -87.930337,
                latitudeDelta: 0.0222,
                longitudeDelta: 0.0421,
              }}
            >
              <Marker
                coordinate={{
                  latitude: 41.752291,
                  longitude: -87.930337
                }}
                title='Compass Lease'
                onPress={() => Linking.openURL('https://www.google.com/maps/place/15W580+N+Frontage+Rd,+Burr+Ridge,+IL+60527,+%D0%A1%D1%98%D0%B5%D0%B4%D0%B8%D1%9A%D0%B5%D0%BD%D0%B5+%D0%94%D1%80%D0%B6%D0%B0%D0%B2%D0%B5/@41.7495129,-87.9411808,15z/data=!4m13!1m7!3m6!1s0x880e48ad87d9434f:0xb6134897c334c143!2zMTVXNTgwIE4gRnJvbnRhZ2UgUmQsIEJ1cnIgUmlkZ2UsIElMIDYwNTI3LCDQodGY0LXQtNC40ZrQtdC90LUg0JTRgNC20LDQstC1!3b1!8m2!3d41.7519792!4d-87.9304485!3m4!1s0x880e48ad87d9434f:0xb6134897c334c143!8m2!3d41.7519792!4d-87.9304485')}
              />
            </MapView>
            <View style={{ flexDirection: 'row', marginRight: wp('1%'), alignItems: 'center', marginTop: hp('2%') }}>
              <FontAwesome name='road' size={wp('6%')} color='#484848' />
              <View style={{ flexDirection: 'column', paddingLeft: wp('2%') }}>
                <Text style={styles.txt}>15W580 N Frontage Rd. Burr Ridge, IL 60527 </Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Ionicons name='md-call' size={wp('7%')} color='#484848' /><Text style={{ ...styles.txt, paddingLeft: wp('4%') }}>630-286-7240</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <MaterialCommunityIcons name='fax' size={wp('7%')} color='#484848' /><Text style={{ ...styles.txt, paddingLeft: wp('2%') }}>630-614-1780</Text>
            </View>
          </View>

          <View style={{ alignSelf: 'center', alignItems: 'flex-start', padding: hp('2%'), backgroundColor: '#F8F8F8', borderRadius: 10, marginTop: hp('3%') }}>
            <MapView
              provider={PROVIDER_GOOGLE}
              style={{ width: wp('80%'), height: hp('25%') }}
              initialRegion={{
                latitude: 32.714153,
                longitude: -97.226928,
                latitudeDelta: 0.0222,
                longitudeDelta: 0.0421,
              }}
            >
              <Marker
                coordinate={{
                  latitude: 32.714153,
                  longitude: -97.226928
                }}
                title='Compass Lease'
                onPress={() => Linking.openURL('https://www.google.rs/maps/place/3301+E+Loop+820+S,+Fort+Worth,+TX+76119,+%D0%A1%D1%98%D0%B5%D0%B4%D0%B8%D1%9A%D0%B5%D0%BD%D0%B5+%D0%94%D1%80%D0%B6%D0%B0%D0%B2%D0%B5/@32.7141797,-97.2276095,3a,75y,84.11h,91.13t/data=!3m7!1e1!3m5!1s6gkFiA2k6kz5ch2k_khzDg!2e0!6s%2F%2Fgeo1.ggpht.com%2Fcbk%3Fpanoid%3D6gkFiA2k6kz5ch2k_khzDg%26output%3Dthumbnail%26cb_client%3Dsearch.gws-prod.gps%26thumb%3D2%26w%3D86%26h%3D86%26yaw%3D117.61885%26pitch%3D0%26thumbfov%3D100!7i16384!8i8192!4m5!3m4!1s0x864e7ae0b05065e5:0xc08eaac9d0d53d0f!8m2!3d32.7139129!4d-97.226986')}
              />
            </MapView>
            <View style={{ flexDirection: 'row', marginRight: wp('1%'), alignItems: 'center', marginTop: hp('2%') }}>
              <FontAwesome name='road' size={wp('6%')} color='#484848' />
              <View style={{ flexDirection: 'column', paddingLeft: wp('2%') }}>
                <Text style={styles.txt}>3301 E. Loop 820 S. Fort Worth, TX 76119</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Ionicons name='md-call' size={wp('7%')} color='#484848' /><Text style={{ ...styles.txt, paddingLeft: wp('4%') }}>214-396-5942</Text>
            </View>
          </View>
          <View style={{ alignSelf: 'center', alignItems: 'flex-start', padding: hp('2%'), backgroundColor: '#F8F8F8', borderRadius: 10, marginTop: hp('3%') }}>
            <MapView
              provider={PROVIDER_GOOGLE}
              style={{ width: wp('80%'), height: hp('25%') }}
              initialRegion={{
                latitude: 37.040210,
                longitude: -86.329742,
                latitudeDelta: 0.0222,
                longitudeDelta: 0.0421,
              }}
            >
              <Marker
                coordinate={{
                  latitude: 37.040210,
                  longitude: -86.329742
                }}
                title='Compass Lease'
                onPress={() => Linking.openURL('https://www.google.com/maps/place/7520+Louisville+Rd,+Bowling+Green,+KY+42101,+USA/@37.0400217,-86.3319897,17z/data=!3m1!4b1!4m5!3m4!1s0x8865e365abc48367:0xe96366a9e60e6261!8m2!3d37.0400217!4d-86.329801')}
              />
            </MapView>
            <View style={{ flexDirection: 'row', marginRight: wp('1%'), alignItems: 'center', marginTop: hp('2%') }}>
              <FontAwesome name='road' size={wp('6%')} color='#484848' />
              <View style={{ flexDirection: 'column', paddingLeft: wp('2%') }}>
                <Text style={styles.txt}>7520 Louisville Road, Bowling Green, KY 42101 </Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Ionicons name='md-call' size={wp('7%')} color='#484848' /><Text style={{ ...styles.txt, paddingLeft: wp('4%') }}>270-200-4808</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Ionicons name='md-call' size={wp('7%')} color='#484848' /><Text style={{ ...styles.txt, paddingLeft: wp('4%') }}>270-996-0787</Text>
            </View>

          </View>
          <View style={{ justifyContent: 'flex-end', alignSelf: 'center', width: '80%', marginTop: hp('3%') }}>
            <LoveCompass />
          </View>

        </ScrollView>
      </KeyboardAwareView>
    </SafeAreaView>
  );
}



const styles = StyleSheet.create({
  container: {
    width: '90%',
    marginHorizontal: '5%',
    marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
    paddingBottom: wp('7%')
  },
  txt: {
    fontFamily: 'Myriad-Pro-Bold-Condensed',
    color: '#484848',
    fontSize: hp('2%'),
    textAlign: 'justify'
  },
  btn: {
    marginVertical: hp('1%'),
    width: '80%',
    alignSelf: 'center',
    borderWidth: 3,
    borderColor: '#da212f',
    borderRadius: 10,
    backgroundColor: 'white',
    shadowOffset: {
      width: 10,
      height: 6,
    },
    shadowOpacity: 0.30,
    shadowRadius: 12,
    shadowColor: '#484848',
    elevation: 8,

  },
  scrollView: {
    flexGrow: 1,
    width: '100%',
    height: '100%'
  },
});

export default TruckRentalLocations;