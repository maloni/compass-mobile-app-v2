import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity, SafeAreaView, ScrollView,Platform} from 'react-native';
import  CLlogo  from '../../components/CLlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import LoveCompass from '../../components/LoveCompass';
import Axios from 'axios';
import { FlatList } from 'react-native-gesture-handler';
import  Modal  from 'react-native-modal'
import { WebView } from 'react-native-webview';

const LogisticScreen = ({ navigation }) => {

    const[deals, setDeals] = useState()
    const[showMore,setShowMore] = useState(false)

    useEffect(() => {
        Axios.get('https://apps.compassaws.net/api/v1/fuelDeals')
       .then(res => {
           console.log(res.data)
           setDeals(res.data)
           
       })
       .then(err => {
           console.log(err)
       })
   }, [])


    return(
    <SafeAreaView style={{width:'100%', height:'100%'}}>
    <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
        <View      
            style={styles.container}           
        >
                <CLlogo />
                <Text style={{...styles.txt, marginTop:hp('2%')}}>SMART,  RELIABLE, STRATEGIC</Text>
                <Text style={{...styles.txt, marginTop:hp('2%')}}>One call away from shipping your product.</Text>
                <View style={{flexDirection:'row', alignSelf:'center'}}>
                    <TouchableOpacity onPress={() => setShowMore(true)}>
                    <   Text style={{...styles.txt, color:'rgb(13,79,147)'}}>Click here</Text>
                    </TouchableOpacity>
                    <Text style={styles.txt}> to start a quote today!</Text></View>
                <View style={styles.txtContainer}>
                    <Text style={{...styles.txt, color:'white',fontSize:wp('4%')}}>Full Truckload</Text>
                    <Text style={{...styles.txt, fontSize:wp('3.5%'), fontFamily:'Myriad-Pro-Condensed', color:'white'}}>Dry Van, Flatbed, Temperature</Text>
                    <Text style={{...styles.txt, fontSize:wp('3.5%'), fontFamily:'Myriad-Pro-Condensed', color:'white'}}>Controlled, Power Only</Text>
                </View>
                <View style={styles.txtContainer}>
                    <Text style={{...styles.txt, color:'white',fontSize:wp('4%')}}>Intermodal</Text>
                    <Text style={{...styles.txt, fontSize:wp('3.5%'), fontFamily:'Myriad-Pro-Condensed', color:'white'}}>20 ft, 40 ft, 48 ft Containers</Text>
                    <Text style={{...styles.txt, fontSize:wp('3.5%'), fontFamily:'Myriad-Pro-Condensed', color:'white'}}>53 ft Trailers</Text>
                </View>
                <View style={styles.txtContainer}>
                    <Text style={{...styles.txt, color:'white',fontSize:wp('4%')}}>Less-Than-Truckload</Text>
                    <Text style={{...styles.txt, fontSize:wp('3.5%'), fontFamily:'Myriad-Pro-Condensed', color:'white'}}>Straight LTL</Text>
                    <Text style={{...styles.txt, fontSize:wp('3.5%'), fontFamily:'Myriad-Pro-Condensed', color:'white'}}>Partial Shipments</Text>
                </View>
                <View style={styles.txtContainer}>
                    <Text style={{...styles.txt, color:'white',fontSize:wp('4%')}}>Specialized</Text>
                    <Text style={{...styles.txt, fontSize:wp('3.5%'), fontFamily:'Myriad-Pro-Condensed', color:'white'}}>Expedited</Text>
                    <Text style={{...styles.txt, fontSize:wp('3.5%'), fontFamily:'Myriad-Pro-Condensed', color:'white'}}>Over Size and Dimensional</Text>
                </View>

                <View style={{flexDirection:'row', justifyContent:'space-between', marginTop:hp('4%')}}>
                    <TouchableOpacity style={styles.btn}><Text style={{...styles.txt, color:'white'}}>Email Us</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.btn}><Text style={{...styles.txt, color:'white'}}>Call Us</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.btn}><Text style={{...styles.txt, color:'white'}}>Review Us</Text></TouchableOpacity>
                </View>
                    <LoveCompass />
                <Modal
                animationType='slideInUp'
                isVisible={showMore}
                onRequestClose={() => setShowMore(false)}
                onBackdropPress={() => setShowMore(false)}
                // backdropOpacity={0.9}
                hasBackdrop={true}
                backdropColor='black'
                >
                    <WebView source={{ uri:'https://compasslogistics.net/contact.php' }} style={{ width:wp('100%'),height:hp('100%')}} />  
                </Modal>
            </View>
    </KeyboardAwareView>
    </SafeAreaView>
    )
};


LogisticScreen.navigationOptions = ({ navigation }) => {
    return{
        header: null,
    }
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        height:'90%',
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent', 
        flex:1     
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    },
    btn:{
        backgroundColor:'rgb(13,79,147)',
        padding:wp('2%'),
        borderRadius:8
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
    txtContainer:{
        width:wp('60%'),
        alignSelf:'center',
        backgroundColor:'rgb(13,79,147)',
        borderRadius:10,
        paddingVertical:wp('2%'),
        paddingHorizontal:hp('2%'),
        marginTop:hp('1%')
      }
      
});

export default LogisticScreen;