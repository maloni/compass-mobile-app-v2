import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity, SafeAreaView, ScrollView,Platform} from 'react-native';
import  SBlogo  from '../../components/SmartBoardLogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp, heightPercentageToDP} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import LoveCompass from '../../components/LoveCompass';
import Axios from 'axios';
import { FlatList } from 'react-native-gesture-handler';
import Modal from "react-native-modal";
import { WebView } from 'react-native-webview';

const SmartBoardHome = ({ navigation }) => {

    const[deals, setDeals] = useState()
    const[showMore,setShowMore] = useState(false)
    const[showVideo, setShowVideo] = useState(false)

    useEffect(() => {
        Axios.get('https://apps.compassaws.net/api/v1/fuelDeals')
       .then(res => {
           console.log(res.data)
           setDeals(res.data)
           
       })
       .then(err => {
           console.log(err)
       })
   }, [])


    return(
    <SafeAreaView style={{width:'100%', height:'100%'}}>
    <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
        <View      
            style={styles.container}           
        >
                <SBlogo />
                <Text style={{...styles.txt, marginTop:hp('2%'), textAlign:'center'}}>Need Transportation Management Software to run your business?</Text>
                <View style={{flexDirection:'row', alignSelf:'center'}}>
                    <TouchableOpacity onPress={() => setShowMore(true)}>
                        <Text style={{...styles.txt, color:'#7c5ca4'}}>Click here</Text>
                    </TouchableOpacity>
                    <Text style={styles.txt}> to schedule a Demo today!</Text></View>
                <View style={styles.txtContainer}>
                    <Text style={{...styles.txt, color:'white',fontSize:wp('5%')}}>Safety {'&'} Compliance</Text>
                    <Text style={{...styles.txt, color:'white',fontSize:wp('5%')}}>Dispatch {'&'} Load Management</Text>
                    <Text style={{...styles.txt, color:'white',fontSize:wp('5%')}}>Billing {'&'} Settlements</Text>
                    <Text style={{...styles.txt, color:'white',fontSize:wp('5%')}}>Maintenance</Text>
                    <Text style={{...styles.txt, color:'white',fontSize:wp('5%')}}>Accounting</Text>
                    <Text style={{...styles.txt, color:'white',fontSize:wp('5%')}}>Dashboards {'&'} Reporting</Text>

                </View>

                <View style={{flexDirection:'row', justifyContent:'space-between', marginTop:hp('4%')}}>
                    <TouchableOpacity style={styles.btn}><Text style={{...styles.txt, color:'white'}}>Email Us</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.btn}><Text style={{...styles.txt, color:'white'}}>Call Us</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.btn}><Text style={{...styles.txt, color:'white'}}>Review Us</Text></TouchableOpacity>
                </View>
                <View style={{flexDirection:'row', alignSelf:'center', marginTop:hp('2%')}}>
                <TouchableOpacity onPress={() => setShowVideo(true)}>
                        <Text style={{...styles.txt, color:'#7c5ca4', fontSize:wp('4.2%')}}>Click here </Text>
                    </TouchableOpacity>
                    <Text style={{...styles.txt, fontSize:wp('4.2%')}}>to watch a short video about SmartBoard!</Text>
                </View>
                    <LoveCompass />
                    <Modal
                        animationType='slideInUp'
                        isVisible={showMore}
                        onRequestClose={() => setShowMore(false)}
                        onBackdropPress={() => setShowMore(false)}
                        // backdropOpacity={0.9}
                        hasBackdrop={true}
                        backdropColor='black'
                        >
                            <WebView source={{ uri:'https://www.smartboardtms.com/contact.php' }} style={{ width:wp('100%'),height:hp('100%')}} />
                    </Modal>
                    <Modal
                        animationType='slideInUp'
                        isVisible={showVideo}
                        onRequestClose={() => setShowVideo(false)}
                        onBackdropPress={() => setShowVideo(false)}
                        // backdropOpacity={0.9}
                        hasBackdrop={true}
                        backdropColor='black'
                        >
                            <WebView source={{ uri:'https://www.youtube.com/watch?v=paTpFFxGU6k&feature=emb_title' }} style={{ width:wp('100%'),height:hp('100%')}} />
                    </Modal>
            </View>
    </KeyboardAwareView>
    </SafeAreaView>
    )
};


SmartBoardHome.navigationOptions = ({ navigation }) => {
    return{
        header: null,
    }
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        height:'90%',
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent', 
        flex:1,
        paddingTop:heightPercentageToDP('5%')
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    },
    btn:{
        backgroundColor:'#7c5ca4',
        padding:wp('2%'),
        borderRadius:8
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
    txtContainer:{
        width:wp('60%'),
        alignSelf:'center',
        backgroundColor:'#7c5ca4',
        borderRadius:20,
        paddingVertical:wp('2%'),
        marginTop:hp('3%')
      }
      
});

export default SmartBoardHome;