import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, SafeAreaView, ScrollView,Platform, Image} from 'react-native';
import  TSlogo  from '../../components/TSlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import Carousel from 'react-native-snap-carousel';
import { SliderBox } from "react-native-image-slider-box";
import numeral from 'numeral';
import { Slider } from 'react-native-elements';

const TruckSalesItem = ({ navigation }) => {
    const [truck, setTruck] = useState(navigation.getParam('truck'))
    
    let price = numeral(Number(truck.askingPrice/100)).format('$0,0.00')
    let miles = numeral(truck.miles).format('0,0')

    const truckImages=[
        `https://s3.amazonaws.com/${truck.truckPhotos1}`,
        `https://s3.amazonaws.com/${truck.truckPhotos2}`,
        `https://s3.amazonaws.com/${truck.truckPhotos3}`,
        `https://s3.amazonaws.com/${truck.truckPhotos4}`,
        `https://s3.amazonaws.com/${truck.truckPhotos5}` 
    ];

    return(
    <SafeAreaView style={{width:'100%', height:'100%'}}>
    <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
        <ScrollView      
            contentContainerStyle={styles.container}           
            automaticallyAdjustContentInsets={true}
            showsVerticalScrollIndicator={false}
            bounces={true}
        >
            
                  <TruckSpec
                  truck={truck}
                  />
            </ScrollView>
    </KeyboardAwareView>
    </SafeAreaView>
    )
};


TruckSalesItem.navigationOptions = ({ navigation }) => {
    return{
        header: null,
    }
};

const TruckSpec = ({ navigation, truck }) => {

    const[showPayment, setShowPayment] = useState(true)

    let price = numeral(Number(truck.askingPrice/100)).format('$0,0.00')
    let miles = numeral(truck.miles).format('0,0')

    const truckImages=[
        `https://s3.amazonaws.com/${truck.truckPhotos1}`,
        `https://s3.amazonaws.com/${truck.truckPhotos2}`,
        `https://s3.amazonaws.com/${truck.truckPhotos3}`,
        `https://s3.amazonaws.com/${truck.truckPhotos4}`,
        `https://s3.amazonaws.com/${truck.truckPhotos5}`
    ];

    return( <View>
        {showPayment
        ?<View>
            <View style={{height:hp('22%'), borderBottomWidth:5, borderBottomColor:'#da212f'}}> 
                <SliderBox 
                sliderBoxHeight={hp('20%')}
                resizeMode={'cover'}
                images={truckImages}
                inactiveDotColor="#90A4AE"
                dotColor="#da212f"
                dotStyle={{
                    width: 15,
                    height: 15,
                    borderRadius: 15,
                    marginHorizontal: 10,
                    padding: 0,
                    margin: 0
                  }}
                />
            </View>
            <Text style={styles.txt}>{truck.year} {truck.makeId_name} {truck.modelId_name}</Text>
            <View style={{flexDirection:'row', justifyContent:'space-evenly', padding:wp('2%')}}>
                <Text style={styles.txtSmall}>{price}</Text>
                <Text style={styles.txtSmall}>{miles} mi</Text>
            </View>
            <TouchableOpacity  
            onPress={() => setShowPayment(false)}
                    style={styles.btn}>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'white', padding:hp('1%'), textAlign:'center'}}>Payment Calculator</Text>
                    </TouchableOpacity>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <View style={styles.detailsContainer}>
                            <Text numberOfLines={1} allowFontScaling={true} style={{...styles.txt, fontSize:wp('5.8%'), alignSelf:'flex-start'}}>SPECIFICATIONS</Text>
                            <Text style={styles.txtDesc}>Stock#: {truck.stockNo}</Text>
                            <Text style={styles.txtDesc}>Make: {truck.makeId}</Text>
                            <Text style={styles.txtDesc}>Engine: {truck.DD15}</Text>
                            <Text style={styles.txtDesc}>Horsepower: {truck.hp}</Text>
                            <Text style={styles.txtDesc}>Gears Ratio: {truck.gearRatio}</Text>
                            <Text style={styles.txtDesc}>Jake Brake: {truck.jakebreak}</Text>
                            <Text style={styles.txtDesc}>Wheel Base: {truck.wheelBase}</Text>
                            <Text style={styles.txtDesc}>Tire: {truck.tire}</Text>
                        </View>
                        <View style={styles.detailsContainer}>
                            <Text style={{...styles.txt, fontSize:wp('5.8%'), alignSelf:'flex-start'}}>DESCRIPTION</Text>
                            <Text style={styles.txtDesc}>Year: {truck.year}</Text>
                            <Text style={styles.txtDesc}>Model: {truck.modelId_name}</Text>
                            <Text style={styles.txtDesc}>Engine Type: {truck.engineType}</Text>
                            <Text style={styles.txtDesc}>Transmission: {truck.transmission}</Text>
                            <Text style={styles.txtDesc}>OverDrive: {truck.overdrive}</Text>
                            <Text style={styles.txtDesc}>Axle Count: {truck.axleCount}</Text>
                            <Text style={styles.txtDesc}>Wheels: {truck.wheel}</Text>
                            <Text style={styles.txtDesc}>Sleeper Size: {truck.sleeperSize}</Text>
                        </View>
                    </View>
                    </View>
                    :<TruckCalculator 
                    truck={truck}
                    showPayment={() => setShowPayment(true)}
                    />}
                    </View>
        
    )
};



const TruckCalculator = ({ navigation , truck}) => {
    const[showPayment, setShowPayment] = useState(true)

    const inactive = {
        txtColor:'#484848',
        backgroundColor:'white',
        borderWidth:1
    }
 
    const active = {txtColor:'white',
        backgroundColor:'#da212f',
        borderWidth:0}

    const [btn1, setBtn1] = useState(inactive)
    const [btn2, setBtn2] = useState(inactive)
    const [btn3, setBtn3] = useState(inactive)
    const [btn4, setBtn4] = useState(inactive)
    const [btn5, setBtn5] = useState(inactive)
        
    const [interestRate, setInterestRate] = useState()
    const [downPayment, setDownPayment] = useState()
    const [months, setMonths] = useState()
    const [res, setRes] = useState()
    const [showCalculation, setShowCalculation]= useState(true)

    let price = numeral(Number(truck.askingPrice/100)).format('$0,0.00')
    let miles = numeral(truck.miles).format('0,0')

    const showpay = () => {
        if ((Number(truck.askingPrice/100) == null || Number(truck.askingPrice/100) == 0) ||
            (months == null || months == 0)
       ||
            (interestRate == null || interestRate.length == 0))
        { setRes("Incomplete data");
        }
        else
        {
        var princ = Number(truck.askingPrice/100);
        var term  = months;
        var intr   = interestRate / 1200;
        setRes(princ * intr / (1 - (Math.pow(1/(1 + intr), term))));
        }
       
       // payment = principle * monthly interest/(1 - (1/(1+MonthlyInterest)*Months))
       }
    let priceRes = numeral(Number(res)).format('$0,0.00')

    
    console.log(res)

    console.log(typeof interestRate)
    return(
        <View>
            <View style={styles.calcTopContainer}>
                <View style={{ width:wp('50%'),height:hp('20%'), borderRightWidth:5, justifyContent:'center', paddingRight:wp('3%'), borderRightColor:'#da212f',paddingLeft:wp('2%')}}>
                    <Image 
                    style={{width:'100%', height:'75%', justifyContent:'center',  alignSelf:'center'}}
                    resizeMode='cover'
                    resizeMethod='resize'       
                    source={{uri: `https://s3.amazonaws.com/${truck.truckPhotos1}`}}/>
                </View>
                <View style={{paddingLeft:wp('1%')}}>
                    <Text style={styles.paymentHead}>{truck.year} {truck.makeId_name}</Text>
                    <Text style={styles.paymentHead}> {truck.modelId_name}</Text>
                    <Text style={styles.paymentHeadSmall}>{price}</Text>
                    <Text style={styles.paymentHeadSmall}>{miles} mi</Text>
                </View>
            </View>
            <View style={{flexDirection:'row', marginTop:hp('5%'), width:'90%', alignSelf:'center'}}>
                <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#484848', fontSize:wp('6%'),  padding:wp('1%'), paddingHorizontal:wp('2%'), alignSelf:'flex-start'}}>Terms (Months)</Text>
                <TouchableOpacity
                onPress={() => {setBtn1(active), setBtn2(inactive), setBtn3(inactive), setBtn4(inactive), setBtn5(inactive), setMonths(12)}}
                style={{borderWidth:btn1.borderWidth, borderRadius:8, marginHorizontal:wp('1%'), justifyContent:'center', alignContent:'center', backgroundColor:btn1.backgroundColor,}}
                >
                    <Text style={{fontFamily:'Myriad-Pro-Condensed', color:btn1.txtColor, fontSize:wp('5%'), padding:wp('1%'), paddingHorizontal:wp('2%'), alignSelf:'center'}}>12</Text>
                </TouchableOpacity>
                <TouchableOpacity
                onPress={() => {setBtn1(inactive), setBtn2(active), setBtn3(inactive), setBtn4(inactive), setBtn5(inactive), setMonths(24)}}
                style={{borderWidth:btn2.borderWidth, backgroundColor:btn2.backgroundColor, borderRadius:8, marginHorizontal:wp('1%')}}
                >
                    <Text style={{fontFamily:'Myriad-Pro-Condensed', color:btn2.txtColor, fontSize:wp('5%'), padding:wp('1%'), paddingHorizontal:wp('2%'), alignSelf:'center'}}>24</Text>
                </TouchableOpacity>
                <TouchableOpacity
                onPress={() => {setBtn1(inactive), setBtn2(inactive), setBtn3(active), setBtn4(inactive), setBtn5(inactive), setMonths(36)}}
                style={{borderWidth:btn3.borderWidth,backgroundColor:btn3.backgroundColor, borderRadius:8, marginHorizontal:wp('1%')}}
                >
                    <Text style={{fontFamily:'Myriad-Pro-Condensed', color:btn3.txtColor, fontSize:wp('5%'), padding:wp('1%'), paddingHorizontal:wp('2%'), alignSelf:'center'}}>36</Text>
                </TouchableOpacity>
                <TouchableOpacity
                onPress={() => {setBtn1(inactive), setBtn2(inactive), setBtn3(inactive), setBtn4(active), setBtn5(inactive), setMonths(48)}}
                style={{borderWidth:btn4.borderWidth,backgroundColor:btn4.backgroundColor, borderRadius:8, marginHorizontal:wp('1%')}}
                >
                    <Text style={{fontFamily:'Myriad-Pro-Condensed', color:btn4.txtColor, fontSize:wp('5%'), padding:wp('1%'), paddingHorizontal:wp('2%'), alignSelf:'center'}}>48</Text>
                </TouchableOpacity>
                <TouchableOpacity
                onPress={() => {setBtn1(inactive), setBtn2(inactive), setBtn3(inactive), setBtn4(inactive), setBtn5(active), setMonths(60)}}
                style={{borderWidth:btn5.borderWidth,backgroundColor:btn5.backgroundColor, borderRadius:8, marginHorizontal:wp('1%')}}
                >
                    <Text style={{fontFamily:'Myriad-Pro-Condensed', color:btn5.txtColor, fontSize:wp('5%'), padding:wp('1%'), paddingHorizontal:wp('2%'), alignSelf:'center'}}>60</Text>
                </TouchableOpacity>
            </View>
            <View style={{width:'90%', alignSelf:'center', marginTop:hp('3%')}}>
                <Text style={{ fontFamily:'Myriad-Pro-Condensed', color:'#484848', fontSize:wp('6%'), padding:wp('1%'), paddingHorizontal:wp('2%'), alignSelf:'flex-start'}}>Interest Rate :</Text>
                <Slider
                    step={0.250}
                    value={interestRate}
                    onValueChange={value => setInterestRate(value)}
                    maximumValue={5.375}
                    minimumValue={0}
                    minimumTrackTintColor='#e08694'
                    maximumTrackTintColor='#d9d9d9'
                    style={{width:'95%', alignSelf:'center'}}
                    trackStyle={{height:hp('1.5%'), borderRadius:10, borderColor:'green'}}
                    thumbStyle={{width:wp('8%'), height:wp('8%'), borderRadius:20, backgroundColor:'#da212f'}}
                />
                <View style={{flexDirection:'row', justifyContent:'space-between', paddingHorizontal:wp('2%')}}>
                    <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#484848', fontSize:wp('4%'), paddingLeft:wp('2%')}}>0%</Text>
                    <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#484848', fontSize:wp('5%'), paddingLeft:wp('2%')}}>{interestRate}%</Text>
                    <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#484848', fontSize:wp('4%')}}>5.375%</Text>
                </View>
            </View>
            <View style={{width:'90%', alignSelf:'center', marginTop:hp('2%')}}>
                <Text style={{ fontFamily:'Myriad-Pro-Condensed', color:'#484848', fontSize:wp('6%'), padding:wp('1%'), paddingHorizontal:wp('2%'), alignSelf:'flex-start'}}>Down Payment:</Text>
                <Slider
                    step={0.5}
                    value={downPayment}
                    onValueChange={value => setDownPayment(value)}
                    maximumValue={20}
                    minimumValue={0}
                    minimumTrackTintColor='#e08694'
                    maximumTrackTintColor='#d9d9d9'
                    style={{width:'95%', alignSelf:'center'}}
                    trackStyle={{height:hp('1.5%'), borderRadius:10, borderColor:'green'}}
                    thumbStyle={{width:wp('8%'), height:wp('8%'), borderRadius:20, backgroundColor:'#e70c31'}}
                />
                <View style={{flexDirection:'row', justifyContent:'space-between', paddingHorizontal:wp('2%')}}>
                    <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#484848', fontSize:wp('4%'), paddingLeft:wp('2%')}}>0%</Text>
                    <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#484848', fontSize:wp('5%'), paddingLeft:wp('2%')}}>{downPayment}%</Text>
                    <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#484848', fontSize:wp('4%')}}>20%</Text>
                </View>
            </View>
            {showCalculation
            ?<TouchableOpacity
            onPress={() => {showpay(), setShowCalculation(false)}}
            style={{backgroundColor:'#e70c31', width:wp('70%'), alignSelf:'center', marginTop:hp('3%'), borderRadius:10}}
            >
                <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'white', fontSize:wp('7%'), alignSelf:'center', paddingVertical:hp('1%')}}>Calculate Monthly Payment</Text>
            </TouchableOpacity>
            :<View style={{marginTop:hp('2%')}}>
                <Text style={{...styles.txtSmall, fontSize:wp('6%')}}>Monthly payment is:</Text>
                <Text style={{...styles.txtSmall, fontSize:wp('6%')}}>{priceRes}</Text>
                <TouchableOpacity
                onPress={() => setShowCalculation(true)}
                style={{backgroundColor:'#e70c31', width:wp('70%'), alignSelf:'center', marginTop:hp('3%'), borderRadius:10}}                
                >
                    <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'white', fontSize:wp('7%'), alignSelf:'center', paddingVertical:hp('1%')}}>Calculate Under New Terms</Text>
                </TouchableOpacity>
            </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    calcTopContainer:{
        paddingTop: Platform.OS === 'ios' ? hp('2%') : null,
        backgroundColor:'white',
        height:hp('23%'),
        width:'100%',
        flexDirection:'row',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 8,
        },
        shadowOpacity: 0.24,
        shadowRadius: 8.00,

        elevation: 14,
    },
    paymentHead:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:wp('6.5%'),
        paddingLeft:wp('1%')
    },
    paymentHeadSmall:{
        fontFamily:'Myriad-Pro-Condensed',
        color:'#484848',
        fontSize:wp('5%'),
        paddingLeft:wp('1%'),
        paddingTop:hp('1%')
    },
    detailsContainer:{
        width:'50%',
        justifyContent:'center',
        paddingLeft:'3%',
    },
    container: {
        width:'100%',
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent',
        paddingBottom: wp('10%')

    },
    txtSmall:{
        fontFamily:'Myriad-Pro-Condensed',
        color:'#484848',
        fontSize:wp('5%'),
        alignSelf:'center',
        textAlign:'justify'
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:wp('6.5%'),
        alignSelf:'center',
        padding:hp('2%'),
        textAlign:'justify'
    },
    btn:{
        marginVertical:hp('2%'),
        width:'70%',
        alignSelf:'center',
        borderRadius:10,
        backgroundColor:'#da212f',
        shadowOffset: {
        width: 10,
        height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#484848',
        elevation: 8,
        
        },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      txtDesc:{
        fontFamily:'Myriad-Pro-Condensed',
        color:'#484848',
        fontSize:wp('4%'),
        textAlign:'justify',
        paddingHorizontal:hp('2%'),


      }
      
});

export default TruckSalesItem;