import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, SafeAreaView, ScrollView, Dimensions, TouchableOpacity, Linking, TextInput, KeyboardAvoidingView } from 'react-native';
import TSlogo from '../../components/TSlogo';
import Constants from 'expo-constants';
import InsuranceInput from '../../components/InsuranceInput';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import LoveCompass from '../../components/LoveCompass';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';



const TruckSalesRefer = () => {
  const [yourName, setYourName] = useState('')
  const [yourCompanyName, setYourCompanyName] = useState('')
  const [yourEmail, setYourEmail] = useState('')
  const [yourPhone, setYourPhone] = useState('')

  const [comapnyName, setCompanyName] = useState('')
  const [state, setState] = useState('')
  const [contactPerson, setContactPerson] = useState('')
  const [phone, setPhone] = useState('')
  const [email, setEmail] = useState('')
  
  let content = 
                  " Referral information:%0D%0A  Name: "+ yourName 
                  + "%0D%0A  Company Name: " + yourCompanyName 
                  + "%0D%0A  Email: " + yourEmail 
                  + "%0D%0A  Phone: " + yourPhone 
                  + "%0D%0A %0D%0A  Referring Company:  %0D%0A  Company Name: " + comapnyName 
                  + "%0D%0A  State: " + state 
                  + "%0D%0A  Contact Person : " + contactPerson 
                  + "%0D%0A  Phone: " + phone 
                  + "%0D%0A  Email: " + email;
  return(
      <SafeAreaView style={{width:'100%', height:'100%'}}>
      <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
          <ScrollView      
              contentContainerStyle={styles.container}           
              automaticallyAdjustContentInsets={true}
              showsVerticalScrollIndicator={false}
              bounces={true}
          >
             <View> 
                  <TSlogo />
                  <Text style={{...styles.txt, fontFamily:'Myriad-Pro-Bold-Condensed'}}>
                  Do you know someone that is looking to rent or lease trucks? Refer them to us. We will contact them and you will earn a referral bonus!                  </Text>
                  <View style={styles.inputContainer}>
                      <Text style={{...styles.txt,padding:0}}>Your Name :</Text>
                          <TextInput 
                          value={yourName}
                          onChangeText={(text) => setYourName(text)}
                          style={styles.input}
                          />
                  </View>
                  <View style={styles.inputContainer}>
                      <Text style={{...styles.txt,padding:0}}>Your Company Name :</Text>
                          <TextInput 
                          value={yourCompanyName}
                          onChangeText={(text) => setYourCompanyName(text)}
                          style={styles.input}
                          />
                  </View>
                  <View style={styles.inputContainer}>
                      <Text style={{...styles.txt,padding:0}}>Your Email :</Text>
                          <TextInput 
                          value={yourEmail}
                          onChangeText={(text) => setYourEmail(text)}
                          style={styles.input}
                          />
                  </View>
                  <View style={styles.inputContainer}>
                      <Text style={{...styles.txt,padding:0}}>Your Phone :</Text>
                          <TextInput 
                          value={yourPhone}
                          onChangeText={(text) => setYourPhone(text)}
                          style={styles.input}
                          />
                  </View>
  
                  <Text style={{...styles.txt, fontFamily:'Myriad-Pro-Bold-Condensed'}}>Enter the information for the company you are referring to us here</Text>
                  <View style={styles.inputContainer}>
                      <Text style={{...styles.txt,padding:0}}>Comapny Name :</Text>
                          <TextInput 
                          value={comapnyName}
                          onChangeText={(text) => setCompanyName(text)}
                          style={styles.input}
                          />
                  </View>
                  <View style={styles.inputContainer}>
                      <Text style={{...styles.txt,padding:0}}>State :</Text>
                          <TextInput 
                          value={state}
                          onChangeText={(text) => setState(text)}
                          style={styles.input}
                          />
                  </View>
                  <View style={styles.inputContainer}>
                      <Text style={{...styles.txt,padding:0}}>Contact Person :</Text>
                          <TextInput 
                          value={contactPerson}
                          onChangeText={(text) => setContactPerson(text)}
                          style={styles.input}
                          />
                  </View>
                  <View style={styles.inputContainer}>
                      <Text style={{...styles.txt,padding:0}}>Phone :</Text>
                          <TextInput 
                          value={phone}
                          onChangeText={(text) => setPhone(text)}
                          style={styles.input}
                          />
                  </View>
                  <View style={styles.inputContainer}>
                      <Text style={{...styles.txt,padding:0}}>Email :</Text>
                          <TextInput 
                          value={email}
                          onChangeText={(text) => setEmail(text)}
                          style={styles.input}
                          />
                  </View>
  
                  <TouchableOpacity
                  style={styles.btn}
                  onPress={() => Linking.openURL(`mailto:contactus@compassholding.net?subject=Referral from CTS mobile app&body=` + content)}
                  >
                      <Text style={{...styles.txt,padding:hp('1%'), fontSize:wp('5%'), fontFamily:'Myriad-Pro-Bold-Condensed'}}>
                          SUBMIT
                      </Text>
                  </TouchableOpacity>
  
                  <View style={{justifyContent:'flex-end', alignSelf:'center', width:'80%', marginTop:hp('2%')}}>
                      <LoveCompass />
                  </View>
  
                      
              </View>
              </ScrollView>
      </KeyboardAwareView>
      </SafeAreaView>
      );
  }
    


const styles = StyleSheet.create({
  container: {
    width:'90%',
    marginHorizontal:'5%',
    marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
    backgroundColor:'transparent',
    paddingBottom: wp('7%')

},
inputContainer:{
    flexDirection:'row', 
    borderBottomWidth:2 , 
    width:wp('80%'), 
    alignSelf:'center', 
    marginBottom:hp('1%'), 
    borderBottomColor:'#eaeaea'
},
input:{
    alignSelf:'center',
    fontFamily:'Myriad-Pro-Condensed',
    fontSize:wp('4%'),
    padding:wp('1%'),
    width:'60%'
},
txt: {
    fontFamily:'Myriad-Pro-Condensed',
    color:'#484848',
    fontSize:22,
    alignSelf:'center',
    padding:hp('2%'),
    textAlign:'justify'
},
btn:{
    marginVertical:hp('2%'),
    width:'60%',
    alignSelf:'center',
    borderWidth:3, 
    borderColor:'#da212f', 
    borderRadius:10,
    backgroundColor:'white',
    shadowOffset: {
    width: 10,
    height: 6,
    },
    shadowOpacity: 0.30,
    shadowRadius: 12,
    shadowColor:'#484848',
    elevation: 8,
    
    },
scrollView: {
    flexGrow:1,
    width:'100%',
    height:'100%'
  },
});

export default TruckSalesRefer;