import React, { useContext, useState, useEffect } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import NavigationSearch from '../../components/NavigationSearch';
import MapView, { PROVIDER_GOOGLE, Marker, Callout } from 'react-native-maps';
import * as Permissions  from 'expo-permissions';
import * as Location from 'expo-location';
import TripPlanSearch from '../../components/TripPlanSearch';
import { MaterialIcons, Ionicons } from '@expo/vector-icons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { AntDesign } from '@expo/vector-icons'; 
import { FontAwesome5 } from '@expo/vector-icons'; 
import Constants from 'expo-constants';

const NavigationScreen = ({ navigation }) => {
    const [location, setLocation] = useState(true)
    const [truckStop, setTruckStop] = useState(false)
    const [tripPlan, setTripPlan] = useState(false)
    const [stil1, setStil1] = useState({borderBottomWidth:6, borderBottomColor:'#cbae00'})
    const [stil2, setStil2] = useState({borderBottomColor:'white', borderBottomWidth:3})
    const [stil3, setStil3] = useState({borderBottomColor:'white', borderBottomWidth:3})

    return(
        <View style={{width:Dimensions.get('window').width, height:Math.round(Dimensions.get('window').height), alignSelf:'center', marginTop:Constants.statusBarHeight, alignItems:'center' }}>
            <View style={{flexDirection:'row', width:'100%',  height:'8%'}}>
                <View style={{...styles.btnTop, borderBottomWidth:stil1.borderBottomWidth, borderBottomColor:stil1.borderBottomColor}}>
                        <TouchableOpacity onPress={() => {
                            setLocation(true), 
                            setStil1({borderBottomWidth:6, borderBottomColor:'#cbae00'})
                            setStil2({borderBottomColor:'white', borderBottomWidth:3})
                            setStil3({borderBottomColor:'white', borderBottomWidth:3})
                            }}>
                            <View style={{width:'100%', height:'100%', justifyContent:'center', alignItems:'center'}}>
                                <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:25, color:'#484848'}}>Locations</Text>
                            </View>
                        </TouchableOpacity>
                </View>
                <View style={{...styles.btnTop ,borderBottomColor:stil2.borderBottomColor, borderBottomWidth:stil2.borderBottomWidth}}>
                        <TouchableOpacity onPress={() => {
                            setTruckStop(true)
                            setLocation(false)
                            setTripPlan(false)
                            setStil2({borderBottomWidth:6, borderBottomColor:'#cbae00'})
                            setStil1({borderBottomColor:'white', borderBottomWidth:3})
                            setStil3({borderBottomColor:'white', borderBottomWidth:3})
                            }}>
                            <View style={{width:'100%', height:'100%', justifyContent:'center', alignItems:'center'}}>
                                <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:25, color:'#484848', alignSelf:'center'}}>Truck Stops</Text>
                            </View>   
                        </TouchableOpacity>
                </View >
                <View style={{...styles.btnTop ,borderBottomColor:stil3.borderBottomColor, borderBottomWidth:stil3.borderBottomWidth}}>
                        <TouchableOpacity onPress={() => {
                            setTripPlan(true), 
                            setLocation(false), 
                            setTruckStop(false)
                            setStil2({borderBottomColor:'white', borderBottomWidth:3})
                            setStil1({borderBottomColor:'white', borderBottomWidth:3})
                            setStil3({borderBottomWidth:6, borderBottomColor:'#cbae00'})
                            }}>
                            <View style={{width:'100%', height:'100%',justifyContent:'center', alignItems:'center'}}>
                                <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:25, color:'#484848', alignSelf:'center'}}>Trip Plan</Text>
                            </View>
                        </TouchableOpacity>
                </View>
            </View>
            <View style={{width:'100%', height:'100%'}}>
            {location ? <Locations navigation={navigation}/> 
            :truckStop ? <TruckStops />
            :<TripPlan navigation={navigation}/>}
            </View>

            
        </View>
    )
}

const Locations = ({ navigation }) => {

    const [latitude, setLatitude] = useState(0)
    const [longitude, setLongitude] = useState(0)
    const [errorMessage, setErrorMessage] = useState('')
    const [lokacijaPozadina, setLokacijaPozadina] = useState('')


    useEffect(() => {
        _getLocationAsync();
        // _lokacijaUPozadini();
    })
    
    const _getLocationAsync = async () => {
        const { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            setErrorMessage('Permission to access location was denied')
        }
        
        let location = await Location.getCurrentPositionAsync({});
        setLatitude(location.coords.latitude);
        setLongitude(location.coords.longitude)
      };

    // const _lokacijaUPozadini = async () => {
    //     const { status } = await Location.requestPermissionsAsync();
    //     if (status === 'granted'){
    //           let lokacija = await Location.startLocationUpdatesAsync(LOCATION_TASK_NAME, {
    //               accuracy: Location.Accuracy.BestForNavigation,
    //               timeInterval:2000,
    //               distanceInterval:1,
    //               showsBackgroundLocationIndicator:true,
    //               foregroundService:{
    //                   notificationTitle:'navigacija',
    //                   notificationBody:'gde se nalazim'
    //               },
    //               pausesUpdatesAutomatically:false
    //           });
    //         //   console.log(JSON.stringify(lokacija))
    //       }
    //   }

      //console.log(lokacijaPozadina)

      

    return(
        <View style={{width:'100%', height:'100%', alignItems:'center'}}>
            <NavigationSearch
            navigation={navigation}
            />
            <View style={styles.mapView}>
                <MapView
                provider={PROVIDER_GOOGLE}
                region={{
                    latitude: latitude,
                    longitude: longitude,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421
                }}
                style={styles.mapStyle}
                showsUserLocation={true}
                >
                    <Marker
                    coordinate={{ latitude: 37.7825259, longitude: -122.4321431}}
                    title={'San Francisco'}
                    >
                        <Callout>
                            <Text>An interesting city</Text>
                        </Callout>
                    </Marker>
                
                </MapView>
            </View>
        </View>
    )
}



const TruckStops = () => {
    return(
        <View>
        <View style={styles.truckStop}>
            <View style={{width:'65%', height:'100%'}}>
                <View style={{flexDirection:'row', padding:wp('2%'), alignItems:'center'}}>
                    <AntDesign name="heart" size={30} color="red" /> 
                    <Text style={styles.tsHead}>Travel Stop #606</Text>
                </View>
                <Text style={{fontSize:wp('4%'), paddingLeft:wp('2%')}}>South Holland, IL</Text>
                <Text style={{fontSize:wp('4%'), paddingLeft:wp('2%')}}>Hardee's</Text>
            </View>
            <View style={{width:'35%', height:'100%', borderColor:'red', padding:wp('2%')}}>
                <Text style={{fontFamily:'Myriad-Pro-Condensed', fontSize:wp('4%'), color:'#808080', fontStyle:'italic'}}>30.8mi</Text>
                <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:wp('10%'), color:'#484848' }}>$3.04</Text>
                <Text style={{fontFamily:'Myriad-Pro-Condensed', fontSize:wp('7%'), color:'#808080'}}>DIESEL #2</Text>
            </View>
        </View>
        <View style={styles.truckStop}>
            <View style={{width:'65%',  height:'100%'}}>
                <View style={{flexDirection:'row', padding:wp('2%'), alignItems:'center'}}>
                <FontAwesome5 name="parking" size={30} color="blue" /> 
                    <Text style={styles.tsHead}>Parking Area #28</Text>
                </View>
                <Text style={{fontSize:wp('4%'), paddingLeft:wp('2%')}}>South Holland, IL</Text>
                <Text style={{fontSize:wp('4%'), paddingLeft:wp('2%')}}>Hardee's</Text>
            </View>
            <View style={{width:'35%', height:'100%', borderColor:'red', padding:wp('2%')}}>
                <Text style={{fontFamily:'Myriad-Pro-Condensed', fontSize:wp('4%'), color:'#808080', fontStyle:'italic'}}>26.7mi</Text>
                <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:wp('10%'), color:'#484848' }}>$2.99</Text>
                <Text style={{fontFamily:'Myriad-Pro-Condensed', fontSize:wp('7%'), color:'#808080'}}>DIESEL #2</Text>
            </View>
        </View>
        </View>
    )
}

const TripPlan = ({ navigation }) => {
    const [currentLocation, setCurrentLocation] = useState('')
    const [enterLocation, setEnterLocation] = useState('')

    return(
        <View style={{flex:1, alignItems:'center'}}>
            <View style={{alignItems:'center', width:'100%', marginTop:40}}>
           <TripPlanSearch 
           vrednost={currentLocation}
           postaviVrednost={() => setCurrentLocation()}
           boja='#26a401'
           nav={true}
           placeholder='Current Location'
           />
           <TripPlanSearch 
           vrednost={enterLocation}
           postaviVrednost={() => setEnterLocation()}
           boja='#a40101'
           placeholder='Enter Location'
           />
        </View>
           <View style={{flexDirection:'column', justifyContent:'center', width:'70%', height:'20%', marginTop:hp('8%')}}>
               <View style={{flexDirection:'row', width:'100%', height:'50%'}}>
                    <TouchableOpacity style={{width:'50%'}}>
                        <View style={styles.btnStyle}>
                        <MaterialIcons
                        name='add-location'
                        size={40}
                        color='#ffffff'
                        />
                        <Text style={styles.btnText}>Add Stop</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity 
                    style={{width:'50%'}}
                    onPress={() => navigation.navigate('Filter')}
                    >
                        <View style={{...styles.btnStyle, alignSelf:'flex-end'}}> 
                        <Ionicons
                        name='md-options'
                        size={40}
                        color='#ffffff'
                        />
                        <Text style={styles.btnText}>Filter</Text>
                        </View>
                    </TouchableOpacity>
               </View>
               <View style={{flexDirection:'row', width:'100%', height:'50%'}}> 
                    <TouchableOpacity style={{width:'50%'}}>
                        <View style={styles.btnStyle}>
                        <MaterialIcons
                        name='clear'
                        size={40}
                        color='#ffffff'
                        />
                        <Text style={styles.btnText}>Clear All</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'50%'}}>
                        <View style={{...styles.btnStyle, alignSelf:'flex-end', backgroundColor:'#cead02'}}>
                            <Text style={styles.btnText}>Get Trip Plan</Text>
                        </View>
                    </TouchableOpacity>
               </View>
           </View>
        </View>
    )
}

const styles = StyleSheet.create({
    btnTop:{
        flex:1,
        height:'100%',
    },
    mapStyle: {
        width: '100%',
        height: '100%',
      },
    mapView:{
        height:'100%',
        width:'100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop:3
    },
    btnStyle:{
        justifyContent:'center',
        flexDirection:'row',
        alignItems:'center',
        width:'95%',
        height:'80%',
        backgroundColor:'#707070',
        borderRadius:10

    },
    btnText:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:24,
        color:'#ffffff',
        paddingLeft:6
    },
    truckStop:{
        width:wp('100%'),
        height:hp('20%'),
        flexDirection:'row',
        backgroundColor:'#ececec',
        marginBottom:hp('1%')
    },
    tsHead:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:wp('6%'),
        paddingHorizontal:wp('2%'),
        color:'#484848'
    }
});

export default NavigationScreen;