import React, { useContext, useState, useEffect } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Dimensions, Image } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { AntDesign } from '@expo/vector-icons'; 
import { FontAwesome5 } from '@expo/vector-icons'; 
import Constants from 'expo-constants';

const Locations = ({ navigation }) => {

    return(
        <View style={{marginTop:Constants.statusBarHeight}}>
        <View style={styles.topBar}>
            <Text style={styles.filter}>
                Locations
            </Text>
        </View>
        <View>
            <View style={styles.truckStop}>
            <View style={{width:'65%', height:'100%'}}>
                <View style={{flexDirection:'row', padding:wp('2%'), alignItems:'center'}}>
                    <AntDesign name="heart" size={30} color="red" /> 
                    <Text style={styles.tsHead}>Travel Stop #606</Text>
                </View>
                <Text style={{fontSize:wp('4%'), paddingLeft:wp('2%')}}>South Holland, IL</Text>
                <Text style={{fontSize:wp('4%'), paddingLeft:wp('2%')}}>Hardee's</Text>
            </View>
            <View style={{width:'35%', height:'100%', borderColor:'red', padding:wp('2%')}}>
                <Text style={{fontFamily:'Myriad-Pro-Condensed', fontSize:wp('4%'), color:'#808080', fontStyle:'italic'}}>30.8mi</Text>
                <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:wp('10%'), color:'#484848' }}>$3.04</Text>
                <Text style={{fontFamily:'Myriad-Pro-Condensed', fontSize:wp('7%'), color:'#808080'}}>DIESEL #2</Text>
            </View>
        </View>
        <View style={styles.truckStop}>
            <View style={{width:'65%',  height:'100%'}}>
                <View style={{flexDirection:'row', padding:wp('2%'), alignItems:'center'}}>
                <FontAwesome5 name="parking" size={30} color="blue" /> 
                    <Text style={styles.tsHead}>Parking Area #28</Text>
                </View>
                <Text style={{fontSize:wp('4%'), paddingLeft:wp('2%')}}>South Holland, IL</Text>
                <Text style={{fontSize:wp('4%'), paddingLeft:wp('2%')}}>Hardee's</Text>
            </View>
            <View style={{width:'35%', height:'100%', borderColor:'red', padding:wp('2%')}}>
                <Text style={{fontFamily:'Myriad-Pro-Condensed', fontSize:wp('4%'), color:'#808080', fontStyle:'italic'}}>26.7mi</Text>
                {/* <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:wp('10%'), color:'#484848' }}>$2.99</Text>
                <Text style={{fontFamily:'Myriad-Pro-Condensed', fontSize:wp('7%'), color:'#808080'}}>DIESEL #2</Text> */}
            </View>
        </View>
        <View style={styles.truckStop}>
            <View style={{width:'65%', height:'100%'}}>
                <View style={{flexDirection:'row', padding:wp('2%'), alignItems:'center'}}>
                    <AntDesign name="heart" size={30} color="red" /> 
                    <Text style={styles.tsHead}>Travel Stop #606</Text>
                </View>
                <Text style={{fontSize:wp('4%'), paddingLeft:wp('2%')}}>South Holland, IL</Text>
                <Text style={{fontSize:wp('4%'), paddingLeft:wp('2%')}}>Hardee's</Text>
            </View>
            <View style={{width:'35%', height:'100%', borderColor:'red', padding:wp('2%')}}>
                <Text style={{fontFamily:'Myriad-Pro-Condensed', fontSize:wp('4%'), color:'#808080', fontStyle:'italic'}}>26.7mi</Text>
                <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:wp('10%'), color:'#484848' }}>$2.99</Text>
                <Text style={{fontFamily:'Myriad-Pro-Condensed', fontSize:wp('7%'), color:'#808080'}}>DIESEL #2</Text>
            </View>
        </View>
        <View style={styles.truckStop}>
            <View style={{width:'65%', height:'100%'}}>
                <View style={{flexDirection:'row', padding:wp('2%'), alignItems:'center'}}>
                    <Image source={require('../../img/pfj_logo2x-1_0.png')} style={{width:'13%',height:'100%' , resizeMode:'contain'}}/> 
                    <Text style={styles.tsHead}>Travel Stop #606</Text>
                </View>
                <Text style={{fontSize:wp('4%'), paddingLeft:wp('2%')}}>South Holland, IL</Text>
                <Text style={{fontSize:wp('4%'), paddingLeft:wp('2%')}}>Hardee's</Text>
            </View>
            <View style={{width:'35%', height:'100%', borderColor:'red', padding:wp('2%')}}>
                <Text style={{fontFamily:'Myriad-Pro-Condensed', fontSize:wp('4%'), color:'#808080', fontStyle:'italic'}}>3.8mi</Text>
                <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:wp('10%'), color:'#484848' }}>$3.00</Text>
                <Text style={{fontFamily:'Myriad-Pro-Condensed', fontSize:wp('7%'), color:'#808080'}}>DIESEL #2</Text>
            </View>
        </View>
        </View>
    </View>
        
    )
}


const styles = StyleSheet.create({
    topBar:{
        backgroundColor:'white',
        height:hp('10%'),
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    filter:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:wp('7%'),
        paddingLeft:wp('5%'),
        color:'#484848'
    },
    apply:{
        fontFamily:'Myriad-Pro-Condensed',
        fontSize:wp('6%'),
        paddingRight:wp('5%')
    },
    truckStop:{
        width:wp('100%'),
        height:hp('20%'),
        flexDirection:'row',
        backgroundColor:'#ececec',
        marginBottom:hp('0.5%')
    },
    tsHead:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:wp('6%'),
        paddingHorizontal:wp('2%'),
        color:'#484848'
    }
});

export default Locations;