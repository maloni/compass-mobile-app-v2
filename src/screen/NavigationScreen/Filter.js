import React, { useContext, useState, useEffect } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';

const Filter = ({ navigation }) => {

    return(
        <View style={{marginTop:Constants.statusBarHeight}}>
            <View style={styles.topBar}>
                <Text style={styles.filter}>
                    Filters
                </Text>
                <TouchableOpacity>
                    <Text style={styles.apply}>
                        Apply
                    </Text>
                </TouchableOpacity>
            </View>
            <View>

            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    topBar:{
        backgroundColor:'white',
        height:hp('10%'),
        borderWidth:1,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },
    filter:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:wp('7%'),
        paddingLeft:wp('5%'),
        color:'#484848'
    },
    apply:{
        fontFamily:'Myriad-Pro-Condensed',
        fontSize:wp('7%'),
        paddingRight:wp('5%'),
        color:'#484848'
    }
});

export default Filter;