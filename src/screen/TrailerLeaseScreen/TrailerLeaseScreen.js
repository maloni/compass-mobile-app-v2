import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity, SafeAreaView, ScrollView,Platform} from 'react-native';
import  CTLlogo  from '../../components/CTLlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import LoveCompass from '../../components/LoveCompass';
import Modal from "react-native-modal";
import { WebView } from 'react-native-webview';


const EquipmentFinanceScreen = ({ navigation }) => {

    const [apply, setApply] = useState(false)


    return(
    <SafeAreaView style={{width:'100%', height:'100%'}}>
    <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
        <ScrollView      
            contentContainerStyle={styles.container}           
            automaticallyAdjustContentInsets={true}
            showsVerticalScrollIndicator={false}
            bounces={true}
        >
                <CTLlogo />
                <TouchableOpacity 
                style={{...styles.btn, marginTop:hp('4%')}}
                    onPress={() => navigation.navigate('About')}>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:hp('1%')}}>ABOUT</Text>
                    </TouchableOpacity>
                    <TouchableOpacity  
                    style={styles.btn}
                    onPress={() => navigation.navigate('Inventory')}>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:hp('1%'), textAlign:'center'}}>INVENTORY</Text>
                    </TouchableOpacity>
                    <TouchableOpacity  
                    style={styles.btn}
                    onPress={() => setApply(true)}>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:hp('1%'), textAlign:'center'}}>APPLY TO LEASE A TRAILER</Text>
                    </TouchableOpacity>
                    <TouchableOpacity  
                    style={{...styles.btn, borderColor:'#da212f'}}
                    onPress={() => navigation.navigate('Refer')}>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:hp('1%'), textAlign:'center'}}>REFER A FRIEND</Text>
                    </TouchableOpacity>
                    <TouchableOpacity  
                    style={{...styles.btn, borderColor:'#da212f'}}
                    onPress={() => navigation.navigate('Locations')}>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:hp('1%'), textAlign:'center'}}>LOCATIONS</Text>
                    </TouchableOpacity>
                <Text style={{...styles.txt, marginTop:hp('1%')}}>Have shortage of equipment?</Text>
                <View style={{flexDirection:'row', alignSelf:'center'}}>
                <Text style={{...styles.txt, color:'#da212f'}}>Click here </Text><Text style={styles.txt}>to apply and start to</Text></View>
                <Text style={{...styles.txt, marginBottom:hp('2%')}}>grow your business today!</Text>                         
                <View style={{alignSelf:'center', width:wp('80%'), position:'absolute', bottom:hp('2%')}}>
                    <LoveCompass />
                </View>
                <Modal
                    animationType='slideInUp'
                    isVisible={apply}
                    onRequestClose={() => setApply(false)}
                    onBackdropPress={() => setApply(false)}
                    backdropOpacity={0.8}
                    hasBackdrop={true}
                    backdropColor='black'
                >
                    <WebView 
                    source={{ uri: 'http://www.compasslease.com/leaseapp/leaseapp.html' }} 
                    startInLoadingState={true}
                    scalesPageToFit={true}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    style={{width:wp('80%'), height:hp('80%'), backgroundColor:'white', borderRadius:10, alignItems:'center', alignSelf:'center', flex:1}}
                    >
                    </WebView>
                </Modal>
            </ScrollView>
    </KeyboardAwareView>
    </SafeAreaView>
    )
};


EquipmentFinanceScreen.navigationOptions = ({ navigation }) => {
    return{
        header: null,
    }
};

const styles = StyleSheet.create({
    container: {
        width:wp('90%'),
        marginHorizontal:'5%',
        height:hp('90%'),
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent',
        flex:1
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    },
    btn:{
        backgroundColor:'white',
        marginVertical:hp('1%'),
        width:'80%',
        alignSelf:'center',
        borderWidth:3, 
        borderColor:'#da212f', 
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
        width: 10,
        height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#484848',
        elevation: 8,
        
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default EquipmentFinanceScreen;