import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, SafeAreaView, ScrollView,Platform} from 'react-native';
import  CTLlogo  from '../../components/CTLlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import LoveCompass from '../../components/LoveCompass';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { Linking } from 'expo';
import { FontAwesome, Ionicons, MaterialCommunityIcons } from '@expo/vector-icons';

const TrailerLeaseLocations = ({ navigation }) => {

    return(
    <SafeAreaView style={{width:'100%', height:'100%'}}>
    <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
        <ScrollView      
            contentContainerStyle={styles.container}           
            automaticallyAdjustContentInsets={true}
            showsVerticalScrollIndicator={false}
            bounces={true}
        >
                <CTLlogo />
                <View style={{alignSelf:'center', alignItems:'flex-start', padding:hp('2%'), backgroundColor:'#F8F8F8', borderRadius:10, marginTop:hp('3%')}}>
                    <MapView
                        provider={PROVIDER_GOOGLE}
                        style={{width:wp('80%'), height:hp('25%')}}
                        initialRegion={{
                        latitude: 41.797645,
                        longitude: -87.823988,
                        latitudeDelta: 0.0222,
                        longitudeDelta: 0.0421,
                        }}
                    >
                        <Marker 
                        coordinate={{
                            latitude: 41.797645,
                            longitude: -87.823988
                        }}
                        title='Compass Lease'
                        onPress={() => Linking.openURL('https://www.google.com/maps/place/Compass+Lease/@41.7789232,-87.8594464,12.5z/data=!4m12!1m6!3m5!1s0x0:0xe92b5cd9530bf1a6!2sCompass+Lease!8m2!3d41.7976165!4d-87.8239918!3m4!1s0x880e365d4eaa5c6f:0xe92b5cd9530bf1a6!8m2!3d41.7976165!4d-87.8239918?hl=en-US')}
                        />
                    </MapView>
                    <View style={{flexDirection:'row', marginRight:wp('1%'), alignItems:'center', marginTop:hp('2%')}}>
                        <FontAwesome name='road' size={wp('6%')} color='#484848' />
                            <View style={{flexDirection:'column', paddingLeft:wp('2%')}}>
                                <Text style={styles.txt}>5150 S. Lawndale Ave, McCook, IL 60525 </Text>
                            </View>
                    </View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Ionicons name='md-call' size={wp('7%')} color='#484848' /><Text style={{...styles.txt, paddingLeft:wp('4%')}}>855-830-0020</Text>
                    </View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <MaterialCommunityIcons name='fax' size={wp('7%')} color='#484848' /><Text style={{...styles.txt, paddingLeft:wp('2%')}}>708-584-0419</Text>
                    </View>
                </View>

                <View style={{alignSelf:'center', alignItems:'flex-start', padding:hp('2%'), backgroundColor:'#F8F8F8', borderRadius:10, marginTop:hp('3%')}}>
                    <MapView
                        provider={PROVIDER_GOOGLE}
                        style={{width:wp('80%'), height:hp('25%')}}
                        initialRegion={{
                        latitude: 32.812960,
                        longitude: -96.889390,
                        latitudeDelta: 0.0222,
                        longitudeDelta: 0.0421,
                        }}
                    >
                        <Marker 
                        coordinate={{
                            latitude: 32.812960,
                            longitude: -96.889390
                        }}
                        title='Compass Lease'
                        onPress={() => Linking.openURL('https://www.google.com/maps/place/Compass+Lease/@41.7789232,-87.8594464,12.5z/data=!4m12!1m6!3m5!1s0x0:0xe92b5cd9530bf1a6!2sCompass+Lease!8m2!3d41.7976165!4d-87.8239918!3m4!1s0x880e365d4eaa5c6f:0xe92b5cd9530bf1a6!8m2!3d41.7976165!4d-87.8239918?hl=en-US')}
                        />
                    </MapView>
                    <View style={{flexDirection:'row', marginRight:wp('1%'), alignItems:'center', marginTop:hp('2%')}}>
                        <FontAwesome name='road' size={wp('6%')} color='#484848' />
                            <View style={{flexDirection:'column', paddingLeft:wp('2%')}}>
                                <Text style={styles.txt}>4221 Halifax Street Dallas, TX 75247</Text>
                            </View>
                    </View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Ionicons name='md-call' size={wp('7%')} color='#484848' /><Text style={{...styles.txt, paddingLeft:wp('4%')}}>888-223-5993</Text>
                    </View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <MaterialCommunityIcons name='fax' size={wp('7%')} color='#484848' /><Text style={{...styles.txt, paddingLeft:wp('2%')}}>863-508-6213</Text>
                    </View>
                </View>

                <View style={{alignSelf:'center', alignItems:'flex-start', padding:hp('2%'), backgroundColor:'#F8F8F8', borderRadius:10, marginTop:hp('3%')}}>
                    <MapView
                        provider={PROVIDER_GOOGLE}
                        style={{width:wp('80%'), height:hp('25%')}}
                        initialRegion={{
                        latitude: 42.222821,
                        longitude: -83.322889,
                        latitudeDelta: 0.0222,
                        longitudeDelta: 0.0421,
                        }}
                    >
                        <Marker 
                        coordinate={{
                            latitude: 42.222821,
                            longitude: -83.322889
                        }}
                        title='Compass Lease'
                        onPress={() => Linking.openURL('https://www.google.com/maps/place/Compass+Lease/@41.7789232,-87.8594464,12.5z/data=!4m12!1m6!3m5!1s0x0:0xe92b5cd9530bf1a6!2sCompass+Lease!8m2!3d41.7976165!4d-87.8239918!3m4!1s0x880e365d4eaa5c6f:0xe92b5cd9530bf1a6!8m2!3d41.7976165!4d-87.8239918?hl=en-US')}
                        />
                    </MapView>
                    <View style={{flexDirection:'row', marginRight:wp('1%'), alignItems:'center', marginTop:hp('2%')}}>
                        <FontAwesome name='road' size={wp('6%')} color='#484848' />
                            <View style={{flexDirection:'column', paddingLeft:wp('2%')}}>
                                <Text style={styles.txt}>29171 Goddard Road Romulus, MI 48174</Text>
                            </View>
                    </View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Ionicons name='md-call' size={wp('7%')} color='#484848' /><Text style={{...styles.txt, paddingLeft:wp('4%')}}>‎734-822-6460</Text>
                    </View>
                </View>

                <View style={{alignSelf:'center', alignItems:'flex-start', padding:hp('2%'), backgroundColor:'#F8F8F8', borderRadius:10, marginTop:hp('3%')}}>
                    <MapView
                        provider={PROVIDER_GOOGLE}
                        style={{width:wp('80%'), height:hp('25%')}}
                        initialRegion={{
                        latitude: 28.153601,
                        longitude: -81.871450,
                        latitudeDelta: 0.0222,
                        longitudeDelta: 0.0421,
                        }}
                    >
                        <Marker 
                        coordinate={{
                            latitude: 28.153601,
                            longitude: -81.871450
                        }}
                        title='Compass Lease'
                        onPress={() => Linking.openURL('https://www.google.com/maps/place/Compass+Lease/@41.7789232,-87.8594464,12.5z/data=!4m12!1m6!3m5!1s0x0:0xe92b5cd9530bf1a6!2sCompass+Lease!8m2!3d41.7976165!4d-87.8239918!3m4!1s0x880e365d4eaa5c6f:0xe92b5cd9530bf1a6!8m2!3d41.7976165!4d-87.8239918?hl=en-US')}
                        />
                    </MapView>
                    <View style={{flexDirection:'row', marginRight:wp('1%'), alignItems:'center', marginTop:hp('2%')}}>
                        <FontAwesome name='road' size={wp('6%')} color='#484848' />
                            <View style={{flexDirection:'column', paddingLeft:wp('2%')}}>
                                <Text style={styles.txt}>8514 North State Rd 33 Lakeland, FL 33809</Text>
                            </View>
                    </View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Ionicons name='md-call' size={wp('7%')} color='#484848' /><Text style={{...styles.txt, paddingLeft:wp('4%')}}>863-984-0200</Text>
                    </View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <MaterialCommunityIcons name='fax' size={wp('7%')} color='#484848' /><Text style={{...styles.txt, paddingLeft:wp('2%')}}>863-984-0800</Text>
                    </View>
                </View>

                <View style={{alignSelf:'center', alignItems:'flex-start', padding:hp('2%'), backgroundColor:'#F8F8F8', borderRadius:10, marginTop:hp('3%')}}>
                    <MapView
                        provider={PROVIDER_GOOGLE}
                        style={{width:wp('80%'), height:hp('25%')}}
                        initialRegion={{
                        latitude: 34.053573,
                        longitude: -117.370562,
                        latitudeDelta: 0.0222,
                        longitudeDelta: 0.0421,
                        }}
                    >
                        <Marker 
                        coordinate={{
                            latitude: 34.053573,
                            longitude: -117.370562
                        }}
                        title='Compass Lease'
                        onPress={() => Linking.openURL('https://www.google.com/maps/place/Compass+Lease/@41.7789232,-87.8594464,12.5z/data=!4m12!1m6!3m5!1s0x0:0xe92b5cd9530bf1a6!2sCompass+Lease!8m2!3d41.7976165!4d-87.8239918!3m4!1s0x880e365d4eaa5c6f:0xe92b5cd9530bf1a6!8m2!3d41.7976165!4d-87.8239918?hl=en-US')}
                        />
                    </MapView>
                    <View style={{flexDirection:'row', marginRight:wp('1%'), alignItems:'center', marginTop:hp('2%')}}>
                        <FontAwesome name='road' size={wp('6%')} color='#484848' />
                            <View style={{flexDirection:'column', paddingLeft:wp('2%')}}>
                                <Text style={styles.txt}>125 W Bryant Street Bloomington, CA 92316</Text>
                            </View>
                    </View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Ionicons name='md-call' size={wp('7%')} color='#484848' /><Text style={{...styles.txt, paddingLeft:wp('4%')}}>909-361-4471</Text>
                    </View>
                </View>

                <View style={{alignSelf:'center', alignItems:'flex-start', padding:hp('2%'), backgroundColor:'#F8F8F8', borderRadius:10, marginTop:hp('3%')}}>
                    <MapView
                        provider={PROVIDER_GOOGLE}
                        style={{width:wp('80%'), height:hp('25%')}}
                        initialRegion={{
                        latitude: 33.614905,
                        longitude: -84.311249,
                        latitudeDelta: 0.0222,
                        longitudeDelta: 0.0421,
                        }}
                    >
                        <Marker 
                        coordinate={{
                            latitude: 33.614905,
                            longitude: -84.311249
                        }}
                        title='Compass Lease'
                        onPress={() => Linking.openURL('https://www.google.com/maps/place/Compass+Lease/@41.7789232,-87.8594464,12.5z/data=!4m12!1m6!3m5!1s0x0:0xe92b5cd9530bf1a6!2sCompass+Lease!8m2!3d41.7976165!4d-87.8239918!3m4!1s0x880e365d4eaa5c6f:0xe92b5cd9530bf1a6!8m2!3d41.7976165!4d-87.8239918?hl=en-US')}
                        />
                    </MapView>
                    <View style={{flexDirection:'row', marginRight:wp('1%'), alignItems:'center', marginTop:hp('2%')}}>
                        <FontAwesome name='road' size={wp('6%')} color='#484848' />
                            <View style={{flexDirection:'column', paddingLeft:wp('2%')}}>
                                <Text style={styles.txt}>5166 John G Glover Ct. Forest Park, GA 30297</Text>
                            </View>
                    </View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Ionicons name='md-call' size={wp('7%')} color='#484848' /><Text style={{...styles.txt, paddingLeft:wp('4%')}}>470-317-2770</Text>
                    </View>
                    
                </View>

                <View style={{justifyContent:'flex-end', alignSelf:'center', width:'80%', marginTop:hp('3%') }}>
                    <LoveCompass />
                </View>
                    
            </ScrollView>
    </KeyboardAwareView>
    </SafeAreaView>
    )
};


TrailerLeaseLocations.navigationOptions = ({ navigation }) => {
    return{
        header: null,
    }
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        paddingBottom: wp('7%')
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:hp('2%'),
        textAlign:'justify'
    },
    btn:{
        marginVertical:hp('1%'),
    width:'80%',
    alignSelf:'center',
    borderWidth:3, 
    borderColor:'#da212f', 
    borderRadius:10,
    backgroundColor:'white',
    shadowOffset: {
      width: 10,
      height: 6,
    },
    shadowOpacity: 0.30,
    shadowRadius: 12,
    shadowColor:'#484848',
    elevation: 8,
    
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default TrailerLeaseLocations;