import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, SafeAreaView, ScrollView, Platform, FlatList, Image} from 'react-native';
import  CTLlogo  from '../../components/CTLlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import TrailerList from '../../components/TrailerList';

const TrailerLeaseInventory = ({ navigation }) => {

    return(
    <SafeAreaView style={{width:'100%', height:'100%'}}>
        <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
            <ScrollView
            contentContainerStyle={styles.container}           
            automaticallyAdjustContentInsets={true}
            showsVerticalScrollIndicator={false}
            bounces={true}
            >
                <CTLlogo />
                <TrailerList 
                title='Vanguard VXP Composite'
                desc={`Vanguard's VXP Composite trailer successfully integrates all of our standard premium features and galvanized components with industry-proven composite technology for a trailer that truly delivers a lower total cost of operation. Composite trailer construction has been around for years. Now, Vanguard has added the premium components to make it that much better.Contact us for more details`}
                imge={require('../../img/VanguardVXP.png')}
                nav={() => navigation.navigate('TrailerLeaseContact', {title : 'Vanguard VXP Composite'})}
                />
                <TrailerList 
                title='Vanguard Cool Globe R800 Refrigerated Trailer'
                desc={`At Vanguard, building durable and reliable trailers isn't an option — it's the very philosophy that goes into our VIP 4000 trailers. Equipped with our standard premium features and fully galvanized components, the VIP 4000 trailer provides better protection from the elements — extending and maximizing the life of your trailer.`}
                imge={require('../../img/VanguardCoolGlobe.png')}
                nav={() => navigation.navigate('TrailerLeaseContact', {title : 'Vanguard Cool Globe R800 Refrigerated Trailer'})}
                />
                <TrailerList 
                title='Utility 4000D - X Composite'
                desc={`The thin-wall 4000D-X Composite® dry van is the most productive composite trailer available today. Our polyurethane foam core composite side walls optimize strength and lower tare weight.`}
                imge={require('../../img/Utility4000D.png')}
                nav={() => navigation.navigate('TrailerLeaseContact', {title : 'Utility 4000D - X Composite'})}
                />
                <TrailerList 
                title='Utility 4000A Flatbed'
                desc={`Built with plasma-cut one-piece main beam webs, the light weight 4000A® is a high strength aluminum/steel combo flatbed with an 80,000 lbs. distributed-load rating.`}
                imge={require('../../img/flatbed.png')}
                nav={() => navigation.navigate('TrailerLeaseContact', {title : 'Utility 4000A Flatbed'})}
                />
                <TrailerList 
                title='Utility Lift Gate VS2DX Trailer'
                desc={`Convenient for loading small to medium loads where no loading/unloading ramp is available.Contact us for more details`}
                imge={require('../../img/UtilityLift.jpg')}
                nav={() => navigation.navigate('TrailerLeaseContact', {title : 'Utility Lift Gate VS2DX Trailer'})}
                />
                <TrailerList 
                title='Great Dane Champion CP'
                desc={`Reliability and Value that Drive Business Forward | The Champion SE dry freight vans combine innovation and affordability for an unsurpassed return on investment. The SE offers value, long-term quality, and increased productivity and reduced maintenance.`}
                imge={require('../../img/GreatDane.jpg')}
                nav={() => navigation.navigate('TrailerLeaseContact', {title : 'Great Dane Champion CP'})}
                />
                <TrailerList 
                title='Wabash DuraPlate'
                desc={`Introduced in 1996, the DuraPlate® van revolutionized the trucking industry. Designed for a wide range of freight hauls, our conventional DuraPlate vans provide maximum cube capacity, exceptional durability, lightweight performance, and resale values among the best in the industry.`}
                imge={require('../../img/WabashDuraPlate.png')}
                nav={() => navigation.navigate('TrailerLeaseContact', {title : 'Wabash DuraPlate'})}
                />
                <TrailerList 
                title='Hyndai HT Composite'
                desc={`The HT Composite is constructed with durable composite plate sidewalls and steel logistic slotted stiffeners. No single sidewall material can be lightweight while providing the same amount of strength.`}
                imge={require('../../img/HyndaiHT.png')}
                nav={() => navigation.navigate('TrailerLeaseContact', {title : 'Hyndai HT Composite'})}
                />
                <TrailerList 
                title='20-40 Tri-Axle Chassis'
                desc={`Top brand OEM parts of the field used ensuring safety and durability. Upper coupler is approved by AAR testing. Patented compact retractable twist lock with integrated lock housing for easy operation and durability purposes.`}
                imge={require('../../img/20-40.png')}
                nav={() => navigation.navigate('TrailerLeaseContact', {title : '20-40 Tri-Axle Chassis'})}
                />
            </ScrollView>
        </KeyboardAwareView>
    </SafeAreaView>
    )
};


TrailerLeaseInventory.navigationOptions = ({ navigation }) => {
    return{
        header: null,
    }
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        paddingBottom: wp('7%')
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center',
        padding:hp('2%'),
        textAlign:'justify'
    },
    btn:{
    marginVertical:hp('1%'),
    width:'80%',
    alignSelf:'center',
    borderWidth:3, 
    borderColor:'#da212f', 
    borderRadius:10,
    backgroundColor:'white',
    shadowOffset: {
      width: 10,
      height: 6,
    },
    shadowOpacity: 0.30,
    shadowRadius: 12,
    shadowColor:'#484848',
    elevation: 8,
    
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default TrailerLeaseInventory;