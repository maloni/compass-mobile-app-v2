import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, SafeAreaView, ScrollView,Platform} from 'react-native';
import  CTLlogo  from '../../components/CTLlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import LoveCompass from '../../components/LoveCompass';
import {Feather, MaterialIcons} from '@expo/vector-icons';

const TrailerLeaseAbout = ({ navigation }) => {


    return(
    <SafeAreaView style={{width:'100%', height:'100%'}}>
    <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
        <ScrollView      
            contentContainerStyle={styles.container}           
            automaticallyAdjustContentInsets={true}
            showsVerticalScrollIndicator={false}
            bounces={true}
        >
           <View> 
                <CTLlogo />
                <Text style={{...styles.txt, marginTop:hp('3%')}}>Compass Lease is the leading provider of trailers nationwide. Whether you are interested in a long-term lease or a short-term rental, Compass provides everything from dry vans to reefers. 
            Compass is ready to help you increase your capacity and eliminate overhead. Our focus is premium trailers, state of the art technology, and exceptional customer service for all your rental and leasing needs.</Text>
            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                <TouchableOpacity
                style={{width:wp('30%'), alignItems:'center', marginHorizontal:wp('5%')}}
                >
                    <Feather 
                    name='phone-call'
                    color='#da212f'
                    size={wp('10%')}
                    />
                    <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:wp('7%'), color:'#484848'}}>Call Us</Text>
                </TouchableOpacity>
                <TouchableOpacity
                style={{width:wp('30%'), alignItems:'center',  marginHorizontal:wp('5%')}}
                >
                    <MaterialIcons 
                    name='mail-outline'
                    color='#da212f'
                    size={wp('10%')}
                    />
                    <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:wp('7%'), color:'#484848'}}>Email Us!</Text>
                </TouchableOpacity>
            </View>
                <View style={{justifyContent:'flex-end', alignSelf:'center', width:'80%', marginTop:hp('12%') }}>
                    <LoveCompass />
                </View>
                    
            </View>
            </ScrollView>
    </KeyboardAwareView>
    </SafeAreaView>
    )
};


TrailerLeaseAbout.navigationOptions = ({ navigation }) => {
    return{
        header: null,
    }
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        height:'90%',
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent',
        flex:1
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center',
        padding:hp('2%'),
        textAlign:'justify'
    },
    btn:{
        marginVertical:hp('1%'),
    width:'80%',
    alignSelf:'center',
    borderWidth:3, 
    borderColor:'#da212f', 
    borderRadius:10,
    backgroundColor:'white',
    shadowOffset: {
      width: 10,
      height: 6,
    },
    shadowOpacity: 0.30,
    shadowRadius: 12,
    shadowColor:'#484848',
    elevation: 8,
    
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default TrailerLeaseAbout;