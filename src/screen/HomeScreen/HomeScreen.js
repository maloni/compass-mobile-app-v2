import React, { useState, useEffect, useContext }from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import { WaveIndicator } from 'react-native-indicators';
import * as Font from 'expo-font';
import CompassContext from '../../context/BlogContext';
import HomeIcon from '../../components/HomeIcon';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const HomeScreen = ({ navigation }) => {
    const [loaded, setLoaded] = useState(false);
    const [imgLoad , setImgLoad] = useState(false);

    const { setItaId, itaId } = useContext(CompassContext);
    
    console.log(navigation.router)
    return(
    <View style={{flex:1}}>
         {loaded && imgLoad ? (
            <WaveIndicator size={40} color="#e9ce00"  style={{justifyContent:'center'}}/>
            )
            :
        <View style={{flex:1}}>
            <View style={{width:wp('100%'), height:hp('25%'), alignItems:'center', }}>
                        <Image onLoad={() => setImgLoad(true)} style={{width:'90%', marginHorizontal:'5%', marginTop:'-3%'}} source={require('../../img/CHlogo.png')}  resizeMode='contain'/>
                        <View style={{height:'20%', marginTop:'-3%'}}>
                            <Text style={{fontSize:20, alignSelf:'center', fontFamily:'Myriad-Pro-Bold-Condensed'}}>
                                TOGETHER WE WILL NAVIGATE TO SUCCESS</Text>
                        </View>
            </View>

        <View style={styles.mainContainer}>
            <View style={styles.container}>
            <View style={styles.viewContainer}>
                    <TouchableOpacity 
                    style={styles.buttonContainer}
                    onPress={() => navigation.navigate('PaymentServices')}
                    >
                        <HomeIcon
                        loading={() => setLoaded(true)}
                        img={require('../../img/Appicon1.jpg')}
                        />
                        <Text numberOfLines={2} style={{fontSize:wp('3.2%'),alignSelf:'center', fontFamily:'Myriad-Pro-Bold-Condensed' }}>PAYMENT SERVICES</Text>
                        </TouchableOpacity>
                    <TouchableOpacity
                    style={styles.buttonContainer}
                    onPress={() => navigation.navigate('CFS')}
                    >
                        <HomeIcon 
                        loading={() => setLoaded(true)}
                        img={require('../../img/funding.png')}
                        />
                        <Text numberOfLines={2} style={{fontSize:wp('3.2%'),alignSelf:'center',  fontFamily:'Myriad-Pro-Bold-Condensed', textAlign:'center'}}>FUNDING SOLUTIONS</Text>
                        </TouchableOpacity>
                    <TouchableOpacity
                    onPress={() => navigation.navigate('Fuel')}
                    style={styles.buttonContainer}
                    >
                       <HomeIcon 
                       loading={() => setLoaded(true)}
                        img={require('../../img/fuel.png')}
                        />
                        <Text numberOfLines={2} style={{fontSize:wp('3.2%'),alignSelf:'center', fontFamily:'Myriad-Pro-Bold-Condensed', textAlign:'center' }}>FUEL</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.viewContainer}>
                    <TouchableOpacity 
                    style={{...styles.buttonContainer}}
                    onPress={() => navigation.navigate('Arena')}
                    >
                        <HomeIcon
                        loading={() => setLoaded(true)}
                        img={require('../../img/Arena.png')}
                        />
                        <Text numberOfLines={2} style={{fontSize:wp('3.2%'),alignSelf:'center', fontFamily:'Myriad-Pro-Bold-Condensed' }}>ARENA</Text>
                        </TouchableOpacity>
                    <TouchableOpacity
                    style={styles.buttonContainer}
                    onPress={() => navigation.navigate('Logistic')}
                    >
                        <HomeIcon 
                        loading={() => setLoaded(true)}
                        img={require('../../img/Logistics-icon.png')}
                        />
                        <Text numberOfLines={2} style={{fontSize:wp('3.2%'),alignSelf:'center',  fontFamily:'Myriad-Pro-Bold-Condensed', textAlign:'center'}}>LOGISTICS</Text>
                        </TouchableOpacity>
                    <TouchableOpacity
                    onPress={() => navigation.navigate('SmartBoard')}
                    style={styles.buttonContainer}
                    >
                       <HomeIcon 
                       loading={() => setLoaded(true)}
                        img={require('../../img/SmartBoard.png')}
                        />
                        <Text numberOfLines={2} style={{fontSize:wp('3.2%'),alignSelf:'center', fontFamily:'Myriad-Pro-Bold-Condensed', textAlign:'center' }}>SMART BOARD</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.viewContainer}>
                    <TouchableOpacity 
                    onPress={() => navigation.navigate('InsuranceGroup')}
                    style={styles.buttonContainer}
                    >
                        <HomeIcon 
                        loading={() => setLoaded(true)}
                        img={require('../../img/Appicon4.jpg')}
                        />
                        <Text numberOfLines={2} style={{fontSize:wp('3.2%'),alignSelf:'center', fontFamily:'Myriad-Pro-Bold-Condensed', textAlign:'center' }}>INSURANCE GROUP</Text>
                        </TouchableOpacity>
                    <TouchableOpacity
                    onPress={() => navigation.navigate('ITA')}
                    style={{...styles.buttonContainer,maxWidth:'100%',flex:1, marginTop:5}}
                    >
                        <HomeIcon 
                        loading={() => setLoaded(true)}
                        img={require('../../img/Appicon5.png')}
                        /><Text numberOfLines={2} style={{fontSize:wp('2.9%'),alignSelf:'center', fontFamily:'Myriad-Pro-Bold-Condensed', textAlign:'center', lineHeight:16, }}>INTERNATIONAL TRUCKING ASSOCIATION</Text>
                        </TouchableOpacity>
                    <TouchableOpacity
                    onPress={() => navigation.navigate('EquipmentFinance')}
                    style={styles.buttonContainer}
                    >
                        <HomeIcon 
                        loading={() => setLoaded(true)}
                        img={require('../../img/Appicon6.jpg')}
                        />
                        <Text numberOfLines={2} style={{fontSize:wp('3.2%'),alignSelf:'center', fontFamily:'Myriad-Pro-Bold-Condensed', textAlign:'center' }}>EQUIPMENT FINANCE</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.viewContainer}>
                    <TouchableOpacity 
                    style={styles.buttonContainer}
                    onPress={() => navigation.navigate('TruckHome')}
                    >
                        <HomeIcon 
                        loading={() => setLoaded(true)}
                        img={require('../../img/Appicon7.jpg')}
                        />
                        <Text numberOfLines={2} style={{fontSize:wp('3.2%'),alignSelf:'center', fontFamily:'Myriad-Pro-Bold-Condensed', textAlign:'center' }}>TRUCK SALES</Text>
                        </TouchableOpacity>
                    <TouchableOpacity
                    onPress={() => navigation.navigate('TrailerLease')}
                    style={styles.buttonContainer}
                    >
                        <HomeIcon 
                        loading={() => setLoaded(true)}
                        img={require('../../img/Appicon8.jpg')}
                        />
                        <Text numberOfLines={2} style={{fontSize:wp('3.2%'),alignSelf:'center', fontFamily:'Myriad-Pro-Bold-Condensed', textAlign:'center' }}>TRAILER LEASE</Text>
                        </TouchableOpacity>
                    <TouchableOpacity
                    onPress={() => navigation.navigate('TruckRentalNavigation')}
                    style={styles.buttonContainer}
                    >
                        <HomeIcon 
                        loading={() => setLoaded(true)}
                        img={require('../../img/Appicon9.jpg')}
                        />
                        <Text numberOfLines={2} style={{fontSize:wp('3.2%'),alignSelf:'center', fontFamily:'Myriad-Pro-Bold-Condensed', lineHeight:16, textAlign:'center' }}>TRUCK RENTAL & LEASING</Text>
                    </TouchableOpacity>
                </View>
            </View>
            </View>
            </View>
            }
        </View>
        
    );
};

HomeScreen.navigationOptions = {
    header: null
    
};

const styles= StyleSheet.create({
    container:{
        flex:1,
        flexDirection:'column',
    },
    viewContainer:{
        flexDirection: 'row',
        justifyContent: 'center',
        alignContent:'center',
        width:'100%',
        height:'33%',
        flex:1,
        
    },
    buttonContainer:{
        flex:1,
        width:'100%',
    },
    image: {
        resizeMode:'contain',
        maxWidth:'80%',
        maxHeight:'80%',
        flex:1,
        alignSelf:'center'
    },
    mainContainer:{
        flex:1,
        marginHorizontal:'10%',
        marginTop:0,
        height:'70%',
        marginBottom:'7%'
    },
    contactContainer:{
        width:'60%',
        height:'8%',
        backgroundColor:'#ffd700',
        marginVertical: 20,
        elevation: 7,
    }


});

export default HomeScreen;