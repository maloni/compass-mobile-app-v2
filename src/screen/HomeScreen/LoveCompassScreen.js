import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, Platform, Linking, Share } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { AntDesign } from '@expo/vector-icons';


const LoveCompassScreen = ({ navigation }) => {

    
    return(
        <View style={{width:wp('100%'), height:hp('100%')}}>
            <View style={{flexDirection:'row', alignItems:'center', alignSelf:'center', marginTop:hp('4%')}}>
                <AntDesign  name='hearto' size={wp('20%')} color='#ed1944'/>
                <View style={{marginLeft:'5%'}}>
                    <Text style={{fontSize:wp('8%'),color:'#484848', fontFamily:'Myriad-Pro-Bold-Condensed'}}>LOVE COMPASS?</Text>
                    <Text style={{fontSize:wp('7%'),color:'#484848', fontFamily:'Myriad-Pro-Condensed'}}>Share with friends.</Text>
                </View>
            </View>
            <View style={{flexDirection:'row', justifyContent:'space-evenly',marginTop:hp('15%')}}>
            <TouchableOpacity
            style={{width:wp('20%'), borderWidth:1}}
            onPress={() => shareThis('Share on', 'Compass is awsome', Platform =='iOS' ? 'https://apps.apple.com/us/app/compass-holding/id1181587901'  : 'https://play.google.com/store/apps/details?id=com.compassHolding' )}
            >
                <Image style={{width:'100%'}} resizeMode='contain' source={require('../../img/facebook.png')}/>
            </TouchableOpacity>
            <TouchableOpacity
            style={{width:wp('20%'), borderWidth:1}}
            >
                <Image style={{width:'100%'}} resizeMode='contain' source={require('../../img/linkedin.png')}/>
            </TouchableOpacity>
            <TouchableOpacity
            style={{width:wp('20%'), borderWidth:1}}
            >
                <Image style={{width:'100%'}} resizeMode='contain' source={require('../../img/whatsapp.png')}/>
            </TouchableOpacity>
            </View>
            <View style={{flexDirection:'row', justifyContent:'space-evenly',marginTop:hp('7%')}}>
            <TouchableOpacity
            style={{width:wp('20%'), borderWidth:1}}
            >
                <Image style={{width:'100%', alignSelf:'center'}} resizeMode='cover' resizeMethod='auto' source={require('../../img/emailShare.png')}/>
            </TouchableOpacity>
            <TouchableOpacity
            style={{width:wp('20%'), borderWidth:1}}
            >
                <Image style={{width:'100%'}} resizeMode='contain' source={require('../../img/twitter.png')}/>
            </TouchableOpacity>
            <TouchableOpacity
            style={{width:wp('20%'), borderWidth:1}}
            >
                <Image style={{width:'100%'}} resizeMode='contain' source={require('../../img/viber.png')}/>
            </TouchableOpacity>
            </View>

        </View>
    )
};

const styles = StyleSheet.create({
    container:{
        width:'90%',
        height:'80%',
        margin:'5%',
        marginTop:30
    },
    heart:{
        borderTopWidth:6,
        borderTopColor:'#eaeaea',
        flexDirection:'row',
        alignItems:'center',
        padding:0,
        height:'20%',
    },
    heading:{
        fontSize:30,
        color:'#484848',
        marginBottom:10,
        fontFamily:'Myriad-Pro-Bold-Condensed'
    },
    img:{
        width:'30%',
        marginVertical:0,
    }
});

export default LoveCompassScreen;