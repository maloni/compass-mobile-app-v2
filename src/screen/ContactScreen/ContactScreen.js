import React, { useState } from 'react';
import { Text, View, TextInput, StyleSheet, Image, Picker, KeyboardAvoidingView } from 'react-native';

const ContactScreen = () => {
    const [name, setName] = useState('')
    const [companyName, setCompanyName] = useState('')
    const [phone, setPhone] = useState('')
    const [email, setEmail] = useState('')
    const [notes, setNotes] = useState('')
    const [contact, setContact] = useState('')

    console.log(contact);

    return(
        <View style={{flex:1}}>
            <View style={styles.container}>
                <Image style={{width:'90%', paddingTop:0}} source={require('../../img/CHlogo.png')}  resizeMode='contain'/>
                <View style={{borderWidth:1, borderRadius:10, width:'90%', padding:8, margin:10, marginVertical:10}}>
                <TextInput
                    value={name}
                    onChangeText={(text) => setName(text)}
                    placeholder='Name'
                    style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:18 }}
                    placeholderTextColor='#484848'
                />
                </View>
                <View style={{borderWidth:1, borderRadius:10, width:'90%', padding:8, marginVertical:10}}>
                <TextInput
                    value={companyName}
                    onChangeText={(text) => setCompanyName(text)}
                    placeholder='Company Name'
                    style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:18 }}
                    placeholderTextColor='#484848'
                />
                </View>
                <View style={{borderWidth:1, borderRadius:10, width:'90%', padding:8, marginVertical:10}}>
                <TextInput
                    value={phone}
                    onChangeText={(text) => setPhone(text)}
                    placeholder='Phone'
                    style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:18 }}
                    placeholderTextColor='#484848'
                />
                </View>
                <View style={{borderWidth:1, borderRadius:10, width:'90%', padding:8, marginVertical:10}}>
                <TextInput
                    value={email}
                    onChangeText={(text) => setEmail(text)}
                    placeholder='Email'
                    style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:18}}
                    placeholderTextColor='#484848'
                />
                
                </View>
                <View style={{width:'90%' , height:50, borderWidth:1, borderRadius:10, marginVertical:10}}>
                    <Picker style={{width:'100%' , height:50}}
                    selectedValue={contact}
                    onValueChange={(value) => setContact(value)}
                    >
                        <Picker.Item label='Which Compass Company you want to contact?' />
                        <Picker.Item label='Compass Truck Sales' value='Truck Sales' />
                        <Picker.Item label='Compass Truck Rental And Leasing' value='Truck Rental And Leasing'/>
                        <Picker.Item label='Compass Lease' value='Lease'/>
                        <Picker.Item label='Compass Payment Services' value='Payment Services'/>
                        <Picker.Item label='Compass Insurance Group' value='Insurance Group'/>
                        <Picker.Item label='Compass Funding Solutions' value='Funding Solutions'/>
                        <Picker.Item label='Compass Equipment Finance' value='Equipment Finance'/>
                        <Picker.Item label='Compass Arena' value='Arena'/>
                        <Picker.Item label='ITA Carrier Services' value='ITA'/>
                    </Picker>
                </View>
                <View style={{borderWidth:1, borderRadius:10, width:'90%', padding:8, marginVertical:10, marginTop:40}}>
                <TextInput
                    value={notes}
                    onChangeText={(text) => setNotes(text)}
                    placeholder='Notes'
                    style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:18 }}
                    placeholderTextColor='#484848'
                    multiline={true}
                    numberOfLines={5}
                    textAlignVertical='top'
                />
                </View>
                </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        width:'90%',
        alignSelf:'center',
        alignItems:'center'
    }
});
export default ContactScreen;
