import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity} from 'react-native';
import cpsApi from '../../api/cpsApi';
import  CPSlogo  from '../../components/CPSlogo';
import LoveCompass from '../../components/LoveCompass'
import CPSMenu from '../../components/CPSMenu';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {MaterialCommunityIcons} from '@expo/vector-icons';
import {Entypo} from '@expo/vector-icons';
import Modal from "react-native-modal";
import { BarIndicator } from 'react-native-indicators';

const StoredCards = ({ navigation }) => {

    const[showModal, setShowModal] = useState(false)
    const[showModal2, setShowModal2] = useState(false)
    const[payment, setPayment] = useState(false)

    const paying = () => {
        setTimeout(function(){
            setPayment(true)
        }, 5000)
    }

    return(
           <View style={styles.container}> 
                <CPSlogo />
                <View style={{width:wp('90%'), height:hp('25%'), borderWidth:2, borderRadius:30, marginTop:hp('3%'), borderColor:'#49a43a'}}>
                    <View style={{width:'70%', height:'23%', marginTop:'5%', alignSelf:'flex-end'}}>
                        <Image resizeMode='contain' style={{height:'80%', width:'80%', alignSelf:'center'}} source={require('../../img/CPS.png')}/>
                    </View>
                    <View style={{ width:'20%', paddingLeft:'5%'}}>
                        <MaterialCommunityIcons 
                        name='sim'
                        size={40}
                        color='#484848'
                        style={{transform:[{rotate:'270deg'}], alignSelf:'center'}}
                        />
                    </View>
                    <View style={{marginTop:hp('1%')}}>
                        <Text style={styles.txt}>1234 - 5678 - 9123 - 4567</Text>
                        <Text style={{...styles.txt, fontSize:wp('4%'), marginTop:hp('0.5%'), alignSelf:'flex-start', paddingLeft:'10%'}}>ROY DOBRASINOVIC</Text>
                        <Text style={{...styles.txt, fontSize:wp('4%'), marginTop:hp('0.5%'), alignSelf:'flex-start', paddingLeft:'7%'}}> COMPASS HOLDING LLC</Text>
                    </View>
                </View>
                <View style={{width:wp('90%'), height:hp('40%'), marginTop:hp('2%')}}>
                    <View style={{width:'100%', borderBottomWidth:4, borderBottomColor:'#d0e7cc', paddingBottom:hp('1%')}}>
                        <Text style={styles.waletTxt}>Pay with Compass Wallet</Text>
                    </View>
                    <TouchableOpacity 
                    onPress={() => setShowModal(true)}
                    style={{width:hp('20%'), height:hp('20%'), alignSelf:'center', marginTop:hp('4%')}}
                    >
                    <View style={{borderRadius:hp('20%'), backgroundColor:'#49a43a', width:hp('20%'), height:hp('20%'), alignSelf:'center', justifyContent:'center'}}>
                        <Entypo 
                        name='wallet'
                        color='white'
                        size={hp('12%')}
                        style={{alignSelf:'center'}}
                        />
                    </View>
                    </TouchableOpacity>
                    <Text style={{...styles.waletTxt, marginTop:hp('2%')}}>Press to pay with Compass Wallet</Text>
                </View>
                {
                    showModal
                    ?<Modal
                        animationType='slideInUp'
                        isVisible={showModal}
                        onRequestClose={() => setShowModal(false)}
                        onBackdropPress={() => setShowModal(false)}
                        backdropOpacity={0.9}
                        hasBackdrop={true}
                        backdropColor='white'
                        >
                            <TouchableOpacity
                            onPress={() => {setShowModal2(true), paying(), setShowModal(false)}}
                            >
                                <View style={{width:wp('60%'), backgroundColor:'#49a43a', alignSelf:'center', borderRadius:20, borderWidth:4, borderColor:'white'}}>
                                    <Text style={{...styles.waletTxt, color:'white', padding:wp('4%'), fontSize:wp('8%'), textAlign:'center'}}>PAY WITH COMPASS CARD</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                            onPress={() => setShowModal(false)}
                            >
                                <View style={{width:wp('60%'), backgroundColor:'#49a43a', alignSelf:'center', marginTop:hp('10%'), borderRadius:20, borderRadius:20, borderWidth:4, borderColor:'white'}}>
                                    <Text style={{...styles.waletTxt, color:'white', padding:wp('4%'), fontSize:wp('9%')}}>CANCEL</Text>
                                </View>
                            </TouchableOpacity>
                    </Modal>
                    :null
                }
                {
                    showModal2
                    ?<Modal
                        animationType='slideInUp'
                        isVisible={showModal2}
                        onRequestClose={() => {setShowModal2(false), setPayment(false)}}
                        onBackdropPress={() => {setShowModal2(false), setPayment(false)}}
                        backdropOpacity={1}
                        hasBackdrop={true}
                        backdropColor='white'
                        >          
                        {!payment         
                            ?<View style={{width:wp('90%'), height:hp('20%'), alignSelf:'center', alignItems:'center', justifyContent:'center'}}>
                               <Text style={{...styles.waletTxt, color:'#49a43a', padding:wp('4%'), fontSize:wp('8%'), justifyContent:'center'}} > Payment in progress...</Text>
                                <BarIndicator
                                count={5}
                                color='#49a43a'
                                size={wp('15%')}
                                />
                            </View>
                            :<Text style={{...styles.waletTxt, color:'#49a43a', padding:wp('4%'), fontSize:wp('8%'), justifyContent:'center'}} > Payment successful</Text>

                        } 
                    </Modal>
                    :null
                }
            </View>
            
    )
    
};




const styles = StyleSheet.create({
    container: {
        height:'100%',
        width:'90%',
        marginHorizontal:'5%',
        alignItems: 'center',
        marginTop:hp('2%')
    },
    waletTxt:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:wp('6%'),
        alignSelf:'center',
    },

        btn:{
        width:'70%',
        alignSelf:'center',
        marginTop:'15%',
        height:'10%', 
        borderWidth:3, 
        borderColor:'#49a43a', 
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
          width: 10,
          height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#484848',
        elevation: 12,
        
    },
    txt: {
        fontFamily:'credit',
        color:'#484848',
        fontSize:wp('4%'),
        alignSelf:'center'
    },
    cpsMenu:{
        marginTop:hp('15%'),
        justifyContent:'flex-end',
        borderBottomWidth:3,
        borderBottomColor:'#d0e7cc',
        marginHorizontal:10
    }

      
});

export default StoredCards;