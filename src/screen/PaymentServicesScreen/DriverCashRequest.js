import React, { useState, useContext, useEffect } from 'react';
import { Text, View, StyleSheet, Image,TextInput } from 'react-native';
import CPSChecksInput from '../../components/CPSChecksInput';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Colors from '../../../assets/Colors';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import CPSlogo from '../../components/CPSlogo';
import Axios from 'axios';

const DriverCashRequest = () => {
    const[amount, setAmount] = useState('')
    const[driver, setDriver] = useState('')
    const[description, setDescription] = useState('')
    const[cardNumber, setCardNumber] = useState('')
    const[unitNumber, setUnitNumber] = useState('')

        // const query = "arwen=rht45P56hytA54trR45tTyb91Iy54yZ097hyA65yyN69hyukl&checkNumber="+cardNumber+"&amount="+(amount * 100)+"&driverId="+driver+"&description="+description+"&userId="+"&unitId="+unitNumber

    const reqCash = () => {
        let filter = {cardNumber:cardNumber};
        let options ={fields:"infoCapture"};
        let query = "filter=" + encodeURIComponent(JSON.stringify(filter))+"&options="+encodeURIComponent(JSON.stringify(options));
        Axios.post('https://apps.compassaws.net/api/card/getAllCards', query)
        .then(res => {
            console.log(res.data)
        })
        .catch(err =>{
            console.log(err)
        })
    }

    return(
            <View style={styles.container}>
                        <CPSlogo />
                <CPSChecksInput 
                naslov='Please enter your Unit Number :'
                vrednost={unitNumber}
                postaviVrednost={(text) => setUnitNumber(text)}            
                />
                <CPSChecksInput 
                naslov='CPS Card Number :'
                vrednost={cardNumber}
                postaviVrednost={(text) => setCardNumber(text)}            
                />
                <CPSChecksInput 
                naslov='Requested Cash Amount :'
                vrednost={amount}
                postaviVrednost={(text) => setAmount(text)}            
                />
                <CPSChecksInput 
                naslov='Reason for Cash :'
                vrednost={driver}
                postaviVrednost={(text) => setDescription(text)}             
                />
                <View style={styles.btn}>
                    <TouchableOpacity
                    onPress={() => reqCash()}
                    >
                        <Text style={styles.btnTxt}>REQUEST CASH</Text>
                    </TouchableOpacity>
                </View>
            </View>
    )
};

const styles = StyleSheet.create({
    container:{
        width:'90%',
        marginHorizontal:'5%',
        alignItems: 'center',
        marginTop:hp('2%'),
        height:'100%'
    },
    txt:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:26,
        color:'#49a43a',
        paddingBottom:10
    },
    inputStyle:{
        width:'90%', 
        height:50, 
        borderColor:'#EEEEEE', 
        borderWidth:2,
        marginVertical:20,
        backgroundColor:'white',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.57,
        shadowRadius: 16,
        elevation: 10,
            },
    btn:{
        backgroundColor:Colors.zelena,
        width:'60%',
        borderRadius:10,
        padding:7,
        position:'absolute',
        bottom:0,
        marginBottom:hp('5%')
        },
    btnTxt:{
        color:'white',
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:wp('5%'),
        alignSelf:'center'
        },
    container1:{
            flexDirection:'row',
            alignItems:'center',
            width:'90%', 
            height:50, 
            borderColor:'white', 
            borderWidth:3,
            marginVertical:10,
            borderRadius:5,
            backgroundColor:'white',
            shadowOffset: {
                width: 0,
                height: 6,
            },
            shadowOpacity: 0.57,
            shadowRadius: 16,
            shadowColor:'green',
    
            elevation: 12,
            borderColor:'#d8d8d8',
            borderWidth:0.2,    
            },
    txt1:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:20,
        color:'#8a8a8a',
        paddingLeft:10
        }
});

export default DriverCashRequest;