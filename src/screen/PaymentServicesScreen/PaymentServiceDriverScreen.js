import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Fumi } from 'react-native-textinput-effects';
import { FontAwesome, Ionicons } from '@expo/vector-icons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { PulseIndicator } from 'react-native-indicators';
import CPSlogo from '../../components/CPSlogo';

const PaymentServiceDriverScreen = ({ navigation }) => {

    const [loading, setLoading] = useState(false);
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    console.log(navigation.getParam('userResponse'))
    //console.log(navigation.getParam('odgovor'))
/*
    const userData = navigation.getParam('response.data.results.result');

    console.log(userData)
*/
    return(
        <View style={{flex:1}}>
            {/* {!loading 
            ? <PulseIndicator size={40} color="green"  style={{justifyContent:'center'}}/> */}
             <View style={styles.container}>
                <CPSlogo />
                  <View style={styles.inputContainer}>
                    <Fumi 
                        iconClass={FontAwesome}
                        iconName={'user'}
                        style={styles.inputStyle}
                        iconColor={'green'}
                        iconSize={20}
                        iconWidth={40}
                        inputPadding={22}
                        value={username}
                        onChangeText={(text) => setUsername(text)}
                        label={'Enter username'}
                        autoCorrect={false}
                        autoCapitalize={'none'}
                    />
                    <Fumi 
                        iconClass={Ionicons}
                        iconName={'md-key'}
                        style={styles.inputStyle}
                        iconColor={'green'}
                        iconSize={20}
                        iconWidth={40}
                        inputPadding={22}
                        value={password}
                        onChangeText={(text) => setPassword(text)}
                        label={'Enter password'}
                        autoCorrect={false}
                        autoCapitalize={'none'}
                        secureTextEntry={true}
                    />
                </View>
                    <TouchableOpacity
                        style={styles.btn}
                        onPress={() => navigation.navigate('DriverHome')
                            // send(user.username,user.password,'prihvati')
                            // Axios.post('https://apps.compassaws.net/api/card/login-cps', smile)
                            // .then(function(response) {
                                
                            //     response.data.success == '0' 
                            //     ? _saveUser()  & navigation.navigate('PaymentDriver', {userData: response.data.results})
                            //     : alert(JSON.stringify(response.data.error))
                            //     //console.log(response.data.success) 
                                

                            //     // const odgovor = response.data;

                            //     // if (!odgovor.success){ 
                            //     //     navigation.navigate('PaymentDriver',{ime: odgovor.results.result})
                            //     // }
                            // })
                            // .catch(function(error) {
                            //    console.log(error)
                            // })
                            }
                              
                    >
                        <Text style={styles.btnText}>LOG IN</Text>
                    </TouchableOpacity>
            </View>
        </View>
    )
    
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        marginTop:hp('2%'),
      },
      inputContainer:{
          width:'100%',
          alignItems:'center',
          justifyContent:'center',
          marginTop:50
          
      },
      inputStyle:{
        width:'60%', 
        height:50, 
        borderColor:'green', 
        borderWidth:2,
        marginVertical:20,
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.57,
        shadowRadius: 16,
        shadowColor:'green',
        justifyContent:'center',
        elevation: 24,
            },
            btn:{
                marginVertical:hp('2%'),
                width:'60%',
                alignSelf:'center',
                borderRadius:10,
                backgroundColor:'green',
                shadowOffset: {
                width: 10,
                height: 6,
                },
                shadowOpacity: 0.30,
                shadowRadius: 12,
                shadowColor:'#484848',
                elevation: 8,
                
                },
        btnText: {
          fontSize:32,
          color:'white',
          fontFamily:'Myriad-Pro-Bold-Condensed',
          alignSelf:'center',
          paddingVertical:10
        }
});
export default PaymentServiceDriverScreen;