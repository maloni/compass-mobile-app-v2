import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, TextInput, SafeAreaView, ScrollView,Platform, KeyboardAvoidingView } from 'react-native';
import Axios from 'axios';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { AsyncStorage } from 'react-native'; 
import { PulseIndicator } from 'react-native-indicators';
import CPSlogo from '../../components/CPSlogo';
import * as Font from 'expo-font';
import { Fumi } from 'react-native-textinput-effects';
import { FontAwesome, Ionicons } from '@expo/vector-icons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import LoveCompass from '../../components/LoveCompass';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';


const PaymentServicesScreen = ({ navigation }) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);
    const [fontLoaded, setFontLoaded] = useState(false)

    const user = {
        username: username,
        password: password
    }

    useEffect(() =>{
        // _retriveData();
    });

    // const _loadFont = async() => {
    //     await Font.loadAsync({
    //         'Myriad-Pro-Bold-Condensed': require('../../../assets/fonts/Myriad-Pro-Bold-Condensed.ttf')
    //     });            
    //     setFontLoaded(true);
    //     } 

    // const _saveUser = async () => {
    //     try {
    //         await AsyncStorage.setItem('Users', JSON.stringify(user));
    //     } catch (error){
    //         console.log(error);
    //     }
    // }

    // const _retriveData = async () => {
    //     try{
    //         const userStorage = await AsyncStorage.getItem('Users');
    //         if (userStorage !== null) {
    //             const parsovano = JSON.parse(userStorage);
    //             send(parsovano.username, parsovano.password, 'snimi');
    //         }   else if (userStorage == null){
    //           setLoading(true);
    //         }
            
    //     } catch (error) {
    
    //     };
    // };

    const send = function(user,sifra,mile){
        const slanje = 'email='+user+'&password='+sifra+'&notificationToken='+'1'
            Axios.post('https://apps.compassaws.net/api/card/login-cps', slanje)
                .then(function(response) {
                    const odgovor = response.data
                           if(odgovor.success == 0 && mile=='prihvati'){
                            //    _saveUser();
                                navigation.navigate('CompanyHome', { userResponse: odgovor})                  
                           } else if (odgovor.success == 0 && mile=='snimi'){ 
                                navigation.navigate('CompanyHome', { userResponse: odgovor})                  
                           }
                           else alert(odgovor.error)
                    })
                .catch(function(error) {
                         console.log(error)
                    })
    };
    
    return(
        <View style={{flex:1}}>
            {!loading && fontLoaded
            ? <PulseIndicator size={40} color="green"  style={{justifyContent:'center'}}/>
            : <View      
                    style={styles.container}           
                >
        
                <CPSlogo />
                  <View style={styles.inputContainer}>
                    <Fumi 
                        iconClass={FontAwesome}
                        iconName={'user'}
                        style={styles.inputStyle}
                        iconColor={'green'}
                        iconSize={20}
                        iconWidth={40}
                        inputPadding={22}
                        value={username}
                        onChangeText={(text) => setUsername(text)}
                        label={'Enter username'}
                        autoCorrect={false}
                        autoCapitalize={'none'}
                    />
                    <Fumi 
                        iconClass={Ionicons}
                        iconName={'md-key'}
                        style={styles.inputStyle}
                        iconColor={'green'}
                        iconSize={20}
                        iconWidth={40}
                        inputPadding={22}
                        value={password}
                        onChangeText={(text) => setPassword(text)}
                        label={'Enter password'}
                        autoCorrect={false}
                        autoCapitalize={'none'}
                        secureTextEntry={true}
                    />
                </View>
                    <TouchableOpacity
                        style={styles.btn}
                        onPress={() => send(user.username,user.password,'prihvati')}
                    >
                        <Text style={styles.btnText}>LOG IN</Text>
                    </TouchableOpacity>                    
                    <View style={{alignSelf:'center', width:'80%', position:'absolute', bottom:hp('4%') }}>
                    <LoveCompass />
                    </View>
                    </View>

                    }
                    
        </View>
    )
    
};



PaymentServicesScreen.navigationOptions = ({ navigation }) => {
    return{
        header: null,
    }
};

const styles = StyleSheet.create({
    container: {
        width:wp('90%'),
        marginHorizontal:'5%',
        height:hp('90%'),
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent',
    },
      inputContainer:{
          width:'100%',
          alignItems:'center',
          justifyContent:'center',
          
      },
      inputStyle:{
        width:'80%', 
        height:50, 
        borderColor:'green', 
        borderWidth:2,
        marginVertical:20,
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.57,
        shadowRadius: 16,
        shadowColor:'green',
        justifyContent:'center',
        elevation: 24,
            },
            btn:{
                marginVertical:hp('2%'),
                width:'60%',
                alignSelf:'center',
                borderRadius:10,
                backgroundColor:'green',
                shadowOffset: {
                width: 10,
                height: 6,
                },
                shadowOpacity: 0.30,
                shadowRadius: 12,
                shadowColor:'#484848',
                elevation: 8,
                
                },
        btnText: {
          fontSize:32,
          color:'white',
          fontFamily:'Myriad-Pro-Bold-Condensed',
          alignSelf:'center',
          paddingVertical:10
        }
});

export default PaymentServicesScreen;