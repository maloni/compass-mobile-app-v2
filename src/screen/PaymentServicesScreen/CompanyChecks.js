import React, { useState, useContext, useEffect } from 'react';
import { Text, View, StyleSheet, Image,TextInput, Picker } from 'react-native';
import CPSChecksInput from '../../components/CPSChecksInput';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Colors from '../../../assets/Colors';
import Axios from 'axios';

const CompanyChecks = ({ navigation }) => {
    const[amount, setAmount] = useState('')
    const[driver, setDriver] = useState('')
    const[description, setDescription] = useState('')
    const[cardNumber, setCardNumber] = useState('')
    const[unitNumber, setUnitNumber] = useState('')
    const[result, setResult] = useState([])

    const cpsUserOK = navigation.getParam('cpsUserOK');


    useEffect(() => {
            const cpsUserOK = navigation.getParam('cpsUserOK');
            Axios.get('https://apps.compassaws.net/api/card/companyDrivers/'+cpsUserOK)
            .then((res) => {
                setResult(res.data.result)
            })
            .then((err) => {
                // console.log(err)
            })
        }, []);

        const checkCreate = () => {
            const query = "arwen=rht45P56hytA54trR45tTyb91Iy54yZ097hyA65yyN69hyukl&checkNumber="+cardNumber+"&amount="+(amount * 100)+"&driverId="+driver+"&description="+description+"&userId="+"&unitId="+unitNumber
            Axios.post('https://apps.compassaws.net/api/card/createCheck', query)
            .then((res) =>{
                console.log(res)
            })
            .catch((err) =>{
                console.log(err)
            })
        }

        // for(let i=0; i<result.length; i++){
        //     setDriversName([...driversName, result[i].name])
        // }


    // console.log(result);

    return(
        <View>
            <View style={styles.container}>
                <View style={{width:'100%', height:'18%', marginTop:5}}>
                    <Image style={{width:'70%', height:'100%' , alignSelf:'center', paddingTop:0}} resizeMode='contain' source={require('../../img/CPSlogo.png')} />
                </View>
            <View style={{borderBottomColor:'#e6e6ea', borderBottomWidth:3, width:'90%', marginBottom:15}}>
                <Text style={styles.txt}>CHECKS</Text>
            </View>
            <CPSChecksInput 
            naslov='Card Number :'
            vrednost={cardNumber}
            postaviVrednost={(text) => setCardNumber(text)}            
            />
            <CPSChecksInput 
            naslov='Amount :'
            vrednost={amount}
            postaviVrednost={(text) => setAmount(text)}            
            />
            <CPSChecksInput 
            naslov='Unit Number :'
            vrednost={unitNumber}
            postaviVrednost={(text) => setUnitNumber(text)}            
            />
            <View style={styles.pickerView}>
                <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed',fontSize:20,color:'#8a8a8a',paddingLeft:10, width:'20%'}}>Driver :</Text>
            <Picker
                style={{width:'80%', alignSelf:'center', borderWidth:1, borderColor:'#da212f'}}
                selectedValue={driver}
                onValueChange={(item) => setDriver(item)}
            >
                <Picker.Item label='' />
                {
                    result.map((item, index) =>{
                        return <Picker.Item label={item.name} value={item.name} key={index} />
                    })
                }
            </Picker>
            </View>
            <CPSChecksInput 
            naslov='Description :'
            vrednost={description}
            postaviVrednost={(text) => setDescription(text)}            
            />
            <View style={styles.btn}>
                <TouchableOpacity
                    onPress={() => checkCreate()}
                >
                    <Text style={styles.btnTxt}>CREATE CHECK</Text>
                </TouchableOpacity>
            </View>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container:{
        width:'90%',
        marginHorizontal:'5%',
        alignItems: 'center',
        height:'100%'
    },
    txt:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:26,
        color:'#49a43a',
        paddingBottom:10
    },
    inputStyle:{
        width:'90%', 
        height:50, 
        borderColor:'#EEEEEE', 
        borderWidth:2,
        marginVertical:20,
        backgroundColor:'white',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.57,
        shadowRadius: 16,
        elevation: 10,
            },
        btn:{
            backgroundColor:Colors.zelena,
            width:'50%',
            borderRadius:10,
            marginTop:40,
            padding:7
        },
        btnTxt:{
            color:'white',
            fontFamily:'Myriad-Pro-Bold-Condensed',
            fontSize:30,
            alignSelf:'center'
        },
        container1:{
            flexDirection:'row',
            alignItems:'center',
            width:'90%', 
            height:50, 
            borderColor:'white', 
            borderWidth:3,
            marginVertical:10,
            borderRadius:5,
            backgroundColor:'white',
            shadowOffset: {
                width: 0,
                height: 6,
            },
            shadowOpacity: 0.57,
            shadowRadius: 16,
            shadowColor:'green',
    
            elevation: 12,
            borderColor:'#d8d8d8',
            borderWidth:0.2,    
            },
            txt1:{
                fontFamily:'Myriad-Pro-Bold-Condensed',
                fontSize:20,
                color:'#8a8a8a',
                paddingLeft:10
            },
        pickerView:{
            justifyContent:'space-between',
            flexDirection:'row',
            alignItems:'center',
            width:'90%', 
            height:50, 
            borderColor:'white', 
            borderWidth:3,
            marginVertical:10,
            borderRadius:5,
            backgroundColor:'white',
            shadowOffset: {
                width: 0,
                height: 6,
            },
            shadowOpacity: 0.57,
            shadowRadius: 16,
            shadowColor:'green',

            elevation: 12,
            borderColor:'#d8d8d8',
            borderWidth:0.2,  
        }
});

export default CompanyChecks;