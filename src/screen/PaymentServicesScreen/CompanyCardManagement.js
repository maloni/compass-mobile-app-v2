import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import SearchBar from '../../components/Search';
import CpsActiveHold from '../../components/CpsActiveHold';
import Axios from 'axios';


const CompanyCardManagement = ({ navigation }) =>{
    const[search, setSearch] = useState('');
    const[allCards, setAllCards] = useState([])
    const[cardLoader, setCardLoader] = useState(false)

    useEffect(() => {
        getCards()
    },[])

    const getCards = () =>{
        let filter ={carrierId:10001}
        let options ={fields:"infoCapture carrierId cardNumber active onHold cashBucket driverId lastTransactionDate cardHolder cardGroupId"};
        let query = "filter=" + encodeURIComponent(JSON.stringify(filter))+"&options="+encodeURIComponent(JSON.stringify(options));

        Axios.post('https://apps.compassaws.net/api/card/getAllCards',query)
        .then(res => {
            setAllCards(res.data.result)
            // console.log(res)
            setCardLoader(true)
        })
        .catch(err => {
            console.log(err)
        })
    }

    // console.log(search)

    return(
        <View style={styles.container}>
            <View style={{width:'100%', height:'18%', marginTop:5}}>
                <Image style={{width:'70%', height:'100%' , alignSelf:'center', paddingTop:0}} resizeMode='contain' source={require('../../img/CPSlogo.png')} />
            </View>
            <View style={{width:'100%', alignItems:'center'}}>
                <SearchBar 
                vrednost = {search}
                postaviVrednost = {(text) => setSearch(text)}

                />
                <CpsActiveHold 
                search={search}
                allCards={allCards}
                getCards={getCards}
                />

                
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container:{
        width:'90%',
        marginHorizontal:'5%',
        alignItems: 'center',
        height:'100%'
    },
    txtViewStyle:{
        flexDirection:'row',
        
    },
    txtStyle:{
        fontSize:18
    }
});

export default CompanyCardManagement;