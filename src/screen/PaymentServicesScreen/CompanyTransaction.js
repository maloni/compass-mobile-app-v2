import React, { useState } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import CpsTransaction from '../../components/CpsTransaction';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import Axios from 'axios';
import CPSlogo from '../../components/CPSlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';



const CompanyTransaction = ({ navigation }) => {

    const cpsUserOK = navigation.getParam('cpsUserOK');
    const [transaction, setTransaction] = useState([])
    const [transactionVisible, setTransactionVisible] = useState(false)
    const [indicator, setIndicator] = useState(false)

    const [showDatePickerFrom, setShowDatePickerFrom] = useState(false);
    const [showDatePickerTo, setShowDatePickerTo] = useState(false);

    const[dateFrom, setDateFrom] = useState(new Date)
    const[dateTo, setDateTo] = useState(new Date)
    
    const dateFromText = moment(dateFrom).format('MMM DD, YYYY')
    const dateToText = moment(dateTo).format('MMM DD, YYYY')

    const dateFromSend = moment(dateFrom).format('YYYY-MM-DD')
    const dateToSend = moment(dateTo).format('YYYY-MM-DD')

    console.log(transaction);
    //console.log(dateTo);

    const show = () => {
        const info="carrierId="+cpsUserOK+"&startDate="+dateFromSend+"&endDate="+dateToSend;
        Axios.post('https://apps.compassaws.net/api/card/getNTransactions', info)
        .then((res) => {
            setTransaction(res.data.result)
            // console.log(res.data.result)
            setTransactionVisible(true)
            setIndicator(false)
        })
        .catch((err) =>{
            console.log(err)
        })
    }

    return(
        <View style={styles.container}>
            <CPSlogo />
            <View style={{flexDirection:'row', justifyContent:'space-between', width:'80%', alignSelf:'center'}}>
                <TouchableOpacity
                style={styles.date}
                onPress={() => setShowDatePickerFrom(true)}
                >
                    <Text>Select Start Date</Text>
                </TouchableOpacity>
                {showDatePickerFrom
                    ?<DateTimePicker
                    mode="date"
                    display="spinner"
                    defaultDate={new Date()}
                    value={new Date()}
                    onChange={(event, value) => {
                    setShowDatePickerFrom(false);
                    setDateFrom(value);
                    }}
                        />
                    :null
                }
                <TouchableOpacity
                style={styles.date}
                onPress={() => {setIndicator(true); setShowDatePickerTo(true); setIndicator(true); }}
                >
                    <Text>Select Start Date</Text>
                </TouchableOpacity>
                {showDatePickerTo
                    ?<DateTimePicker
                    mode="date"
                    display="spinner"
                    defaultDate={new Date()}
                    value={new Date()}
                    onChange={(event, value) => {
                    setShowDatePickerTo(false);
                    setDateTo(value);
                    show();
                    }}
                        />
                    :null
                }
            </View>
            <View style={{flexDirection:'row', alignSelf:'center', width:'80%', justifyContent:'space-between',padding:10 }}>
                <Text>{dateFromText}</Text>
                <Text>{dateToText}</Text>
            </View>
        <CpsTransaction 
        dateFrom={dateFromSend}
        dateTo={dateToSend}
        cpsUserOK={cpsUserOK}
        transaction={transaction}
        transactionVisible={transactionVisible}
        indicator={indicator}
        />
        </View>
    )
};

const styles = StyleSheet.create({
    container:{
        marginHorizontal:'5%',
        alignItems: 'center',
        height:hp('100%')
    },
    date:{
        borderWidth:1,
        padding:10
    }
});

export default CompanyTransaction;