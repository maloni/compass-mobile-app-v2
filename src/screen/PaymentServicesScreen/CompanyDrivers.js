import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, Image, FlatList } from 'react-native';
import CPSDrivers from '../../components/CPSDrivers';
import Axios from 'axios';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import CPSlogo from '../../components/CPSlogo'

const CompanyDrivers = ({ navigation }) => {
    
    const cpsUserOK = navigation.getParam('cpsUserOK');


    return(
        <View style={styles.container}>
                <CPSlogo />
                <CPSDrivers 
                cpsUserOK={cpsUserOK}
                />
        </View>
    )
};

const styles = StyleSheet.create({
    container:{
        marginHorizontal:wp('5%'),
        alignItems: 'center',
        height:hp('100%')
    }
});

export default CompanyDrivers;