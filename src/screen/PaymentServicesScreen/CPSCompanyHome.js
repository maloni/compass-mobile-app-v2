import React, { useEffect, useState, useContext } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import { ProgressBar } from 'react-native-paper';
import BalanceBar from '../../components/BalanceBar'
import CPSMenu from '../../components/CPSMenu';
import { PulseIndicator } from 'react-native-indicators';
import * as Font from 'expo-font';
import Axios from 'axios';
import { WaveIndicator } from 'react-native-indicators';



const CPSCompanyHome = ({ navigation }) => {
    const [limit, setLimit] = useState()
    const [creditBalance, setCreditBalance] = useState()
    const [creditLimit , setCreditLimit] = useState()
    const [dailyBalance, setDailyBalance] = useState()
    const [dailyLimit, setDailyLimit] = useState()
    const [active , setActive] = useState(false)
    
    const odgovor = navigation.getParam('userResponse');
    const cpsUserOK = odgovor.results.result.refId;

    //const cpsUserID = odgovor.results.result._id;

    useEffect(() =>{
    
        const fetchData = async () => {
            const odgovor = navigation.getParam('userResponse');
            const cpsUserOK = odgovor.results.result.refId;
            let info ="carrierId="+cpsUserOK;
                const result = await Axios.post('https://apps.compassaws.net/api/card/getBalanceLimit', info )
                .then(res => {
                    setLimit(res)
                    setCreditBalance(res.data.result[0].creditBalance/100)
                    setCreditLimit(res.data.result[0].creditLimit/100)
                    setDailyBalance(res.data.result[0].dailyBalance/100)
                    setDailyLimit(res.data.result[0].dailyLimit/100)
                    //console.log(res)
                    setActive(true)
                    console.log(odgovor.results );
                })
                .then(err => {
                    //console.log(err)
                })
        }
    fetchData();
    }, [])

//    console.log(cpsUserOK);
//    console.log(dailyBalance);
//    console.log(dailyLimit);



    return(
        <View style={{width:'100%', height:'100%'}}>
            { !active
            ?<WaveIndicator size={40} color="#49a43a"  style={{justifyContent:'center'}}/>
            :<View style={styles.container}>
                <View style={{width:'100%', height:'20%'}}>
                    <Image style={{width:'100%', height:'100%' }} resizeMode='contain' source={require('../../img/CPSlogo.png')} />
                </View>
                <View style={{width:'85%'}}>
                    <BalanceBar 
                    balance='Credit Balance'
                    minBalance={0}
                    maxBalance={creditLimit}
                    currentBalance={creditBalance}
                    />
                </View>    
                <View style={{width:'85%', marginTop:15}}> 
                    <BalanceBar 
                    balance='Daily Balance'
                    minBalance={0}
                    maxBalance={dailyLimit}
                    currentBalance={dailyBalance}
                    />
                </View>    
                    <View style={styles.cpsMenu}>
                        <CPSMenu title='CARD MANAGEMENT' navigacija={() => navigation.navigate('CardManagement')}/>
                        <CPSMenu title='TRANSACTIONS' navigacija={() => navigation.navigate('Transaction', {cpsUserOK:cpsUserOK})}/>
                        <CPSMenu title='CASH ADVANCES' navigacija={() => navigation.navigate('CashAdvances', {cpsUserOK:cpsUserOK})}/>
                        <CPSMenu title='CHECKS' navigacija={() => navigation.navigate('Checks', {cpsUserOK:cpsUserOK})}/>
                        <CPSMenu title='STATEMENTS' navigacija={() => navigation.navigate('Statements', {cpsUserOK:cpsUserOK})}/>
                        <CPSMenu title='DRIVERS' navigacija={() => navigation.navigate('Drivers', {cpsUserOK:cpsUserOK})}/>
                    </View>
            </View>
            
            }   
        </View>
    );
};

const styles = StyleSheet.create({
    container:{
        width:'90%',
        marginHorizontal:'5%',
        alignItems: 'center',
        justifyContent: 'center',
        height:'100%'
    },
    cpsMenu:{
        marginTop:20,
        justifyContent:'flex-end',
        borderBottomWidth:3,
        borderBottomColor:'#d0e7cc',
        marginHorizontal:10
    }
    
});

export default CPSCompanyHome;