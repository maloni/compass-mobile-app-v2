import React, { useState, useContext, useEffect } from 'react';
import { Text, View, StyleSheet, Image,TextInput, TouchableOpacity, FlatList  } from 'react-native';
import CPSChecksInput from '../../components/CPSChecksInput';
import CPSlogo from '../../components/CPSlogo';
import Colors from '../../../assets/Colors';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import * as ImagePicker from 'expo-image-picker';
import Axios from 'axios';
import {AntDesign} from '@expo/vector-icons'
import Modal from "react-native-modal";
import { BallIndicator } from 'react-native-indicators';
import {ToastAndroid} from 'react-native';

const DriverFuelReceipts = () => {
    const[amount, setAmount] = useState('')
    const[driver, setDriver] = useState('')
    const[description, setDescription] = useState('')
    const[cardNumber, setCardNumber] = useState('')
    const[unitNumber, setUnitNumber] = useState('')
    const[imgArray, setImgArray] = useState([])
    const[img, setImg] = useState()
    const[unit, setUnit] = useState()
    const[imgVisible, setImgVisible]= useState(false)
    const[render, setRender] = useState(true)
    const[imgModal, setImgModal] = useState(false)
    const[fileName,setFileName]=useState({filetype:'', filename:'', base64:''})
    const query = "arwen=rht45P56hytA54trR45tTyb91Iy54yZ097hyA65yyN69hyukl&checkNumber="+cardNumber+"&amount="+(amount * 100)+"&driverId="+driver+"&description="+description+"&userId="+"&unitId="+unitNumber
    const[uploading, setUploading] = useState(false)

    const imgUpload = async () => {
        let resul = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            // allowsEditing:true,
            base64: true,
            type: true,
            // exif: true
        })
    //    //console.log(resul)
        // let mi = mime.contentType(resul.uri)
        let fileExtension = resul.uri.substr(resul.uri.lastIndexOf('.') + 1);

        let fileNameType = resul.uri.substr(resul.uri.lastIndexOf('ImagePicker/') + 12);
        let fileName1 = fileNameType.substring(0, fileNameType.length -4);
        let mime = `${resul.type}/${fileExtension === 'jpg' ? 'jpeg' : fileExtension}`
        let size = (resul.base64.length/4)*3
        console.log(size)
        // //console.log(mime)
        setImgArray([...imgArray, {
            filesize: size, /* bytes */
            filetype: mime,
            filename: fileNameType,
            base64: resul.base64
          }])
        setImgVisible(true)
    }

    // console.log(imgArray)

    const send = () => {
        setUploading(true)
        if(cardNumber!='' && unitNumber!='' && driver!=''){
            let filter = {cardNumber:Number(cardNumber)};
            let options ={fields:"infoCapture carrierId"};

            let query = "filter=" + encodeURIComponent(JSON.stringify(filter))+"&options="+encodeURIComponent(JSON.stringify(options));
            Axios.post('https://apps.compassaws.net/api/card/getAllCards', query)
            .then(res => {
                // console.log(res)

                if(res.data.result.length > 0){
                    setUnit(res.data.result[0].infoCapture.unit)
                } else {
                    console.log('ne radi')
                    setUnit('')
                }
                // console.log(unit)
                if (unit == unitNumber){
                    let filter = {refId:res.data.result[0].carrierId};
                    let options = {fields:"email"};
                    let query = "filter=" + encodeURIComponent(JSON.stringify(filter))+"&options="+encodeURIComponent(JSON.stringify(options));

                    Axios.post('https://apps.compassaws.net/api/card/getUsers', query)
                    .then(res => {
                        console.log('drugi upit',res.data.result)
                        let finalSend=[];
                        
                        finalSend.push({"email":res.data.result, "last6":cardNumber, "location":driver, "unit":unitNumber, "slike":imgArray })
                        Axios.post('https://apps.compassaws.net/inc/sendMail1.php', finalSend)
                        .then(res => {
                            console.log(res)
                            setUploading(false)
                            ToastAndroid.show('Fuel receipts uploaded successfully', ToastAndroid.LONG)
                        })
                        .catch(err => {
                            console.log(err)
                        })
                    })
                    .catch(err =>{
                        console.log(err)
                    })
                }
                
                //console.log(res.data)
            })
            .catch(err => {
                //console.log(err)
            })
        }
    }

    const remove = (name) => {
        // //console.log(name)
        setImgArray(imgArray => imgArray.filter(item => item.filename !== name ))
    }
    
    // ////console.log(imgArray)

    return(
            <View style={styles.container}>
                <CPSlogo />
            <CPSChecksInput 
            naslov='Unit Number :'
            vrednost={unitNumber}
            postaviVrednost={(text) => setUnitNumber(text)}            
            />
            <CPSChecksInput 
            naslov='Card Number :'
            vrednost={cardNumber}
            postaviVrednost={(text) => setCardNumber(text)}            
            />
            <CPSChecksInput 
             naslov='Truck Stop Location :'
             vrednost={driver}
             postaviVrednost={(text) => setDriver(text)}             
            />
            {
                imgVisible
                ? imgArray.map((item, index) =>{
                    // console.log('ajtem nejm',item.filename)
                    console.log(item.base64.length)
                return(<View key={index} style={{justifyContent:'space-between', width:wp('80%'), paddingBottom:hp('1%'), flexDirection:'row'}}>
                    <TouchableOpacity
                    onPress={() => {setImgModal(true), setFileName({filetype:item.filetype, filename:item.filename, base64:item.base64})}}
                    >
                        <Text>{item.filename}</Text>
                        </TouchableOpacity>
                    <TouchableOpacity
                    onPress={() => {remove(item.filename), setRender(false)}}
                    > 
                        <AntDesign name='delete' size={25} color='red'/>
                    </TouchableOpacity>
                    <Image key={index} style={{width:'100%', height:'100%'}} source={{uri: `data:${item.filetype};base64,${item.base64}`}} />
                       
                    </View>
                    )
                })
                :null
            }
             <Modal
                animationType='slideInUp'
                isVisible={imgModal}
                onRequestClose={() => setImgModal(false)}
                onBackdropPress={() => setImgModal(false)}
                backdropOpacity={0.8}
                hasBackdrop={true}
                backdropColor='black'
                >
                    <Image style={{width:'100%', height:'100%'}} source={{uri: `data:${fileName.filetype};base64,${fileName.base64}`}} />
            </Modal>
                <TouchableOpacity
                style={{ 
                borderWidth:1, 
                width:'90%', 
                borderRadius:10, 
                borderColor:'#484848', 
                alignSelf:'center', 
                alignItems:'center', 
                backgroundColor:'white',
                shadowOffset: {
                    width: 0,
                    height: 6,
                },
                shadowOpacity: 0.57,
                shadowRadius: 16,
                elevation: 6,
                marginTop:hp('30%'),
                marginBottom:hp('2%')
            }}
                onPress={() => imgUpload()}
                >
                    <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:26, color:'#484848',padding:wp('2%'),textAlignVertical:'center', justifyContent:'center'}}>
                        Select Files, One by One
                        </Text>
                </TouchableOpacity>

            <View style={styles.btn}>
                <TouchableOpacity
                onPress={() => send()}
                >
                    <Text style={styles.btnTxt}>SUBMIT FUEL RECEIPTS</Text>
                </TouchableOpacity>
                <Modal
                    animationType='slideInUp'
                    isVisible={uploading}
                    onRequestClose={() => setUploading(false)}
                    onBackdropPress={() => setUploading(false)}
                    backdropOpacity={0.9}
                    hasBackdrop={true}
                    backdropColor='white'
                    >
                        <View style={{width:wp('60%'), height:hp('30%'), alignItems:'center', justifyContent:'center', alignSelf:'center'}}>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', color:Colors.zelena, fontSize:wp('6%')}}>Images Uploading</Text>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', color:Colors.zelena, fontSize:wp('6%')}}>Please Wait</Text>
                            <BallIndicator size={50} color={Colors.zelena} />
                        </View>
                    </Modal>
            </View>
            </View>
    )
};

const styles = StyleSheet.create({
    container:{
        height:'100%',
        width:'90%',
        marginHorizontal:'5%',
        alignItems: 'center',
        marginTop:hp('2%')    },
    txt:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:26,
        color:'#49a43a',
        paddingBottom:10
    },
    inputStyle:{
        width:'90%', 
        height:50, 
        borderColor:'#EEEEEE', 
        borderWidth:2,
        marginVertical:20,
        backgroundColor:'white',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.57,
        shadowRadius: 16,
        elevation: 10,
            },
        btn:{
            backgroundColor:Colors.zelena,
            width:'90%',
            borderRadius:10,
            marginTop:40,
            padding:wp('2%'),
            bottom:hp('5%')
        },
        btnTxt:{
            color:'white',
            fontFamily:'Myriad-Pro-Bold-Condensed',
            fontSize:hp('3%'),
            alignSelf:'center'
        },
        container1:{
            flexDirection:'row',
            alignItems:'center',
            width:'90%', 
            height:50, 
            borderColor:'white', 
            borderWidth:3,
            marginVertical:10,
            borderRadius:5,
            backgroundColor:'white',
            shadowOffset: {
                width: 0,
                height: 6,
            },
            shadowOpacity: 0.57,
            shadowRadius: 16,
            shadowColor:'green',
    
            elevation: 12,
            borderColor:'#d8d8d8',
            borderWidth:0.2,    
            },
            txt1:{
                fontFamily:'Myriad-Pro-Bold-Condensed',
                fontSize:20,
                color:'#8a8a8a',
                paddingLeft:10
            }
});

export default DriverFuelReceipts;