import React, { useEffect } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import CashAdvances from '../../components/CashAdvances';
import Axios from 'axios';


const CompanyCashAdvances = ({ navigation }) => {

    useEffect(() => {
        const cpsUserOK = navigation.getParam('cpsUserOK');
        let filter = {pending:true, carrierId:cpsUserOK}
        let options = { fields:"createdOn eventReason eventValue eventType cardId" }
        let query = "filter=" + encodeURIComponent(JSON.stringify(filter))+"&options="+encodeURIComponent(JSON.stringify(options));

        Axios.post('https://apps.compassaws.net/api/card/getCardEvents', query)
        .then(res => {
            console.log(res)
        })
        .catch(err => {
            console.log(err)
        })
    },[])

    return(
        <View style={styles.container}>
            <View style={{width:'100%', height:'18%', marginTop:5}}>
                <Image style={{width:'70%', height:'100%' , alignSelf:'center', paddingTop:0}} resizeMode='contain' source={require('../../img/CPSlogo.png')} />
            </View>
            <CashAdvances />
        </View>
    )
};

const styles = StyleSheet.create({
    container:{
        width:'90%',
        marginHorizontal:'5%',
        alignItems: 'center',
        height:'100%'
    }
});

export default CompanyCashAdvances;