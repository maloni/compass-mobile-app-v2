import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity} from 'react-native';
import cpsApi from '../../api/cpsApi';
import  CPSlogo  from '../../components/CPSlogo';
import LoveCompass from '../../components/LoveCompass'
import CPSMenu from '../../components/CPSMenu';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


const DriverHome = ({ navigation }) => {

    //  _fontLoad = async () => {
    //     try{
    //     await Font.loadAsync({
    //         'MYRIADPRO-SEMIBOLD': require('../../../assets/fonts/MYRIADPRO-SEMIBOLD.ttf')
    //     })
    //     } catch (error) {
    //         console.log(error)
    //     }
    // }

    return(
           <View style={styles.container}> 
                <CPSlogo />
                    <View style={styles.cpsMenu}>
                        <CPSMenu title='CASH REQUEST' navigacija={() => navigation.navigate('DriverCashRequest')}/>
                        <CPSMenu title='SUBMIT FUEL RECEIPT' navigacija={() => navigation.navigate('DriverFuelReceipts')}/>
                        <CPSMenu title='MERCHANT LOCATION' navigacija={() => navigation.navigate('DriverMerchantLocation')}/>
                        <CPSMenu title='STORED CARDS' navigacija={() => navigation.navigate('StoredCards')}/>
                    </View>
                <View style={{position:'absolute', bottom:0, marginBottom:hp('5%'), justifyContent:'center'}}>
                    <LoveCompass 
                    nav={() => navigation.navigate('LoveCompassScreen')}
                    />
                </View>
            </View>
            
    )
    
};




const styles = StyleSheet.create({
    container: {
        height:'100%',
        width:'90%',
        marginHorizontal:'5%',
        alignItems: 'center',
        marginTop:hp('2%')
    },
        btn:{
        width:'70%',
        alignSelf:'center',
        marginTop:'15%',
        height:'10%', 
        borderWidth:3, 
        borderColor:'#49a43a', 
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
          width: 10,
          height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#484848',
        elevation: 12,
        
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    },
    cpsMenu:{
        marginTop:hp('15%'),
        justifyContent:'flex-end',
        borderBottomWidth:3,
        borderBottomColor:'#d0e7cc',
        marginHorizontal:10
    }

      
});

export default DriverHome;