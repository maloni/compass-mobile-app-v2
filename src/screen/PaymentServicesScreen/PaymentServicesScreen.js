import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity} from 'react-native';
import cpsApi from '../../api/cpsApi';
import Axios from 'axios';
import * as Font from 'expo-font';
import  CPSlogo  from '../../components/CPSlogo';
import LoveCompass from '../../components/LoveCompass'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';


const PaymentServicesScreen = ({ navigation }) => {

   

    return(
           <View style={styles.container}> 
                <CPSlogo />
                    <TouchableOpacity  
                    style={{...styles.btn, alignItems:'center', alignContent:'center'}}
                    onPress={() => navigation.navigate('PaymentCompany')}>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:wp('2%')}}>COMPANY LOGIN</Text>
                    </TouchableOpacity>
                    <TouchableOpacity  
                    style={{...styles.btn,  alignItems:'center'}}
                    onPress={() => navigation.navigate('DriverHome')}>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:wp('2%')}}>DRIVER LOGIN</Text>
                    </TouchableOpacity>
                <Text style={{...styles.txt, marginTop:'10%'}}>Don't have CPS fuelAccount?</Text>
                <View style={{flexDirection:'row', alignSelf:'center'}}>
                <Text style={{...styles.txt, color:'#49a43a'}}>Click here</Text><Text style={styles.txt}>to apply and start earning</Text></View>
                <Text style={{...styles.txt, marginBottom:'10%'}}>discounts today!</Text>
                <View style={{alignSelf:'center', position:'absolute', bottom:hp('6%'), width:wp('80%')}}>
                    <LoveCompass/>
                </View>
            </View>
    )
    
};


const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        height:'100%',
        marginTop:Constants.statusBarHeight
    },
        btn:{
        width:'70%',
        marginTop:hp('4%'),
        alignSelf:'center',
        borderWidth:3, 
        borderColor:'#49a43a', 
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
          width: 10,
          height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#484848',
        elevation: 12,
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    }
      
});

export default PaymentServicesScreen;