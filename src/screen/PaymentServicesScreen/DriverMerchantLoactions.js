import React, { useState, useContext, useEffect } from 'react';
import { Text, View, StyleSheet, Image,TextInput } from 'react-native';
import CPSChecksInput from '../../components/CPSChecksInput';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Colors from '../../../assets/Colors';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import CPSlogo from '../../components/CPSlogo';

const DriverMerchantLocation = () => {
    const[amount, setAmount] = useState('')
    const[driver, setDriver] = useState('')
    const[description, setDescription] = useState('')
    const[cardNumber, setCardNumber] = useState('')
    const[unitNumber, setUnitNumber] = useState('')

        const query = "arwen=rht45P56hytA54trR45tTyb91Iy54yZ097hyA65yyN69hyukl&checkNumber="+cardNumber+"&amount="+(amount * 100)+"&driverId="+driver+"&description="+description+"&userId="+"&unitId="+unitNumber


    return(
            <View style={styles.container}>
                <CPSlogo />
            <CPSChecksInput 
            naslov='Card Number :'
            vrednost={cardNumber}
            postaviVrednost={(text) => setCardNumber(text)}            
            />
            <CPSChecksInput 
            naslov='Amount :'
            vrednost={amount}
            postaviVrednost={(text) => setAmount(text)}            
            />
            <CPSChecksInput 
            naslov='Unit Number :'
            vrednost={unitNumber}
            postaviVrednost={(text) => setUnitNumber(text)}            
            />
            <CPSChecksInput 
             naslov='Driver :'
             vrednost={driver}
             postaviVrednost={(text) => setDriver(text)}             
            />
            <CPSChecksInput 
            naslov='Description :'
            vrednost={description}
            postaviVrednost={(text) => setDescription(text)}            
            />
            <View style={styles.btn}>
                <TouchableOpacity>
                    <Text style={styles.btnTxt}>CREATE CHECK</Text>
                </TouchableOpacity>
            </View>
            </View>
    )
};

const styles = StyleSheet.create({
    container:{
        width:'90%',
        marginHorizontal:'5%',
        alignItems: 'center',
        marginTop:hp('2%')
    },
    txt:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:26,
        color:'#49a43a',
        paddingBottom:10
    },
    inputStyle:{
        width:'90%', 
        height:50, 
        borderColor:'#EEEEEE', 
        borderWidth:2,
        marginVertical:20,
        backgroundColor:'white',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.57,
        shadowRadius: 16,
        elevation: 10,
            },
        btn:{
            backgroundColor:Colors.zelena,
            width:'50%',
            borderRadius:10,
            marginTop:40,
            padding:7
        },
        btnTxt:{
            color:'white',
            fontFamily:'Myriad-Pro-Bold-Condensed',
            fontSize:30,
            alignSelf:'center'
        },
        container1:{
            flexDirection:'row',
            alignItems:'center',
            width:'90%', 
            height:50, 
            borderColor:'white', 
            borderWidth:3,
            marginVertical:10,
            borderRadius:5,
            backgroundColor:'white',
            shadowOffset: {
                width: 0,
                height: 6,
            },
            shadowOpacity: 0.57,
            shadowRadius: 16,
            shadowColor:'green',
    
            elevation: 12,
            borderColor:'#d8d8d8',
            borderWidth:0.2,    
            },
            txt1:{
                fontFamily:'Myriad-Pro-Bold-Condensed',
                fontSize:20,
                color:'#8a8a8a',
                paddingLeft:10
            }
});

export default DriverMerchantLocation;