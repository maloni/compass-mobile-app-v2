import React, { useState } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView } from 'react-native';
import CPSStatements from '../../components/CPSStatements';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import CPSlogo from '../../components/CPSlogo'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import Axios from 'axios';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';


const CompanyStatements= ({ navigation }) => {
    const [showDatePickerFrom, setShowDatePickerFrom] = useState(false);
    const [showDatePickerTo, setShowDatePickerTo] = useState(false);
    const [result, setResult] = useState()

    const[dateFrom, setDateFrom] = useState(new Date)
    const[dateTo, setDateTo] = useState(new Date)
    
    const dateFromText = moment(dateFrom).format('MMM DD, YYYY')
    const dateToText = moment(dateTo).format('MMM DD, YYYY')

    const dateFromSend = moment(dateFrom).format('YYYY-MM-DD')
    const dateToSend = moment(dateTo).format('YYYY-MM-DD')

    const cpsUserOK = navigation.getParam('cpsUserOK');


    const statements = () => {
        // let filter = {carrierId:cpsUserOK,tdate:{ $gte:dateFrom, $lte:dateTo}};
        // var options ={fields:"fuel cash info carrierCashAdvanceFee carrierFee oil other"};
        // let query = "filter=" + encodeURIComponent(JSON.stringify(filter))+"&options="+encodeURIComponent(JSON.stringify(options));
        var info="carrierId="+cpsUserOK+"&startDate="+dateFromSend+"&endDate="+dateToSend;
        Axios.post('https://apps.compassaws.net/api/card/getStatement', info)
        .then(res => {
            // console.log(res)
            setResult(res.data.result)
        })
        .catch(err => {
            console.log(err)
        })
    }

    

    console.log(result)

    return(
        <SafeAreaView style={{width:'100%', height:'100%'}}>
        <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
                <View style={styles.container} >
                <CPSlogo />
            <View style={{flexDirection:'row', justifyContent:'space-between', width:'80%', alignSelf:'center'}}>
                <TouchableOpacity
                style={styles.date}
                onPress={() => setShowDatePickerFrom(true)}
                >
                    <Text>Select Start Date</Text>
                </TouchableOpacity>
                {showDatePickerFrom
                    ?<DateTimePicker
                    mode="date"
                    display="spinner"
                    defaultDate={new Date()}
                    value={new Date()}
                    onChange={(event, value) => {
                    setShowDatePickerFrom(false);
                    setDateFrom(value);
                    }}
                        />
                    :null
                }
                <TouchableOpacity
                style={styles.date}
                onPress={() => setShowDatePickerTo(true)}
                >
                    <Text>Select Start Date</Text>
                </TouchableOpacity>
                {showDatePickerTo
                    ?<DateTimePicker
                    mode="date"
                    display="spinner"
                    defaultDate={new Date()}
                    value={new Date()}
                    onChange={(event, value) => {
                    setShowDatePickerTo(false);
                    setDateTo(value);
                    statements()
                    }}
                        />
                    :null
                }
            </View>     
            <View style={{flexDirection:'row', alignSelf:'center', width:'80%', justifyContent:'space-between',padding:10 }}>
                <Text>{dateFromText}</Text>
                <Text>{dateToText}</Text>
            </View>   
            <CPSStatements 
            result={result}
            dateFromSend={dateFromSend}
            dateToSend={dateToSend}
            cpsUserOK={cpsUserOK}
            />
        </View>
        </KeyboardAwareView>
        </SafeAreaView>

    )
};

const styles = StyleSheet.create({
    container:{
        marginHorizontal:'5%',
        alignItems: 'center',
        height:hp('100%')
    },
    date:{
        borderWidth:1,
        padding:10
    }

});

export default CompanyStatements;