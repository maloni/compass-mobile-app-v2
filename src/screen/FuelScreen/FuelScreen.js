import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity, SafeAreaView, ScrollView,Platform} from 'react-native';
import  CFuelLogo  from '../../components/CFuelLogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import LoveCompass from '../../components/LoveCompass';
import Axios from 'axios';
import { FlatList } from 'react-native-gesture-handler';

const FuelScreen = ({ navigation }) => {

    const[deals, setDeals] = useState()

    useEffect(() => {
        Axios.get('https://apps.compassaws.net/api/v1/fuelDeals')
       .then(res => {
           console.log(res.data)
           setDeals(res.data)
           
       })
       .then(err => {
           console.log(err)
       })
   }, [])


    return(
    <SafeAreaView style={{width:'100%', height:'100%'}}>
    <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
        <View      
            style={styles.container}           
        >
                <CFuelLogo />

                {/* <FlatList 
                data={deals}
                keyExtractor={(item) => item.id}
                renderItem={({ item }) => {
                    return(
                        <View>
                            <Text>{item.dealName}</Text>
                            <Text>{item.dealText}</Text>
                            <Image 
                            style={{width:'60%', resizeMode:'contain'}}
                            source={{uri:`http://apps.compassaws.net/fuelDeals/${item.dealImg}`}}
                            />
                        </View>
                    )
                }}
                /> */}
                <Text style={{...styles.txt, marginTop:hp('2%')}}>A Truck Stop and a Gas Station with competitive fuel prices 
                and a convenience store. All major fuel cards accepted!</Text>
                    <Image style={{width:wp('90%'), height:hp('25%'), marginTop:hp('2%')}} resizeMode='contain' source={require('../../img/compassFuel.jpg')} />
                <Text style={{...styles.txt, marginTop:hp('2%')}}>8147 Joliet Rd, McCook, IL 60525</Text>
                <Text style={{...styles.txt, color:'#cead02'}}>WE ARE OPEN 24/7!</Text>
                <View style={{flexDirection:'row', justifyContent:'space-between', marginTop:hp('4%')}}>
                    <TouchableOpacity style={styles.btn}><Text style={{...styles.txt, color:'white'}}>Email Us</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.btn}><Text style={{...styles.txt, color:'white'}}>Call Us</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.btn}><Text style={{...styles.txt, color:'white'}}>Review Us</Text></TouchableOpacity>
                </View>
                    <LoveCompass />
            </View>
    </KeyboardAwareView>
    </SafeAreaView>
    )
};


FuelScreen.navigationOptions = ({ navigation }) => {
    return{
        header: null,
    }
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        height:'90%',
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent',        
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    },
    btn:{
        backgroundColor:'#cead02',
        padding:wp('3%'),
        borderRadius:8
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default FuelScreen;