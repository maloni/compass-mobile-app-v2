import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity, SafeAreaView, ScrollView,Platform, ImageBackground} from 'react-native';
import  CTLlogo  from '../../components/CTLlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import LoveCompass from '../../components/LoveCompass';
import Arenalogo from '../../components/Arenalogo';
import ArenaBtn from '../../components/ArenaBtn';
import moment from 'moment'; 
import {MaterialIcons, MaterialCommunityIcons, FontAwesome} from '@expo/vector-icons'
import DateTimePicker from '@react-native-community/datetimepicker';


const FairPlayBook = ({ navigation }) => {
    const [showDatePicker, setShowDatePicker] = useState(false);
    const [selectedDate, setSelectedDate] = useState(new Date()); 

    var currentDate = moment().format("DD/MM/YYYY");
    var currentTime = moment().format('LT');


    return(
        <SafeAreaView style={{width:'100%', height:'100%'}}>
        <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
            <ScrollView      
                contentContainerStyle={styles.container}           
                automaticallyAdjustContentInsets={true}
                showsVerticalScrollIndicator={false}
                bounces={true}
            >
                <Arenalogo />
                <ImageBackground style={{width:wp('100%'), height:hp('14%'), marginTop:hp('4%')}} resizeMode='cover' source={require('../../img/event.jpg')}>
                <View style={styles.overlay}>
                    <Text style={styles.bookTxt}>BOOK EVENT</Text>
                </View>
                </ImageBackground>
                <View style={{width:wp('80%'), alignSelf:'center'}}>
                    <Text style={{...styles.txt, marginTop:hp('4%')}}>Name :</Text>
                    <TextInput 
                    style={styles.input}
                    />
                    <Text style={styles.txt}>Email :</Text>
                    <TextInput 
                    style={styles.input}
                    />
                    <Text style={styles.txt}>Name :</Text>
                    <TextInput 
                    style={styles.input}
                    />
                    <Text style={styles.txt}>Type of Event :</Text>
                    <TextInput 
                    style={styles.input}
                    />
                    <Text style={styles.txt}>Which room would you like to book? :</Text>
                    <TextInput 
                    style={styles.input}
                    />
                </View>
                </ScrollView>
        </KeyboardAwareView>
        </SafeAreaView>
        )
    };
    
const styles = StyleSheet.create({
    container: {
        width:wp('100%'),
        height:hp('90%'),
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent',
    },
    input:{
        width:'90%',
        borderWidth:0.4,
        borderRadius:10,
        paddingVertical:hp('0.8%'),
        backgroundColor:'#ebebeb',
        paddingLeft:wp('1%')
    },
    bookTxt:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:wp('10%'),
        color:'white',
        alignSelf:'center'
    },
    overlay:{
        width:'100%',
        height:'100%',
        backgroundColor:'rgba(41,40,112,0.8)',
        justifyContent:'center'
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:wp('4%'),
        marginVertical:hp('1%')
    },
    btn:{
        backgroundColor:'white',
        marginVertical:hp('1%'),
        width:'80%',
        alignSelf:'center',
        borderWidth:3, 
        borderColor:'#da212f', 
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
        width: 10,
        height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#484848',
        elevation: 8,
        
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default FairPlayBook;