import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity, SafeAreaView, ScrollView,Platform} from 'react-native';
import  CTLlogo  from '../../components/CTLlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import LoveCompass from '../../components/LoveCompass';
import Arenalogo from '../../components/Arenalogo';
import ArenaBtn from '../../components/ArenaBtn';

const FairPlayHome = ({ navigation }) => {

    return(
        <SafeAreaView style={{width:'100%', height:'100%'}}>
        <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
            <ScrollView      
                contentContainerStyle={styles.container}           
                automaticallyAdjustContentInsets={true}
                showsVerticalScrollIndicator={false}
                bounces={true}
            >
                <Arenalogo />
                {/* <Text style={{...styles.txt, alignSelf:'flex-start'}}>Next Event</Text>
                <View style={{borderWidth:3, borderColor:'#e09195', marginTop:10, height:'30%', borderRadius:10}}>
                    <Text style={{...styles.txt}}>Super Bowl 2020</Text>
                    <Image 
                        resizeMethod='resize'
                        resizeMode='contain'
                        style={{width:'80%', height:'70%', alignSelf:'center'}}
                        source={require('../../img/super.jpg')}/>
                    <Text style={styles.txt}>02.03.2020 at: 9pm</Text>
                </View> */}
                <View>
                <ArenaBtn 
                title='Book Event'
                navigacija={() => navigation.navigate('FairPlayBook')}
                />
                <ArenaBtn 
                title='Upcoming Events'
                navigacija={() => navigation.navigate('FairPlayEvents')}
                />
                <ArenaBtn 
                title='Reservation'
                navigacija={() => navigation.navigate('Reservation')}
                />
                <ArenaBtn 
                title='Gallery'
                // navigacija={() => navigation.navigate('')}
                />
                    </View>
                    <View style={{alignSelf:'center', width:wp('80%'), position:'absolute', bottom:hp('2%')}}>
                        <LoveCompass />
                    </View>

                </ScrollView>
        </KeyboardAwareView>
        </SafeAreaView>
        )
    };
    
const styles = StyleSheet.create({
    container: {
        width:wp('90%'),
        marginHorizontal:'5%',
        height:hp('90%'),
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent',
        flex:1
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    },
    btn:{
        backgroundColor:'white',
        marginVertical:hp('1%'),
        width:'80%',
        alignSelf:'center',
        borderWidth:3, 
        borderColor:'#da212f', 
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
        width: 10,
        height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#484848',
        elevation: 8,
        
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default FairPlayHome;