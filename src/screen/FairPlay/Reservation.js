import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity, SafeAreaView, ScrollView,Platform} from 'react-native';
import  CTLlogo  from '../../components/CTLlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import LoveCompass from '../../components/LoveCompass';
import Arenalogo from '../../components/Arenalogo';
import ArenaBtn from '../../components/ArenaBtn';
import moment from 'moment'; 
import {MaterialIcons, MaterialCommunityIcons, FontAwesome} from '@expo/vector-icons'
import DateTimePicker from '@react-native-community/datetimepicker';


const Reservation = ({ navigation }) => {
    const [showDatePicker, setShowDatePicker] = useState(false);
    const [selectedDate, setSelectedDate] = useState(new Date()); 

    var currentDate = moment().format("DD/MM/YYYY");
    var currentTime = moment().format('LT');


    return(
        <SafeAreaView style={{width:'100%', height:'100%'}}>
        <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
            <ScrollView      
                contentContainerStyle={styles.container}           
                automaticallyAdjustContentInsets={true}
                showsVerticalScrollIndicator={false}
                bounces={true}
            >
                <Arenalogo />
                <View style={{width:'80%', alignSelf:'center', alignItems:'center', marginTop:20}}>
                        <Text style={{fontSize:50, fontFamily:'Myriad-Pro-Bold-Condensed', color:'#e32b35'}}>Make a</Text>
                        <Text style={{fontSize:40, fontFamily:'Myriad-Pro-Bold-Condensed', color:'#484848'}}>R E S E R V A T I O N</Text>
                        <Text style={{color:'#484848',textAlign:'center', fontSize:18, fontFamily:'Myriad-Pro-Condensed'}}>
                            Booking a table online is easy and takes {'\n'}
                            just a couple of minutes
                            </Text>
                        <View style={{width:'90%', marginTop:20}}>
                            <View style={{width:'100%', flexDirection:'row', justifyContent:'space-between'}}>
                                <TouchableOpacity style={{width:'45%', borderColor:'#e32b35', borderWidth:1, padding:5,paddingHorizontal:10, flexDirection:'row', justifyContent:'center', alignItems:'center', borderRadius:10}}>
                                    <Text style={{fontSize:18, color:'#e32b35', fontFamily:'Myriad-Pro-Condensed', paddingRight:8}}>{currentDate}</Text>
                                    <MaterialIcons 
                                    name='date-range'
                                    color='#e32b35'
                                    size={30}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity style={{width:'45%', borderColor:'#e32b35', borderWidth:1, padding:5,paddingHorizontal:10, flexDirection:'row', justifyContent:'center', alignItems:'center', borderRadius:10}}>
                                    <Text style={{fontSize:18, color:'#e32b35', fontFamily:'Myriad-Pro-Condensed', paddingRight:8}}>{currentTime}</Text>
                                    <MaterialIcons 
                                    name='access-time'
                                    color='#e32b35'
                                    size={30}
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={{width:'100%', borderWidth:1, borderColor:'#e32b35', justifyContent:'space-between', flexDirection:'row', alignItems:'center', marginTop:20, paddingHorizontal:10, padding:2, borderRadius:10}}>
                                <Text style={{fontSize:22, color:'#e32b35', fontFamily:'Myriad-Pro-Condensed', paddingRight:8, padding:4}}>Marko Petrovic</Text>
                            </View>
                            <View style={{width:'100%', borderWidth:1, borderColor:'#e32b35', justifyContent:'space-between', flexDirection:'row', alignItems:'center', marginTop:20, paddingHorizontal:10, padding:2, borderRadius:10}}>
                                <Text style={{fontSize:22, color:'#e32b35', fontFamily:'Myriad-Pro-Condensed', paddingRight:8}}>4 People</Text>
                                <MaterialCommunityIcons
                                name='account-group'
                                color='#e32b35'
                                size={35}                                
                                />
                            </View>

                            <TouchableOpacity
                            style={{width:'100%', backgroundColor:'#e32b35', marginTop:20, alignItems:'center', borderRadius:10}}
                            >
                                <Text style={{...styles.txt, padding:4, fontSize:30, color:'white'}}>RESERVE</Text>
                            </TouchableOpacity>
                        </View>
                        </View>
                    <View style={{alignSelf:'center', width:wp('80%'), position:'absolute', bottom:hp('2%')}}>
                        <LoveCompass />
                    </View>

                </ScrollView>
        </KeyboardAwareView>
        </SafeAreaView>
        )
    };
    
const styles = StyleSheet.create({
    container: {
        width:wp('90%'),
        marginHorizontal:'5%',
        height:hp('90%'),
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent',
        flex:1
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    },
    btn:{
        backgroundColor:'white',
        marginVertical:hp('1%'),
        width:'80%',
        alignSelf:'center',
        borderWidth:3, 
        borderColor:'#da212f', 
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
        width: 10,
        height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#484848',
        elevation: 8,
        
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default Reservation;