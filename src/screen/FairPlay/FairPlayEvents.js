import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity, SafeAreaView, ScrollView,Platform} from 'react-native';
import  CTLlogo  from '../../components/CTLlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import LoveCompass from '../../components/LoveCompass';
import Arenalogo from '../../components/Arenalogo';
import {Entypo} from '@expo/vector-icons';

const FairPlayEvents = ({ navigation }) => {

    return(
        <SafeAreaView style={{width:'100%', height:'100%'}}>
        <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
            <ScrollView      
                contentContainerStyle={styles.container}           
                automaticallyAdjustContentInsets={true}
                showsVerticalScrollIndicator={false}
                bounces={true}
            >
                    <Arenalogo />

                    <View style={styles.eventContainer}>
                        <View style={styles.eventContainerImg}>
                            <Image style={{width:'100%', height:'100%', borderRadius:10}} resizeMode='cover' source={require('../../img/network1.jpg')}/>
                        </View>
                        <View style={styles.eventInfo}>
                            <Entypo 
                            name='calendar'
                            size={wp('9%')}
                            />
                            <View style={{width:'50%', paddingLeft:wp('2%')}}>
                                <Text style={{...styles.txtInfo, fontSize:wp('4%')}}>Networking event</Text>
                                <Text style={styles.txtInfo}>Friday 7:00pm</Text>
                                <Text style={styles.txtInfo}>Where: The Whiskey Bar</Text>
                            </View>
                            <View style={{width:'35%'}}>
                                <TouchableOpacity
                                style={styles.btnTicket}
                                >
                                    <Text style={styles.btnText}>BUY TICKETS</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={styles.eventContainer}>
                        <View style={styles.eventContainerImg}>
                            <Image style={{width:'100%', height:'100%', borderRadius:10}} resizeMode='cover' source={require('../../img/network.jpg')}/>
                        </View>
                        <View style={styles.eventInfo}>
                            <Entypo 
                            name='calendar'
                            size={wp('9%')}
                            />
                            <View style={{width:'50%', paddingLeft:wp('2%')}}>
                                <Text style={{...styles.txtInfo, fontSize:wp('4%')}}>Networking event</Text>
                                <Text style={styles.txtInfo}>Friday 7:00pm</Text>
                                <Text style={styles.txtInfo}>Where: The Whiskey Bar</Text>
                            </View>
                            <View style={{width:'35%'}}>
                                <TouchableOpacity
                                style={styles.btnTicket}
                                >
                                    <Text style={styles.btnText}>BUY TICKETS</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View style={styles.eventContainer}>
                        <View style={styles.eventContainerImg}>
                            <Image style={{width:'100%', height:'100%', borderRadius:10}} resizeMode='cover' source={require('../../img/network2.jpg')}/>
                        </View>
                        <View style={styles.eventInfo}>
                            <Entypo 
                            name='calendar'
                            size={wp('9%')}
                            />
                            <View style={{width:'50%', paddingLeft:wp('2%')}}>
                                <Text style={{...styles.txtInfo, fontSize:wp('4%')}}>Networking event</Text>
                                <Text style={styles.txtInfo}>Friday 7:00pm</Text>
                                <Text style={styles.txtInfo}>Where: The Whiskey Bar</Text>
                            </View>
                            <View style={{width:'35%'}}>
                                <TouchableOpacity
                                style={styles.btnTicket}
                                >
                                    <Text style={styles.btnText}>BUY TICKETS</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                    {/* <View style={{width:'90%', height:'20%', borderWidth:3, borderColor:'#e09195', alignSelf:'center', marginTop:15, borderRadius:10, flexDirection:'row'}}>
                        <View style={{width:'30%', height:'80%', backgroundColor:'#e32b35', margin:'5%', borderRadius:10}}>
                            <Text style={{...styles.txt, fontSize:45, color:'white'}}>09</Text>
                            <Text style={{...styles.txt, fontSize:30, color:'white'}}>MAY</Text>
                        </View>
                        <View style={{width:'62%'}}>
                        <Text style={{...styles.txt, alignSelf:'flex-start', marginTop:10, fontSize:30}}>
                            Supper Bowl
                        </Text>
                        <Text style={{...styles.txt, fontSize:16, paddingRight:10}}>The Super Bowl is finally here, and it’s officially time to get excited!</Text>
                        <Text style={{...styles.txt, alignSelf:'flex-start', marginTop:15}}>Time: 9pm</Text>
                        </View>
                    </View> */}

                    <TouchableOpacity
                    style={{width:wp('45%'), alignSelf:'center', alignItems:'center', backgroundColor:'rgb(41,40,112)', paddingVertical:hp('1%'), borderRadius:10, marginTop:hp('2%') }}
                    >
                        <Text style={styles.btnText}>BOOK EVENT</Text>
                    </TouchableOpacity>

                </ScrollView>
        </KeyboardAwareView>
        </SafeAreaView>
        )
    };
    
const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        paddingBottom: wp('10%')
    },
    txtInfo:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:wp('3%')
    },
    btnText:{
        color:'white',
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:wp('5%')
    },
    btnTicket:{
        backgroundColor:'#da212f',
        alignItems:'center',
        borderRadius:10,
        paddingVertical:hp('1.3%'),
        width:'100%'
    },
    eventInfo:{
        width:'100%',
        height:'30%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    },
    eventContainer:{
        width:wp('90%'),
        height:hp('30%'),
        marginTop:hp('3%'),
        flexDirection:"column"
    },
    eventContainerImg:{
        width:'100%',
        height:'70%',
        borderRadius:10,
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    },
    btn:{
        backgroundColor:'white',
        marginVertical:hp('1%'),
        width:'80%',
        alignSelf:'center',
        borderWidth:3, 
        borderColor:'#da212f', 
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
        width: 10,
        height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#484848',
        elevation: 8,
        
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default FairPlayEvents;