import React, { useState, useEffect, useContext } from 'react';
import { Text, View, StyleSheet, Image, AsyncStorage } from 'react-native';
import Constants from 'expo-constants';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import ITAInput from '../../components/ITAInput';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import Axios from 'axios';
import { BarIndicator } from 'react-native-indicators';
import CompassContext from '../../context/BlogContext';


const ItaLogInHome = ({ navigation }) => {

    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [loading, setLoading] = useState(false);
    const [id, setId] = useState();

    const user = {
        username: username,
        password: password,
        id: id
    };
    
    const { setItaId, itaId } = useContext(CompassContext);



    useEffect(() => {
        _retriveData();
    },[])

    //console.log(username,password)

    const send = function(user,password, mile){
        const data = 'email='+user+'&password='+password;
            Axios.post('https://apps.compassaws.net/api/v1/login', data)
                .then(response => {
                    const odgovor = response.data
                           if(!odgovor.error && mile =='prihvati'){
                                setItaId(odgovor.id)
                                //console.log(odgovor);
                                _saveUser();
                                navigation.navigate('ItaAccountScreen', { userResponse: odgovor})                  
                           } else if (!odgovor.error && mile=='snimi'){ 
                            setId(odgovor.id)
                            navigation.navigate('ItaAccountScreen', { userResponse: odgovor, })}
                           else alert(JSON.stringify(odgovor))
                    })
                .catch(error => {
                         console.log(error.response)
                    })
    };

    _saveUser = async () => {
        try {
            await AsyncStorage.setItem('UserITA', JSON.stringify(user));
        } catch (error){
            console.log(error);
        }
    };

    _retriveData = async () => {
        try{
            const userStorage = await AsyncStorage.getItem('UserITA');
            if (userStorage !== null) {
                const parsovano = JSON.parse(userStorage);
                send(parsovano.username, parsovano.password, 'snimi');
                //console.log(parsovano);
            }   else if (userStorage == null){
              setLoading(true);
            }
            
        } catch (error) {
    
        };
    };



    return(       
    <View style={{flex:1}}>
        {!loading 
        ? <BarIndicator size={80} color="#20316basd"  style={{justifyContent:'center'}}/>     
        :<KeyboardAwareView style={{flex:1}}>
            <ScrollView style={{flex:1}}>
            <View style={styles.container}>
                <View style={styles.img}>
                    <Image 
                    resizeMode='contain'
                    resizeMethod='resize'
                    source={require('../../img/ITALogo.png')}
                    style={{width:'100%'}}
                    />
                    <Text style={{fontSize:20, fontWeight:'bold', color:'#20316b', alignSelf:'center'}}
                    >Where Success Begins</Text>
                </View>
                <ITAInput 
                value={username}
                placeholder='Username'
                onChangeText={(text) => setUsername(text)}
                secure={false}
                />
                <ITAInput 
                value={password}
                placeholder='Password'
                onChangeText={(text) => setPassword(text)}
                secure={true}
                />
                <View style={styles.btnContainer}>
                    <TouchableOpacity
                    onPress={() => send(username,password,'prihvati')}
                    >
                        <View style={styles.btn}>
                            <Text style={styles.btnTxt}>
                                LOG IN
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>  
            </View>             
            </ScrollView>
        </KeyboardAwareView>
        }
    </View>

    )
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        height:'90%',
        flex:1,
        alignItems:'center',
        marginTop: Constants.statusBarHeight,
    },
    img: {
        width:'75%',
        paddingBottom:0
    },
    btnContainer:{
        width:'80%',
        height:'30%',
        marginTop:30
    },
    btn:{
        borderWidth:1, 
        borderRadius:10, 
        paddingVertical:15, 
        alignItems:'center',
        backgroundColor:'#20316b',
        marginTop:30
    },
    btnTxt:{
        fontSize:22,
        color:'white',
        fontFamily:'Myriad-Pro-Condensed'
    }
});

export default ItaLogInHome;