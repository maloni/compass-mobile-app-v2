import React from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import Constants from 'expo-constants';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import { ScrollView } from 'react-native-gesture-handler';


const ItaCarrier = () => {

    return(
        <KeyboardAwareView style={{flex:1}}>
            <ScrollView style={{flex:1}}>     
            <View style={styles.container}>
            <View style={styles.img}>
                <Image 
                resizeMode='contain'
                resizeMethod='resize'
                source={require('../../img/ITALogo.png')}
                style={{width:'100%', position:'relative', height:'90%'}}
                />
                <Text style={{fontSize:14, fontWeight:'bold', color:'#20316b', alignSelf:'center'}}
                >Where Success Begins</Text>
            </View>
            <View style={{height: hp('65%'), width: wp('80%')}}>
            <View style={styles.txtContainer}>
                <Text style={styles.txtHeader}>OPEN NEW TRUCKING AUTHORITY</Text>
                <Text style={styles.txtDesc}>Obtain MC and DOT numbers</Text>
            </View>
            <View style={styles.txtContainer}>
                <Text style={styles.txtHeader}>KEEP YOUR FLEET COMPLIANT</Text>
                <Text style={styles.txtDesc}>UCR</Text>
                <Text style={styles.txtDesc}>IFTA Sticker</Text>
                <Text style={styles.txtDesc}>State Permits</Text>
                <Text style={styles.txtDesc}>Annual Highway Use Tax Filings</Text>
            </View>
            <View style={styles.txtContainer}>
                <Text style={styles.txtHeader}>TRAIN YOUR DRIVERS</Text>
                <Text style={styles.txtDesc}>Safety Training Program for your drivers</Text>
            </View>
            <View style={styles.txtContainer}>
                <Text style={styles.txtHeader}>BENEFITS & DISCOUNTS</Text>
            </View>
            <TouchableOpacity style={{width:'100%'}}>
                <View style={{backgroundColor:'#267937', alignItems:'center', width:'100%', borderRadius:10, marginTop:hp('1%')}}>
                    <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:wp('5%'), padding:hp('2%'), color:'white'}}>
                        START YOUR BUSINESS TODAY
                    </Text>
                </View>
            </TouchableOpacity>
            </View>
            </View>
            </ScrollView>
            </KeyboardAwareView>
    )
}

const styles = StyleSheet.create({
    container: {
        width:wp('90%'),
        alignSelf:'center',
        height:hp('85%'),
        flex:1,
        alignItems:'center',
        marginTop: Constants.statusBarHeight,
    },
    img: {
        width:wp('40%'),
        justifyContent:'space-around',
        height:hp('25%'),
        paddingBottom:5
    },
    txtContainer:{
        borderWidth:2,
        width:wp('80%'),
        marginBottom:hp('1%'),
        padding:hp('1%'),
        alignItems:'center',
        borderRadius:10,
        borderColor:'#20316b'
    },
    txtHeader:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#20316b',
        fontSize:wp('6%')
    },
    txtDesc:{
        fontFamily:'Myriad-Pro-Condensed',
        color:'#20316b',
        fontSize:wp('5%')
    }
});

export default ItaCarrier;