import React, { useState, useContext, useEffect } from 'react';
import { Text, View, StyleSheet, Image, TextInput, ScrollView, KeyboardAvoidingView, AsyncStorage } from 'react-native';
import Constants from 'expo-constants';
import { MaterialIcons } from '@expo/vector-icons';
import LoveCompass from '../../components/LoveCompass';
import { TouchableOpacity } from 'react-native';
import CompassContext from '../../context/BlogContext';


const ItaAccountScreen = ({ navigation }) => {

    const [account, setAccount] = useState(navigation.getParam('userResponse'))

         const { setItaId, itaId } = useContext(CompassContext);

        //console.log(marko);
        //console.log(account.id)
    // useEffect(() => {
    //     it.changeID(account.id)
    // },[])
    //console.log(itaId)

    return(
        <CompassContext.Consumer>
            { id =>
        <View style={styles.container}>
            <View style={styles.headerContainer}>
                <MaterialIcons 
                size={70}
                color='#484848'
                name='account-circle'
                />
                <Text style={{fontSize:60, fontFamily:'Myriad-Pro-Bold-Condensed', color:'#484848', paddingLeft:'5%', paddingRight:'4%'}}>
                    Account
                </Text>
                <View style={{width:'30%',height:'90%', alignItems:'center', alignSelf:'center', justifyContent:'center'}}>
                    <Image 
                        resizeMode='center'
                        source={require('../../img/ITALogo.png')}
                        resizeMethod='scale'
                        style={{alignSelf:'center', width:'70%'}}
                    />
                </View>                
            </View>
            <View style={{height:'15%', justifyContent:'center',borderBottomColor:'#e5e5e6',borderBottomWidth:5}}>
                <LoveCompass />
            </View>
            <KeyboardAvoidingView
          behavior="padding"
        >
            <View style={{marginTop:15}}>
                <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:26, color:'#484848', alignSelf:'flex-start'}}>
                    Personal Information :
                </Text>
                <TextInput 
                //placeholder={account.name}
                style={styles.input}
                placeholderTextColor='black'
                value={account.name}
                />
                <TextInput 
                placeholder='Company Owner'
                style={styles.input}
                placeholderTextColor='black'

                />         
                <TextInput 
                value={account.email}
                style={styles.input}
                placeholderTextColor='black'

                />                
                <TextInput 
                placeholder='(312) 520-5276'
                style={styles.input}
                placeholderTextColor='black'

                />
                
            </View>
            </KeyboardAvoidingView>
            <View style={{width:'100%', alignItems:'center', height:'100%', marginTop:1}}>
            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:20, color:'#484848', alignSelf:'center', marginTop:'3%'}}>Change your Privacy PIN</Text>
                <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:27, color:'#39a442', marginTop:'5%'}}>CPS FUEL CARD    &#11044; &#11044; &#11044; &#11044;1000091</Text>
            <TouchableOpacity
            onPress={() => { 

                AsyncStorage.removeItem('UserITA');
                id.setItaId('1')
                setTimeout(() => navigation.navigate('ItaLogInHome')),1000;
            }}
            >
                <View style={{width:'60%',  alignItems:'center', backgroundColor:'#39a442', borderRadius:10, marginTop:'5%'}}>
                    <Text style={{fontSize:20, color:'white', fontFamily:'Myriad-Pro-Bold-Condensed', paddingVertical:'5%', paddingHorizontal:'30%'}}>
                        SIGN OUT
                    </Text>
                </View>
            </TouchableOpacity>
            </View>
        </View>
        }
        </CompassContext.Consumer>
    )
}

const styles = StyleSheet.create({
    container: {
            width:'90%',
            marginHorizontal:'5%',
            height:'90%',
            marginTop: Constants.statusBarHeight,
            flex:1,
            alignItems:'center'
        },
    headerContainer:{
            width:'100%', 
            flexDirection:'row', 
            justifyContent:'space-between', 
            height:'10%', 
            alignItems:'center', 
            borderBottomWidth:5, 
            paddingBottom:40, 
            marginTop:40, 
            borderBottomColor:'#e5e5e6'
    },
    input: {
        width:'100%', 
        alignSelf:'center', 
        borderBottomWidth:1, 
        marginTop:'6%', 
        fontFamily:'Myriad-Pro-Condensed', 
        fontSize:20, 
        paddingBottom:4,
        
    }
});

export default ItaAccountScreen