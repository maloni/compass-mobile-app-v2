import React from 'react';
import { Text, View, StyleSheet, ScrollView, Image, TouchableOpacity } from 'react-native';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import ItaCarrierButton from '../../components/ItaCarrierButton';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


const ItaCarrierServiceHome = ({ navigation }) => {
    return(
        <View style={styles.container}>
            <View style={styles.img}>
                <Image 
                resizeMode='contain'
                resizeMethod='resize'
                source={require('../../img/ITALogo.png')}
                style={{width:'100%', position:'relative', height:'90%'}}
                />
                <Text style={{fontSize:14, fontWeight:'bold', color:'#20316b', alignSelf:'center'}}
                >Where Success Begins</Text>
            </View>
            <View style={{width:'100%', marginTop: 10, alignItems:'center', height:'70%'}}>
            <ItaCarrierButton 
            Title='CARRIER SERVICES'
            navigation={() => navigation.navigate('ItaCarrier')}
            />
            <ItaCarrierButton 
            Title='MEMBERSHIP BENEFITS'
            navigation={() => navigation.navigate('ItaMembership')}
            />
            <ItaCarrierButton 
            Title='SAFETY VIDEOS'
            navigation={() => navigation.navigate('ItaVideos')}
            />
            <ItaCarrierButton 
            Title='ITA MAGAZINE'
            navigation={() => navigation.navigate('ItaMagazine')}
            />
            <ItaCarrierButton 
            Title='ITA EVENTS'
            navigation={() => navigation.navigate('ItaEvenets')}
            />
            <ItaCarrierButton 
            Title='ITA NOTIFICATIONS'
            navigation={() => navigation.navigate('ItaNotifications')}
            />
            <TouchableOpacity style={{width:'80%', marginTop:10}}>
                <View style={{backgroundColor:'#267937', borderRadius:10, alignItems:'center'}}>
                    <Text style={{fontSize:25, paddingVertical:10, color:'white', fontFamily:'Myriad-Pro-Bold-Condensed', textAlign:'center'}}>CONTACT US</Text>
                </View>
            </TouchableOpacity>           
            </View>
        </View>             
    )
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        height:'90%',
        flex:1,
        alignItems:'center',
        marginTop: Constants.statusBarHeight,
    },
    img: {
        width:wp('40%'),
        justifyContent:'space-around',
        height:hp('25%'),
        paddingBottom:5

    },
    btnContainer:{
        width:'80%',
        height:'30%',
        marginTop:20
    },
    btn:{
        borderWidth:2, 
        borderRadius:10, 
        paddingVertical:10, 
        alignItems:'center',
        marginTop:30,
        borderColor:'#20316b'
    },
    btnTxt:{
        fontSize:25,
        color:'#20316b',
        fontFamily:'Myriad-Pro-Bold-Condensed'
    }
});

export default ItaCarrierServiceHome;