import React, { useState, useEffect, useContext } from 'react';
import { Text, View, StyleSheet, Image, FlatList, Dimensions } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import { ScrollView } from 'react-native-gesture-handler';
import { TouchableOpacity } from 'react-native';
import  Ionicons from '@expo/vector-icons/Ionicons';
import AntDesign from '@expo/vector-icons/AntDesign'
import CompassContext from '../../context/BlogContext';
import Axios from 'axios';
import Modal from "react-native-modal";
import { WebView } from 'react-native-webview';

const ItaVideos = () => {
    const[active1,setActive1] = useState(true);
    const[active2,setActive2] = useState(false);
    const[active3,setActive3] = useState(false);
    const[style, setStyle] = useState({borderwidth1:5, bordercolor1:'green', borderwidth2:3, bordercolor2:'#cdcdcd', borderwidth3:3, bordercolor3:'#cdcdcd'});
    const[assignedVideos , setAssignedVideos] = useState([])
    const [videoUrl, setVideoUrl] = useState()

    const ita = useContext(CompassContext);

    console.log("ite id" + ita.itaId)

    const assignedRequest = () => {
        Axios.get('https://apps.compassaws.net/inc/getVideosAssigned.php?userId=20')
            .then(res => {
                setAssignedVideos(res.data)
            })
            .then(err => {
                console.log("err" + err)
            })
    }

    return(
        <KeyboardAwareView style={{flex:1}}>
                <View style={styles.container}>
                <View style={styles.img}>
                    <Image 
                    resizeMode='contain'
                    resizeMethod='resize'
                    source={require('../../img/ITALogo.png')}
                    style={{width:'100%', position:'relative', height:'90%'}}
                    />
                    <Text style={{fontSize:14, fontWeight:'bold', color:'#20316b', alignSelf:'center'}}
                    >Where Success Begins</Text>
                </View>
                <View style={styles.top}>
                    <TouchableOpacity 
                    onPress={() => {
                        setActive1(true), 
                        setActive2(false),
                        setActive3(false),
                        setStyle({borderwidth1:5, bordercolor1:'green', borderwidth2:3, bordercolor2:'#cdcdcd', borderwidth3:3, bordercolor3:'#cdcdcd'})}}
                    style={{...styles.opacity, borderBottomWidth:style.borderwidth1, borderBottomColor:style.bordercolor1}}>
                        <Text style={styles.txtHead}>
                            ALL
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                    onPress={() => {
                        assignedRequest(),
                        setActive1(false), 
                        setActive2(true),
                        setActive3(false), 
                        setStyle({borderwidth1:3, bordercolor1:'#cdcdcd', borderwidth2:5, bordercolor2:'green', borderwidth3:3, bordercolor3:'#cdcdcd'})}}                    
                    style={{...styles.opacity,borderBottomWidth:style.borderwidth2, borderBottomColor:style.bordercolor2 }}>
                        <Text style={styles.txtHead}>
                            ASSIGNED
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                    onPress={() => {
                        setActive1(false), 
                        setActive2(false),
                        setActive3(true), 
                        setStyle({borderwidth1:3, bordercolor1:'#cdcdcd', borderwidth2:3, bordercolor2:'#cdcdcd', borderwidth3:5, bordercolor3:'green'})}}                    
                    style={{...styles.opacity, borderBottomWidth:style.borderwidth3, borderBottomColor:style.bordercolor3}}>
                        <Text style={styles.txtHead}>
                            COMPLETED
                        </Text>
                    </TouchableOpacity>
                </View>
                    {active1 ? <AllVideos /> : null}
                    {active2 ? <AssignedVideos ita={ita} assignedVideos={assignedVideos}/> : null}
                    {active3 ? <CompletedVideos /> : null}
                
                </View>
        </KeyboardAwareView>
    )
}

const AllVideos = () => {
    
    const [activeDriverCompliance,setAactiveDriverCompliance] = useState(false)
    const [activeSafeDriving, setActiveSafeDriving] = useState(false)
    const [activeVehicleInspections, setActiveVehicleInspections] = useState(false)
    const [activeCargoSecurement, setActiveCargoSecurement] = useState(false)
    const [activeDriverHealthAndWellness, setActiveDriverHealthAndWellness] = useState(false)
    const [showMore, setShowMore] = useState(null)
    const [allVideos , setAllVideos] = useState()
    const [showVideo, setShowVideo] = useState(false)
    const [videoUrl, setVideoUrl] = useState()

    useEffect(() => {
        request()
        },[])

    const request = () => {
        Axios('https://apps.compassaws.net/inc/getAllVideos.php',)
            .then(res => {
                setAllVideos(res.data)
            })
            .then(err => {
                console.log("err" + err)
            })
    }

    const toggleMore = (id) => {
        if (showMore === id) {
            setShowMore(null);
        } else {
            setShowMore(id);
        }
      }
      

    //   console.log(allVideos)

    return(
        <ScrollView 
        automaticallyAdjustContentInsets={true}
        style={{width:'100%'}}>
            
                <TouchableOpacity
                onPress={() => !activeDriverCompliance ? setAactiveDriverCompliance(true) : setAactiveDriverCompliance(false)
                }
                >
                <View style={{borderColor: '#cdcdcd' ,swidth:'100%', borderWidth:1, flexDirection:'row', justifyContent:'space-between', padding:wp('2%'), marginBottom:hp('1%')}}>
                    <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:26, padding:'2%', color:'#20316b'}}>Driver Compliance</Text>
                    <Ionicons 
                        name='ios-arrow-down'
                        size={wp('10%')}
                        color='#20316b'
                        />
                    </View>
                </TouchableOpacity>
                {
                    activeDriverCompliance
                    ?<FlatList
                    data={allVideos}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item) => item.id}
                    renderItem={({ item }) => {
                        
                        return(
                        item.videoCategory === "Driver Compliance"
                        ?<TouchableOpacity
                        onPress={() => {setShowVideo(true),
                        setVideoUrl('https://apps.compassaws.net/video/watch.html#?videoId='+item.id+ '&userId=20')}}>
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                            <AntDesign 
                            size={wp('8%')}
                            color='#20316b'
                            name='playcircleo'
                            style={{padding:wp('3%')}}
                            />
                            <Text style={{paddingVertical:'5%', fontFamily:'Myriad-Pro-Bold-Condensed', color:'#20316b', fontSize:22}}>
                                {item.videoName}
                                </Text>
                        </View>
                        </TouchableOpacity>
                        :null
                        )
                    }}
                    /> 
                    
                    
                    : null
                }
                <TouchableOpacity
                onPress={() => !activeSafeDriving ? setActiveSafeDriving(true) : setActiveSafeDriving(false)
                }
                >
                <View style={{borderColor: '#cdcdcd' ,width:'100%', borderWidth:1, flexDirection:'row', justifyContent:'space-between', padding:wp('2%'), marginBottom:hp('1%')}}>
                    <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:26, padding:'2%', color:'#20316b'}}>Safe Driving</Text>
                    <Ionicons 
                        name='ios-arrow-down'
                        size={wp('10%')}
                        color='#20316b'
                        />
                    </View>
                </TouchableOpacity>
                {
                    activeSafeDriving
                    ?<FlatList
                    data={allVideos}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item) => item.id}
                    renderItem={({ item }) => {
                        
                        return(
                        item.videoCategory === "Safe Driving"
                        ?<TouchableOpacity
                        onPress={() => setShowVideo(true)}
                        >
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                            <AntDesign 
                            size={wp('8%')}
                            color='#20316b'
                            name='playcircleo'
                            style={{padding:wp('3%')}}
                            />
                            <Text style={{paddingVertical:'5%', fontFamily:'Myriad-Pro-Bold-Condensed', color:'#20316b', fontSize:22}}>
                                {item.videoName}
                                </Text>
                        </View>
                        </TouchableOpacity>
                        :null
                        )
                    }}
                    /> 
                    
                    
                    : null
                }
                <TouchableOpacity
                    onPress={() => !activeVehicleInspections ? setActiveVehicleInspections(true) : setActiveVehicleInspections(false)
                    }
                    >
                    <View style={{borderColor: '#cdcdcd' ,swidth:'100%', borderWidth:1, flexDirection:'row', justifyContent:'space-between', padding:wp('2%'), marginBottom:hp('1%')}}>
                        <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:26, padding:'2%', color:'#20316b'}}>Vehicle Inspections</Text>
                        <Ionicons 
                            name='ios-arrow-down'
                            size={wp('10%')}
                            color='#20316b'
                            />
                    </View>
                </TouchableOpacity>
                {
                    activeVehicleInspections
                    ?<FlatList
                    data={allVideos}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item) => item.id}
                    renderItem={({ item }) => {
                        
                        return(
                        item.videoCategory === "Vehicle Inspections"
                        ?<TouchableOpacity
                        onPress={() => setShowVideo(true)}
                        >
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                            <AntDesign 
                            size={wp('8%')}
                            color='#20316b'
                            name='playcircleo'
                            style={{padding:wp('3%')}}
                            />
                            <Text style={{paddingVertical:'5%', fontFamily:'Myriad-Pro-Bold-Condensed', color:'#20316b', fontSize:22}}>
                                {item.videoName}
                                </Text>
                        </View>
                        </TouchableOpacity>
                        :null
                        )
                    }}
                    /> 
                    
                    
                    : null
                }
                <TouchableOpacity
                    onPress={() => !activeCargoSecurement ? setActiveCargoSecurement(true) : setActiveCargoSecurement(false)
                    }
                    >
                    <View style={{borderColor: '#cdcdcd' ,swidth:'100%', borderWidth:1, flexDirection:'row', justifyContent:'space-between', padding:wp('2%'), marginBottom:hp('1%')}}>
                        <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:26, padding:'2%', color:'#20316b'}}>Cargo Securement</Text>
                        <Ionicons 
                            name='ios-arrow-down'
                            size={wp('10%')}
                            color='#20316b'
                            />
                    </View>
                </TouchableOpacity>
                {
                    activeCargoSecurement
                    ?<FlatList
                    data={allVideos}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item) => item.id}
                    renderItem={({ item }) => {
                        
                        return(
                        item.videoCategory == "Cargo Securement"
                        ?<TouchableOpacity
                        onPress={() => setShowVideo(true)}
                        >
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                            <AntDesign 
                            size={wp('8%')}
                            color='#20316b'
                            name='playcircleo'
                            style={{padding:wp('3%')}}
                            />
                            <Text style={{paddingVertical:'5%', fontFamily:'Myriad-Pro-Bold-Condensed', color:'#20316b', fontSize:22}}>
                                {item.videoName}
                                </Text>
                        </View>
                        </TouchableOpacity>
                        :null
                        )
                    }}
                    /> 
                    
                    
                    : null
                }
                <TouchableOpacity
                    onPress={() => !activeDriverHealthAndWellness ? setActiveDriverHealthAndWellness(true) : setActiveDriverHealthAndWellness(false)
                    }
                    >
                    <View style={{borderColor: '#cdcdcd' ,swidth:'100%', borderWidth:1, flexDirection:'row', justifyContent:'space-between', padding:wp('2%'), marginBottom:hp('1%')}}>
                        <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:26, padding:'2%', color:'#20316b'}}>Driver Health and Wellness</Text>
                        <Ionicons 
                            name='ios-arrow-down'
                            size={wp('10%')}
                            color='#20316b'
                            />
                    </View>
                </TouchableOpacity>
                {
                    activeDriverHealthAndWellness
                    ?<FlatList
                    data={allVideos}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item) => item.id}
                    renderItem={({ item }) => {
                        
                        return(
                        item.videoCategory === "Driver Health and Wellness"
                        ?<TouchableOpacity
                        onPress={() => setShowVideo(true)}
                        >
                        <View style={{flexDirection:'row',alignItems:'center'}}>
                            <AntDesign 
                            size={wp('8%')}
                            color='#20316b'
                            name='playcircleo'
                            style={{padding:wp('3%')}}
                            />
                            <Text style={{paddingVertical:'5%', fontFamily:'Myriad-Pro-Bold-Condensed', color:'#20316b', fontSize:22}}>
                                {item.videoName}
                                </Text>
                        </View>
                        </TouchableOpacity>
                        :null
                        )
                    }}
                    /> 
                    : null
                }

                <Modal
                    animationType='slideInUp'
                    isVisible={showVideo}
                    onRequestClose={() => setShowVideo(false)}
                    onBackdropPress={() => setShowVideo(false)}
                    // backdropOpacity={0.9}
                    hasBackdrop={true}
                    backdropColor='white'
                    >
                        <WebView source={{ uri: videoUrl}} style={{ width:wp('80%'),alignSelf:'center' }} />
                </Modal>
        </ScrollView>
    )
} 

const AssignedVideos = ({ ita, assignedVideos }) => {

    const [active,setActive] = useState(true)
    const [showMore, setShowMore] = useState(null)
    const [showVideo, setShowVideo] = useState(false)
    const [videoUrl, setVideoUrl] = useState()

    // useEffect(() => {
    //     request()
    //     },[])

    // const assignedRequest = () => {
    //     Axios.get('https://apps.compassaws.net/inc/getVideosAssigned.php?userId=20')
    //         .then(res => {
    //             setAssignedVideos(res.data)
    //         })
    //         .then(err => {
    //             console.log("err" + err)
    //         })
    // }

    console.log(assignedVideos)

    return(
        <View style={{width:'100%', height:'60%'}}>
        {assignedVideos.length > 0
        ?<TouchableOpacity
        onPress={() => !active ? setActive(true) : setActive(false)
        }
        >
        <View style={{borderColor: '#cdcdcd' ,swidth:'100%', borderWidth:1, flexDirection:'row', justifyContent:'space-between', padding:wp('2%'), marginBottom:hp('1%')}}>
            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:26, padding:'2%', color:'#20316b'}}>Driver Compliance</Text>
            <Ionicons 
                name='ios-arrow-down'
                size={wp('10%')}
                color='#20316b'
                />
            </View>
        </TouchableOpacity>
        :null }
        {
            active
            ?<FlatList
            data={assignedVideos}
            keyExtractor={(item) => item.assId}
            renderItem={({ item }) => {
                
                return(
                item.videoCategory === "Driver Compliance"
                ?<TouchableOpacity
                onPress={() => {
                    setVideoUrl('https://apps.compassaws.net/video/watchAssigned.html#?videoId="'+item.id+'"&userId="+20;'),
                    setShowVideo(true)}}
                >
                <View style={{flexDirection:'row',alignItems:'center'}}>
                    <AntDesign 
                    size={wp('8%')}
                    color='#20316b'
                    name='playcircleo'
                    style={{padding:wp('3%')}}
                    />
                    <Text style={{paddingVertical:'5%', fontFamily:'Myriad-Pro-Bold-Condensed', color:'#20316b', fontSize:22}}>
                        {item.videoName}
                        </Text>
                </View>
                </TouchableOpacity>
                :null
                )
            }}
            /> 
            : null
        }
        <Modal
            animationType='slideInUp'
            isVisible={showVideo}
            onRequestClose={() => setShowVideo(false)}
            onBackdropPress={() => setShowVideo(false)}
            // backdropOpacity={0.9}
            hasBackdrop={true}
            backdropColor='black'
            >
                <WebView source={{ uri: videoUrl }} style={{ width:wp('100%'),height:hp('100%') ,alignSelf:'center' }} />
        </Modal>
</View>
    )
}

const CompletedVideos = () => {
    return(
        <AllVideos />
    )
}

const styles = StyleSheet.create({
    container: {
        width:wp('90%'),
        alignSelf:'center',
        height:hp('85%'),
        flex:1,
        alignItems:'center',
        marginTop: Constants.statusBarHeight,
    },
    img: {
        width:wp('40%'),
        justifyContent:'space-around',
        height:hp('25%'),
        paddingBottom:5
    },
    top:{
        alignItems:'center',
        flexDirection:'row',
        justifyContent:'space-around',
        marginTop:'5%'
    },
    opacity:{
        alignItems:'center', 
        justifyContent:'center', 
        flexDirection:'row', 
        width:'33%',
        borderBottomWidth:3,
        borderBottomColor:'#cdcdcd',
        paddingBottom:5,
    },
    txtHead:{
        alignSelf:'center', 
        fontFamily:'Myriad-Pro-Bold-Condensed', 
        fontSize:wp('5%'), 
        color:'#20316b',
        
    }
});

export default ItaVideos;