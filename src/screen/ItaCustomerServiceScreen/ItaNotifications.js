import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import Axios from 'axios';
import numeral from 'numeral';
import moment from 'moment';
import { WaveIndicator } from 'react-native-indicators';


const ItaNotifications = () => {

    const [allNotification, setAllNotification] = useState([]);
    const[loading, setLoading] = useState(false)

    useEffect(() => {
         Axios.get('https://apps.compassaws.net/api/v1/notificationHistory/71')
        .then(res => {
            console.log(res.data.tasks)
            setAllNotification(res.data.tasks)
            setLoading(true)
        })
        .then(err => {
            console.log(err)
        })
    }, [])


    return(
    <KeyboardAwareView style={{flex:1, alignItems:'center', marginTop: Constants.statusBarHeight}}>
                <View style={styles.img}>
                    <Image 
                    resizeMode='contain'
                    resizeMethod='resize'
                    source={require('../../img/ITALogo.png')}
                    style={{width:'100%', position:'relative', height:'90%'}}
                    />
                    <Text style={{fontSize:14, fontWeight:'bold', color:'#20316b', alignSelf:'center'}}
                    >Where Success Begins</Text>
                </View>
                {
                    loading
                    ?<FlatList 
                        data={allNotification}
                        showsVerticalScrollIndicator={false}
                        keyExtractor={(item) => item.id}
                        renderItem={({item}) => {

                    let newDate = new Date(item.date)

                    return(
                        <View style={{width:'90%', marginTop:'3%', borderTopWidth:4, paddingVertical:'2%', paddingHorizontal:'2%', borderColor:'#267937'}}>
                            <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
                                <Text style={{fontSize:wp('6%'),fontFamily:'Myriad-Pro-Bold-Condensed', color:'#267937'}}>Push Notification</Text>
                                <Text style={{fontSize:wp('3%'),fontFamily:'Myriad-Pro-Condensed'}}>{moment(item.date).format("MMM Do YYYY")}</Text>
                            </View>
                            <Text style={{fontFamily:'Myriad-Pro-Condensed', fontSize:wp('4%')}}>{item.text}</Text>
                        </View>
                    )
                }}
                />
                    :<WaveIndicator size={50} color="#267937"  style={{justifyContent:'center', alignSelf:'center'}}/>
                }
        </KeyboardAwareView>
    )
}

const styles = StyleSheet.create({
    container: {
        width:wp('90%'),
        alignSelf:'center',
        alignItems:'center',
        marginTop: Constants.statusBarHeight,
    },
    img: {
        width:wp('40%'),
        justifyContent:'space-around',
        height:hp('25%'),
        paddingBottom:5,
        alignSelf:'center'
    },
});

export default ItaNotifications;