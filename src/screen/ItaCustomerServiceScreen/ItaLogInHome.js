import React from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import Constants from 'expo-constants';
import { TouchableOpacity } from 'react-native-gesture-handler';

const ItaLogInHome = ({ navigation }) => {
    return(
        <View style={styles.container}>
            <View style={styles.img}>
                <Image 
                resizeMode='contain'
                resizeMethod='resize'
                source={require('../../img/ITALogo.png')}
                style={{width:'100%'}}
                />
                <Text style={{fontSize:20, fontWeight:'bold', color:'#20316b', alignSelf:'center'}}
                >Where Success Begins</Text>
            </View>
            
            <View style={styles.btnContainer}>
                <TouchableOpacity
                onPress={() => navigation.navigate('ItaLogIn')}>
                    <View style={styles.btn}>
                        <Text style={styles.btnTxt}>
                            MEMEBER LOG IN
                        </Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                onPress={() => navigation.navigate('ItaRegisterScreen')}>
                    <View style={styles.btn}>
                        <Text style={styles.btnTxt}>
                            BECOME A MEMEBER
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        height:'90%',
        flex:1,
        alignItems:'center',
        marginTop: Constants.statusBarHeight,
    },
    img: {
        width:'75%',
        paddingBottom:0
    },
    btnContainer:{
        width:'80%',
        height:'30%',
        marginTop:30
    },
    btn:{
        borderWidth:1, 
        borderRadius:10, 
        paddingVertical:15, 
        alignItems:'center',
        backgroundColor:'#20316b',
        marginTop:30
    },
    btnTxt:{
        fontSize:22,
        color:'white',
        fontFamily:'Myriad-Pro-Condensed'
    }
});

export default ItaLogInHome;