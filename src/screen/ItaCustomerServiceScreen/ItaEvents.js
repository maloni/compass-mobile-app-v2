import React, {useEffect, useState} from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import { ScrollView, FlatList } from 'react-native-gesture-handler';
import Axios from 'axios';
import moment from 'moment';
import CountDown from 'react-native-countdown-component';
import { WaveIndicator } from 'react-native-indicators';

const ItaEvenets = () => {

    const[events, setEvents] = useState()
    const[loading, setLoading] = useState(false)

    useEffect(() => {
        Axios.get('https://apps.compassaws.net/inc/getEvents.php')
       .then(res => {
        //    console.log(res.data)
           setEvents(res.data)
           setLoading(true)
       })
       .then(err => {
           console.log(err)
       })
   }, [])

    return(
        <KeyboardAwareView style={{flex:1, alignItems:'center', marginTop: Constants.statusBarHeight}}>
                <View style={styles.img}>
                    <Image 
                    resizeMode='contain'
                    resizeMethod='resize'
                    source={require('../../img/ITALogo.png')}
                    style={{width:'100%', position:'relative', height:'90%'}}
                    />
                    <Text style={{fontSize:14, fontWeight:'bold', color:'#20316b', alignSelf:'center'}}
                    >Where Success Begins</Text>
                </View>
                {
                    loading
                    ?<FlatList 
                data={events}
                keyExtractor={(item) => item.id}
                renderItem={({ item }) => {

                    const dateInSec = moment(item.eventDate).format()
                    // let elapsed = dateInSec.getTime()
                    const dateDate = new Date(dateInSec)
                    const dateNow = new Date()

                    const secondsLeft = Math.floor((dateDate - dateNow) / 1000)

                    return(

                        <View style={{width:wp('90%'), marginTop:'3%', borderWidth:1, borderRadius:10, paddingVertical:'2%', paddingHorizontal:'2%', borderColor:'#267937'}}>
                            <Text  style={{fontSize:wp('6%'),fontFamily:'Myriad-Pro-Bold-Condensed', color:'#267937', alignSelf:'center'}}>{item.eventName}</Text>
                            <Text style={{fontSize:wp('4%'),fontFamily:'Myriad-Pro-Condensed'}}>{item.eventText}</Text>
                            <Text style={{fontSize:wp('4%'),fontFamily:'Myriad-Pro-Condensed'}}>{item.eventLoc}</Text>
                            {/* <Text>{dateInSec}</Text> */}
                            <Text  style={{fontSize:wp('4%'),fontFamily:'Myriad-Pro-Bold-Condensed', color:'#267937', alignSelf:'center'}}>Event End in</Text>
                            <CountDown
                                digitStyle={{backgroundColor:'#267937'}}
                                until={secondsLeft}
                                onFinish={() => alert('finished')}
                                onPress={() => alert('hello')}
                                size={25}
                                digitTxtStyle={{color:'white'}}
                                timeLabelStyle={{color:'black', fontFamily:'Myriad-Pro-Condensed', fontSize:wp('4%')}}

                            />
                        </View>
                    )
                }}
                />
                    :<WaveIndicator size={50} color="#267937"  style={{justifyContent:'center', alignSelf:'center'}}/>
                }
                
        </KeyboardAwareView>
    )
}

const styles = StyleSheet.create({
    container: {
        width:wp('90%'),
        alignSelf:'center',
        height:hp('85%'),
        flex:1,
        alignItems:'center',
        marginTop: Constants.statusBarHeight,
    },
    img: {
        width:wp('40%'),
        justifyContent:'space-around',
        height:hp('25%'),
        paddingBottom:5,
        alignSelf:'center'
    },
});

export default ItaEvenets;