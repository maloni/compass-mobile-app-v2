import React, { useState, useContext, useEffect } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import  Ionicons  from '@expo/vector-icons/Ionicons';
import CompassContext from '../../context/BlogContext';

const ItaMembership = ({ navigation }) => {

    const [visible1, setVisible1] = useState(false);
    const [visible2, setVisible2] = useState(false);
    const [visible3, setVisible3] = useState(false);
    const [visible4, setVisible4] = useState(false);
    const [visible5, setVisible5] = useState(false);


    const it = useContext(CompassContext)

    

    console.log(it.itaId)

    return(
        <KeyboardAwareView style={{flex:1}}>
            <ScrollView style={{flex:1}}>     
                <View style={styles.container}>
                <View style={styles.img}>
                    <Image 
                    resizeMode='contain'
                    resizeMethod='resize'
                    source={require('../../img/ITALogo.png')}
                    style={{width:'100%', position:'relative', height:'90%'}}
                    />
                    <Text style={{fontSize:14, fontWeight:'bold', color:'#20316b', alignSelf:'center'}}
                    >Where Success Begins</Text>
                </View>
                <View style={{marginTop:hp('2%')}}>
                    <View style={{backgroundColor:'transparent',elevation:5}}>
                        <TouchableOpacity onPress={() => {visible1
                            ? setVisible1(false) 
                            : setVisible1(true) & setVisible2(false) & setVisible3(false) & setVisible4(false) & setVisible5(false)
                             }}
                             style={{backgroundColor:'transparent'}}>
                            <View style={styles.btn}>
                                <Text style={styles.btnTxt}>TIRE AND MAINTENANCE DISCOUNTS </Text>
                                <Ionicons 
                                name='ios-arrow-down'
                                size={wp('10%')}
                                color='#20316b'
                                />
                            </View>
                        </TouchableOpacity>
                        {
                            visible1 
                            ?<View style={{backgroundColor: 'transparent', padding:5}}>{ 
                            it.itaId ==='1' 
                            ?<View>
                                <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#20316b', fontSize:20}}>Content available only for ITA Members</Text>
                                <TouchableOpacity
                                onPress={() => navigation.navigate('ItaLogInHome')}
                                >
                                    <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#20316b', fontSize:20}}>Log In or Register</Text>
                                </TouchableOpacity>
                                </View>
                            :<Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#20316b', fontSize:20}}>TIRE AND MAINTENANCE DISCOUNTS</Text>}</View>
                            :null
                        }
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => { visible2
                            ? setVisible2(false)
                            : setVisible1(false) & setVisible2(true) & setVisible3(false) & setVisible4(false) & setVisible5(false)
                            }}>
                            <View style={styles.btn}>
                                <Text style={styles.btnTxt}>FUEL SAVINGS </Text>
                                <Ionicons 
                                name='ios-arrow-down'
                                size={wp('10%')}
                                color='#20316b'
                                />
                            </View>
                        </TouchableOpacity>
                        {
                            visible2 
                            ?<View style={{backgroundColor: 'transparent', padding:5}}>{ 
                                it.itaId ==='1' 
                                ?<View>
                                    <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#20316b', fontSize:20}}>Content available only for ITA Members</Text>
                                    <TouchableOpacity
                                    onPress={() => navigation.navigate('ItaLogInHome')}
                                    >
                                        <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#20316b', fontSize:20}}>Log In or Register</Text>
                                    </TouchableOpacity>
                                    </View>
                                :<View style={{flexDirection:'row'}}>
                                    <Image source={require('../../img/glyingj.jpg')} style={{width:'30%', height:'100%', resizeMode:'contain'}}/>

                                    <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'black', fontSize:20}}>12 cents off cash price</Text>
                                </View>
                                }</View>
                                :null
                        }
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => { visible3
                            ? setVisible3(false)
                            : setVisible1(false) & setVisible2(false) & setVisible3(true) & setVisible4(false) & setVisible5(false)
                            }}>
                            <View style={styles.btn}>
                                <Text style={styles.btnTxt}>EQUIPMENT PURCHASES </Text>
                                <Ionicons 
                                name='ios-arrow-down'
                                size={wp('10%')}
                                color='#20316b'
                                />
                            </View>
                        </TouchableOpacity>
                        {
                            visible3 
                            ?<View style={{backgroundColor: 'transparent', padding:5}}>{ 
                                it.itaId ==='1' 
                                ?<View>
                                    <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#20316b', fontSize:20}}>Content available only for ITA Members</Text>
                                    <TouchableOpacity
                                    onPress={() => navigation.navigate('ItaLogInHome')}
                                    >
                                        <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#20316b', fontSize:20}}>Log In or Register</Text>
                                    </TouchableOpacity>
                                    </View>
                                :<Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#20316b', fontSize:20}}>EQUIPMENT PURCHASES</Text>}</View>
                                :null
                        }
                    </View>
                    <View>
                        <TouchableOpacity onPress={() => { visible4
                            ? setVisible4(false)
                            : setVisible1(false) & setVisible2(false) & setVisible3(false) & setVisible4(true) & setVisible5(false)}}>
                            <View style={styles.btn}>
                                <Text style={styles.btnTxt}>SAFETY & COMPLIANCE </Text>
                                <Ionicons 
                                name='ios-arrow-down'
                                size={wp('10%')}
                                color='#20316b'
                                />
                            </View>
                        </TouchableOpacity>
                        {
                            visible4 
                            ?<View style={{backgroundColor: 'transparent', padding:5}}>{ 
                                it.itaId ==='1' 
                                ?<View>
                                    <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#20316b', fontSize:20}}>Content available only for ITA Members</Text>
                                    <TouchableOpacity
                                    onPress={() => navigation.navigate('ItaLogInHome')}
                                    >
                                        <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#20316b', fontSize:20}}>Log In or Register</Text>
                                    </TouchableOpacity>
                                    </View>
                                :<Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#20316b', fontSize:20}}>SAFETY & COMPLIANCE</Text>}</View>
                                :null
                        }
                    </View>
                    <View style={{backgroundColor:'transparent'}}>
                        <TouchableOpacity 
                        onPress={() => {visible5
                            ? setVisible5(false)
                            : setVisible1(false) & setVisible2(false) & setVisible3(false) & setVisible4(false) & setVisible5(true)}}>
                            <View style={styles.btn}>
                                <Text style={styles.btnTxt}>SOFTWARE TECHNOLOGY </Text>
                                <Ionicons 
                                name='ios-arrow-down'
                                size={wp('10%')}
                                color='#20316b'
                                />
                            </View>
                        </TouchableOpacity>
                        {
                            visible5 
                            ?<View style={{backgroundColor: 'transparent', padding:5}}>{ 
                                it.itaId ==='1' 
                                ?<View>
                                    <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#20316b', fontSize:20}}>Content available only for ITA Members</Text>
                                    <TouchableOpacity
                                    onPress={() => navigation.navigate('ItaLogInHome')}
                                    >
                                        <Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#20316b', fontSize:20}}>Log In or Register</Text>
                                    </TouchableOpacity>
                                    </View>
                                :<Text style={{fontFamily:'Myriad-Pro-Condensed', color:'#20316b', fontSize:20}}>SOFTWARE TECHNOLOGY</Text>}</View>
                                :null
                        }
                    </View>
                </View>
                </View>
            </ScrollView>
        </KeyboardAwareView>
    )
}

const styles = StyleSheet.create({
    container: {
        width:wp('90%'),
        alignSelf:'center',
        height:hp('85%'),
        flex:1,
        alignItems:'center',
        marginTop: Constants.statusBarHeight,
    },
    img: {
        width:wp('40%'),
        justifyContent:'space-around',
        height:hp('25%'),
        paddingBottom:5,
    },
    btn:{
        width:wp('80%'),
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        padding:wp('3%'),
        marginBottom:wp('2%'),
        backgroundColor:'transparent',
        borderWidth:1,
        borderColor: '#cdcdcd'   
    },
    btnTxt:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:wp('5%'),
        color:'#20316b'
    },
});

export default ItaMembership;