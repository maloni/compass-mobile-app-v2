import React, { useState } from 'react';
import { Text, View, StyleSheet, Image, SafeAreaView, ScrollView , Switch, TextInput, Picker } from 'react-native';
import Constants from 'expo-constants';
import ITAInput from '../../components/ITAInput';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import ToggleSwitch from 'toggle-switch-react-native'
import { TouchableOpacity } from 'react-native';
import Axios from 'axios';

const ItaRegisterScreen = ({ navigation }) => {
    const [cps, setCps] = useState(false);
    const [groupType, setGroupType] = useState('')
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')


    const send = () => {
        const data = 'cps='+(cps ? '0':'')+'&userType='+groupType +'&email='+email+'&name='+name+'&phone='+phone+'&title=';
        Axios.post('https://apps.compassaws.net/api/v1/register', data)
        .then(response =>{
            console.log(response)
        })
        .catch(error => {
            console.log(error)
        })
    }


    console.log(cps);
    return(            
    <SafeAreaView style={styles.container}>
    <KeyboardAwareView behavior={"padding"} enabled  style={{flexGrow:1,height:'100%'}} animated={true}>
        <ScrollView      
            contentContainerStyle={styles.scrollView}           
            automaticallyAdjustContentInsets={true}
            showsVerticalScrollIndicator={false}
            bounces={true}
        >
        <View style={{height:'100%'}} >
            <View style={{width:'100%', height:'22%'}}>
                <View style={{width:'50%',height:'90%', alignItems:'center', alignSelf:'center', justifyContent:'center'}}>
                    <Image 
                        resizeMode='center'
                        source={require('../../img/ITALogo.png')}
                        resizeMethod='scale'
                        style={{alignSelf:'center'}}
                    />
                </View>
                <Text style={{fontSize:12, fontWeight:'bold', color:'#20316b', alignSelf:'center'}}
                >Where Success Begins</Text>
            </View>
            <View style={{alignItems:'center', marginHorizontal:'7%', marginTop:'7%' }}>
                <View style={{flexDirection:'row', justifyContent:'space-between', width:'100%', marginTop:20}}>
                    <Text style={{fontFamily:'Myriad-Pro-Condensed', fontSize:22}}>
                        Do you have a CPS card ? 
                    </Text>
                    <ToggleSwitch
                    size='large'
                    onToggle={(isOn) => {!cps ? setCps(true) : setCps(false), isOn}}
                    isOn={cps}
                    onCollor="green"
                    offColor='#484848'
                    />
                </View>
                <TextInput 
                    editable={cps}
                    placeholder='Please enter CPS Fuel Card Number'
                    style={{width:'100%', alignSelf:'center', borderBottomWidth:1, marginTop:'6%', fontFamily:'Myriad-Pro-Condensed', fontSize:22, paddingBottom:8}}
                />
                <Picker
                selectedValue={groupType}
                onValueChange={(value) => setGroupType(value)}
                style={{width:'100%' , paddingBottom:6, backgroundColor:'#39a442', marginTop:'8%'}}
                >
                    <Picker.Item color='white' label='Please select User Group Type'/>
                    <Picker.Item label='Company Driver' value='Company Driver' />
                    <Picker.Item label='Owner Operator' value='Owner Operator' />
                    <Picker.Item label='Other' value='Other' />
                </Picker>
                <View style={{width:'100%', marginTop:15}}>
                    <Text style={{fontFamily:'Myriad-Pro-Condensed', fontSize:22}}>
                        Personal Information:
                    </Text>
                    <TextInput 
                    placeholder='Name'
                    style={styles.input}
                    value={name}
                    onChangeText={(text) => setName(text)}
                    />
                    <TextInput 
                    placeholder='Email'
                    style={styles.input}
                    onChangeText={(text) => setEmail(text)}
                    value={email}
                    />
                    <TextInput 
                    placeholder='Phone'
                    style={styles.input}
                    onChangeText={(text) => setPhone(text)}
                    value={phone}
                    />
                    <TouchableOpacity
                    onPress={() => {send()
                    navigation.navigate('ItaLogIn')}
                    }
                    >
                        <View style={{width:'60%', backgroundColor:'#39a442', borderRadius:10, marginTop:'5%', alignSelf:'center'}}>
                            <Text style={{fontSize:20, color:'white', fontFamily:'Myriad-Pro-Bold-Condensed',paddingVertical:'5%', paddingHorizontal:'30%', alignSelf:'center'}}>
                                SUBMIT
                            </Text>
                        </View>
                    </TouchableOpacity>

                </View>
            </View>
        </View>             
        </ScrollView>
    </KeyboardAwareView>
    </SafeAreaView>

    )
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        height:'90%',
        alignItems:'center',
        marginTop: Constants.statusBarHeight,
        flexGrow:1
    },
    scrollView: {
        marginHorizontal: 20,
        flexGrow:1,
        width:'100%',
        alignSelf:'center'
      },
    input: {
        width:'100%', 
        alignSelf:'center', 
        borderBottomWidth:1, 
        marginTop:'6%', 
        fontFamily:'Myriad-Pro-Condensed', 
        fontSize:22, 
        paddingBottom:5
    }
});

export default ItaRegisterScreen;