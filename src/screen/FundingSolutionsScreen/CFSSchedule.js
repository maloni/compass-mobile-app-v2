import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity, SafeAreaView, ScrollView,Platform} from 'react-native';
import  CFSlogo  from '../../components/CFSlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import CFSInvoiceInput from '../../components/CFSInvoiceInput';
import * as ImagePicker from 'expo-image-picker';
import Axios from 'axios';
import DateTimePicker from '@react-native-community/datetimepicker';
import { FlatList } from 'react-native-gesture-handler';


const CFSSchedule = ({ navigation }) => {

    const [totalAmount, setTotalAmount] = useState(100000.01)

    const [selectedDate, setSelectedDate] = useState(new Date()); 
    const [showDatePicker, setShowDatePicker] = useState(false);

    const [username, setUsername] = useState(navigation.getParam('username'))
    const [cfsUserToken, setCfsUserToken] = useState(navigation.getParam('cfsUserToken'))
    const [cfsClientNo, setCfsClientNo] = useState(navigation.getParam('cfsClientNo'))

    console.log(selectedDate)

    return(
    <SafeAreaView style={{width:'100%', height:'100%'}}>
    <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
        <ScrollView      
            contentContainerStyle={styles.container}           
            automaticallyAdjustContentInsets={true}
            showsVerticalScrollIndicator={false}
            bounces={true}
        >
           <View> 
                <CFSlogo />
                <Text style={{
                        fontFamily:'Myriad-Pro-Bold-Condensed',
                        fontSize:hp('8%'),
                        color:'#484848',
                        alignSelf:'center'}}>${(totalAmount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</Text>
                    <Text style={{
                        fontFamily:'Myriad-Pro-Bold-Condensed',
                        fontSize:hp('3%'),
                        color:'#484848',
                        alignSelf:'center'}}>Purchased Month to Date</Text>

                <View
                style={{alignItems:'center', width:'100%',borderBottomWidth:4, borderBottomColor:'#cead02', alignSelf:'center', flexDirection:'row', justifyContent:'space-between'}}>
                    <TouchableOpacity
                    onPress={() => setShowDatePicker(true)}
                    >
                        <Text style={{...styles.txt, color:'#cead02', fontSize:hp('2.5%')}}>Start Date</Text>
                    </TouchableOpacity>
                        <Text
                        style={{padding:wp('3%'), fontFamily:'Myriad-Pro-Bold-Condensed',color:'#484848',fontSize:hp('2.5%'),}}
                        >PURCHASED SCHEDULES</Text>
                    <TouchableOpacity>
                        <Text style={{...styles.txt, color:'#cead02', fontSize:hp('2.5%')}}>End Date</Text>
                    </TouchableOpacity>
                </View>
                    {showDatePicker 
                        ?<DateTimePicker
                        mode="date"
                        display="spinner"
                        defaultDate={new Date()}
                        value={new Date()}
                        onChange={(event, value) => {
                          setShowDatePicker(!showDatePicker);
                          setSelectedDate(value);
                        }}
                                    />
                        :null
                    }
                
                 <CFSPurchasedSchedules 
                 cfsClientNo={cfsClientNo}
                 username={username}
                 cfsUserToken={cfsUserToken}
                 /> 
                    
            </View>
            </ScrollView>
    </KeyboardAwareView>
    </SafeAreaView>
    )
};

const CFSPurchasedSchedules = ({ cfsClientNo, username, cfsUserToken }) => {

    const [result, setResult] = useState()
    
    useEffect(() =>{
        
        const forSend = "ClientNo="+username+"&token="+cfsUserToken+"&username="+cfsClientNo;

        Axios.post('https://apps.compassaws.net/api/cfs/PurchasedSchedulesDash', forSend)
        .then(res =>{
            setResult(res.data)
        })
        .then(err =>{
            console.log(err)
        })
    }, []);

    return(
        <View>
            <FlatList 
            showsVerticalScrollIndicator={false}
            data={result}
            keyExtractor={(item) => String(item.TransKey)}
            renderItem={({ item }) =>{
                return(
                    <View style={{borderBottomColor:'#e5e5e6', borderBottomWidth:3, marginTop:wp('2%'), marginLeft:wp('2%')}}>
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <Text style={styles.txt}>Invoice Date : </Text>
                            <Text style={{...styles.txt, fontFamily:'Myriad-Pro-Condensed'}}>{item.PostDate}</Text>
                        </View>
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <Text style={styles.txt}>Batch Number : </Text>
                            <Text style={{...styles.txt, fontFamily:'Myriad-Pro-Condensed'}}>{item.BatchNo}</Text>
                        </View>
                        <View style={{flexDirection:'row', alignItems:'center'}}>
                            <Text style={styles.txt}>A/R Amount : </Text>
                            <Text style={{...styles.txt, fontFamily:'Myriad-Pro-Condensed'}}>{item.ArAmt}</Text>
                        </View>
                        <View style={{flexDirection:'row', alignItems:'center', }}>
                            <Text style={styles.txt}>Check Amount : </Text>
                            <Text style={{...styles.txt, fontFamily:'Myriad-Pro-Condensed'}}>{item.CheckAmt}</Text>
                        </View>
                    <TouchableOpacity
                    style={{ backgroundColor:'#cead02', marginBottom:wp('2%'), width:wp('40%'), alignSelf:'center', alignItems:'center', borderRadius:10, marginTop:wp('2%')}}
                    >
                        <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', padding:wp('1%'), fontSize: 22, color:'white'}}> BATCH DETAILS </Text>
                    </TouchableOpacity>
                    </View>
                )
            }}
            />
        </View>
    )
}


CFSSchedule.navigationOptions = ({ navigation }) => {
    return{
        header: null,
    }
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        height:'90%',
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent',
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    },
    btn:{
        marginVertical:hp('1%'),
        width:wp('60%'),
        alignSelf:'center',
        borderWidth:1, 
        borderColor:'#cdcdcd',
        borderRadius:4,
        backgroundColor:'white',
        shadowOffset: {
          width: 5,
          height: 5,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#cdcdcd',
        elevation:9,
        
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default CFSSchedule;