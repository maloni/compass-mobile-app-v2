import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity, SafeAreaView, ScrollView,Platform, Alert} from 'react-native';
import  CFSlogo  from '../../components/CFSlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import CFSInvoiceInput from '../../components/CFSInvoiceInput';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import { FlatList } from 'react-native-gesture-handler';
import Modal from "react-native-modal";
import Axios from 'axios';
import * as mime from 'react-native-mime-types';
import moment from 'moment';
import { BallIndicator } from 'react-native-indicators';
import {ToastAndroid} from 'react-native';


const CFSInvoices = ({ navigation }) => {
    const [description, setDescription] = useState('')
    const [amount, setAmount ] = useState('0');
    const [item, setItem] = useState("start");
    const [itemArray, setItemArray] = useState([]);
    const [addItem, setAddItem] = useState(false);
    const [showBill, setShowBill] = useState(false);

    const ClientKey = navigation.getParam('ClientKey')
    const cfsUserToken = navigation.getParam('cfsUserToken')
    const cfsClientNo = navigation.getParam('cfsClientNo')
    const username = navigation.getParam('username')    
    const [debtor, setDebtor] = useState(false);
    const [broker, setBroker] = useState([])
    const [billTo, setBillTo] = useState('');
    const [address, setAddress] = useState(false)
    const [address1, setAddress1] = useState('')
    const [address2, setAddress2] = useState('')
    const [responseCreate , setResponseCreate] = useState(false)
    const [responseAll, setResponseAll] = useState()
    const [uploadFileLength, setUploadFileLength] = useState()
    const [mime, setMime] = useState('')
    const [invoice, setInvoice] = useState('');
    const [invoiceDate, setInvoiceDate] = useState('');
    const [po, setPo] = useState('');
    const [invoiceInput , setInvoiceInput] = useState({Invoice: invoice, InvoiceDate: invoiceDate, BillTo: billTo, PO:po})

    const [companyName, setCompanyName] = useState('');
    const [mc, setMc] = useState('');
    const [uploading, setUploading] = useState(false)
    
    const [imgArray, setImgArray] = useState([])
    const [img, setImage] = useState()
    const [sum , setSum] = useState(0)
    const [sumErr, setSumErr] = useState(true)
    const [invoiceData, setInvoiceData] = useState([])
    const [no, setNo] = useState()
    const[countSum, setCountSum] = useState('')

    useEffect(() => {
        var sendFirst = "ClientNo="+cfsClientNo+"&username="+username;
        Axios.post('https://apps.compassaws.net/api/cfs/getDefaultInvoiceNumber', sendFirst)
        .then(res =>{
            console.log('prvo',res.data.InfoResponse)
            setInvoice(res.data.InfoResponse)
        })
        .catch(err => {
            console.log(err)
        })
    }, [])
    debugger
                console.log('no',no)

        // console.log(imgArray)
    const  createInvoice = () => {
        setUploading(true)
        let sendFirst= {
            "Inv_id":invoice,
            "Inv_Date":invoiceDate, 
            "Po_No":po, 
            "InvAmt": countSum, 
            "DebName":billTo, 
            "Addr1":address1, 
            "Addr2":address2, 
            "ClientKey":ClientKey, 
            "ShipmentNum": "", 
            "UserName":username,
            "DebtorKey": "0", 
            "Name": ""
        }

        let sendSec = 
        "JsonHeader="+JSON.stringify(sendFirst)+
        "&JsonBody="+JSON.stringify(itemArray)+
        "&ClientNo="+cfsClientNo+
        "&username="+username+
        "&ClientKey="+ClientKey+
        "&token="+cfsUserToken;

        // console.log(sendSec)
        
        Axios.post('https://apps.compassaws.net/api/cfs/CreateInvoice', sendSec)
        .then(res => {
            console.log(res.data)
            let ExtraInfoResponse = res.data.ExtraInfoResponse;
            let InfoResponse = res.data.InfoResponse;

            if(res.data.StatusResponse == 1){
                if(imgArray.length > 0){
                    for(let i=0; i<imgArray.length; i++){
                        let uploadFileList =imgArray.length
                        let uploadFileLength= imgArray.length-1;
                        console.log('fgf', imgArray[i].filename)

                    let soap ='<?xml version="1.0" encoding="utf-8"?>'+
                    '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'+
                      '<soap:Body>'+
                        '<CompassUploadforInvoice xmlns="http://tempuri.org/">'+
                          '<user>'+username+'</user>'+
                          '<token>38uGsj*wa</token>'+
                          '<FileName>'+imgArray[i].filename+'</FileName>'+
                          '<base64String>'+imgArray[i].base64+'</base64String>'+
                          '<extra_info>'+imgArray[i].filetype+'</extra_info>'+
                          '<sequence_number>'+uploadFileList+'</sequence_number>'+
                          '<total_files>'+imgArray.length+'</total_files>'+
                          '<InvoiceInputId>'+ExtraInfoResponse+'</InvoiceInputId>'+
                        '</CompassUploadforInvoice>'+
                      '</soap:Body>'+
                    '</soap:Envelope>';

                    Axios.post('http://clients.compassfs.info:84/WebService/CompassWebService.asmx?op=CompassUploadMobile', soap, {headers: {'Content-Type': 'text/xml; charset=utf-8'}})
                    .then(res => {
                        console.log(res)
                        var sendFirst = "ClientNo="+cfsClientNo+"&username="+username;
                        Axios.post('https://apps.compassaws.net/api/cfs/getDefaultInvoiceNumber', sendFirst)
                        .then(res =>{
                            console.log(res.data.InfoResponse)
                            setNo(res.data.InfoResponse)
                            setUploading(false)
                            setBillTo('')
                            setInvoiceDate('')
                            setImgArray([])
                            setInvoiceData([])
                            setPo('')
                            setAddress(false)
                            setItemArray([])
                            ToastAndroid.show('Invoice created successfully', ToastAndroid.LONG)
                        })
                        .catch(err => {
                            console.log(err)
                        })
                    })
                    .catch(err => {
                        console.log('greska',err)
                    })
                    }
                }
            }
            else{
                setUploading(false)
                ToastAndroid.show(res.data.InfoResponse, ToastAndroid.LONG)
            }
        })
        .catch(err => {
            console.log(err)
        })
    }

    const sendDebtor = () =>{
        let forSend = {
            username:username,
            token:cfsUserToken,
            ClientNo:cfsClientNo,
            Name:billTo
          }

          console.log(forSend);
            Axios.get('http://clients.compassfs.info/api/Debtorss_feed?source=Debtors&data='+JSON.stringify(forSend))
                .then(response => {
                    setBroker(response.data)
                    // console.log(response.data)
                })
                .then(error => {
                    console.log(error);
                })
            }


    //console.log(JSON.stringify(imgArray))

            // console.log(imgArray)

    const selectImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            quality: 1,
            base64: true,
            type: true
          });
        //  console.log(result)
          //setMime(mime.lookup(result.uri))
          if (!result.cancelled) {
            // let type = mime.lookup(result.uri)
            // console.log(type)

            let uri = result.uri;
            let fileExtension = uri.substr(uri.lastIndexOf('.') + 1);

            let fileNameType = uri.substr(uri.lastIndexOf('ImagePicker/') + 13);
            let fileName1 = fileNameType.substring(0, fileNameType.length -4);
            let mime = `${result.type}/${fileExtension === 'jpg' ? 'jpeg' : fileExtension}`
            let size = (result.base64.length/4)*3
            // let mime ='image/jpeg'
            console.log(mime)
    
            setImgArray([...imgArray, {
                filesize: size, /* bytes */
                filetype: mime,
                filename: fileNameType,
                base64: result.base64
              }])
      };
    }

        // const countSum = () =>{
        //     for(let i = itemArray.length; i>0; i++){
        //         // let suma = amount + itemArray[i].Amount
        //         console.log(itemArray[i].Amount)
        //         }
        // }

        // console.log(countSum)
    return(
        <SafeAreaView style={styles.container}>
        <KeyboardAwareView behavior={"padding"} enabled  style={{flexGrow:1,height:'100%'}} animated={true}>
            <ScrollView      
                contentContainerStyle={styles.scrollView}           
                automaticallyAdjustContentInsets={true}
                showsVerticalScrollIndicator={false}
                bounces={true}
            >
           <View style={{height:'100%'}}> 
                <CFSlogo />
                <CFSInvoiceInput 
                value={invoice}
                setValue={(text) => setInvoice(text)}
                title='Invoice #:'
                />
                <CFSInvoiceInput 
                value={invoiceDate}
                setValue={(text) => setInvoiceDate(text)}
                title='Invoice Date:'
                placeholder='MM/DD/YYYY'
                />
                <CFSInvoiceInput 
                value={billTo}
                setValue={(text) => setBillTo(text)}
                title='Bill to:'
                onFocus={() => setShowBill(true)}
                />
                { 
                address
                ? <View style={{borderRadius:10, padding:hp('1%'), backgroundColor:'#eaeaea', marginTop:hp('2%')}}>
                    <View style={{flexDirection:'row'}}>
                        <Text style={styles.txt}>Address :</Text>
                        <Text style={styles.txt}> {address1}</Text>
                    </View>
                    { address2 !== ''
                    ?<View style={{flexDirection:'row'}}>
                        <Text style={styles.txt}>City, State, Zip :</Text>
                        <Text style={styles.txt}> {address2}</Text>
                    </View>
                    :null
                    }
                </View>
                :null
                }
                {billTo
                ?<TouchableOpacity
                style={{backgroundColor:'#cead00', width:wp('50%'), alignSelf:'center', borderRadius:10, marginTop:hp('2%')}}
                onPress={() => 
                {sendDebtor()
                setDebtor(true)}}>
                    <Text style={{...styles.txt, padding:wp('2%'), fontSize:wp('5%'), color:'white'}}>Find Debtor</Text>
                </TouchableOpacity>
                :null
                }
                <CFSInvoiceInput 
                value={po}
                setValue={(text) => setPo(text)}
                title='PO#:'
                />
                { itemArray.length >= 1 
                    ?itemArray.map((item, index) => 
                    <View key={index} style={{borderRadius:10, padding:hp('1%'), backgroundColor:'#eaeaea', marginTop:hp('2%')}}>
                        <View style={{flexDirection:'row'}}><Text style={styles.txtDesc}>Description :</Text><Text style={styles.txtDesc}>{item.Description}</Text></View>
                        <View style={{flexDirection:'row'}}><Text style={styles.txtDesc}>Amount : </Text><Text style={styles.txtDesc}>{item.Amount}</Text></View>
                    </View>
                    )
                    : <View></View>
                }
                <Modal
                    animationType='slideInUp'
                    isVisible={addItem}
                    onRequestClose={() => setAddItem(false)}
                    onBackdropPress={() => setAddItem(false)}
                    backdropOpacity={0.8}
                    hasBackdrop={true}
                    backdropColor='black'
                    >
                        <View style={{width:wp('90%'), height:hp('30%'), backgroundColor:'white', borderRadius:10, alignSelf:'center', padding:hp('3%')}}>
                            <CFSInvoiceInput 
                            value={description}
                            setValue={(text) => setDescription(text)}
                            title='Description:'
                            />
                            <CFSInvoiceInput 
                            value={amount}
                            setValue={(text) => setAmount(text)}
                            title='Amount:'
                            />
                            <TouchableOpacity  
                                onPress={() => {if(description !='' && amount !=''){
                                     setItemArray([...itemArray, {Description:description,Amount:amount,Qyt:'1'}]),setAddItem(false),setCountSum(Number(countSum)+Number(amount))}
                                    else{
                                        alert('Fields can not be empty')
                                    }
                                    }}       
                            
                                style={{...styles.btn, marginTop:wp('3%'), backgroundColor:'#cead02'}}>
                                    <View >
                                        <Text 
                                        style={{textAlign:'justify' ,fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:26 , color:'white', padding:hp('1%'), textAlign:'center'}}>Add Item</Text>
                                    </View>
                            </TouchableOpacity>
                        </View> 
                    </Modal>

                    <TouchableOpacity  
                    onPress={() => {
                        setAddItem(true);
                        setInvoiceInput({Invoice: invoice, InvoiceDate: invoiceDate, BillTo: billTo, PO:po});
                        // countSum()
                    }}
                    style={{...styles.btn, marginTop:wp('3%')}}>
                        <View >
                            <Text 
                            style={{textAlign:'justify' ,fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:24 , color:'#484848', padding:hp('1%'), textAlign:'center'}}>Add New Item</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity  
                    onPress={() =>
                        selectImage()
                    }
                    style={{...styles.btn, marginTop:wp('1%')}}>
                        <View >
                            <Text 
                            style={{textAlign:'justify' ,fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:24 , color:'#484848', padding:hp('1%'), textAlign:'center'}}>Select Files, One by One</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity  
                    onPress={() =>  itemArray.length>0 ? createInvoice() : null}
                    style={{...styles.btn, marginTop:wp('1%'), backgroundColor:'#cead02'}}>
                        <View >
                            <Text 
                            style={{textAlign:'justify' ,fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:24 , color:'white', padding:hp('1%'), textAlign:'center'}}>CREATE INVOICE</Text>
                        </View>
                    </TouchableOpacity>
                    <Modal
                    animationType='slideInUp'
                    isVisible={debtor}
                    onRequestClose={() => setDebtor(false)}
                    onBackdropPress={() => setDebtor(false)}
                    backdropOpacity={0.8}
                    hasBackdrop={true}
                    backdropColor='black'
                    >
                        <View style={{width:wp('90%'), height:hp('80%'), backgroundColor:'#cead00', borderRadius:10, alignSelf:'center', padding:hp('3%')}}>
                            <FlatList 
                            showsVerticalScrollIndicator={false}
                            data={broker}
                            keyExtractor={(broker) => JSON.stringify(broker.DebtorKey)}
                            renderItem={({item}) =>{
                                return (
                                    <TouchableOpacity
                                    onPress={() => {
                                        setAddress1(item.Addr1)
                                        setAddress2(item.Addr2)
                                        setBillTo(item.Name)
                                        setAddress(true)
                                        setDebtor(false)
                                    }}
                                    >
                                    <View style={{ padding:hp('1%'), marginBottom:hp('1%'), borderRadius:10, backgroundColor:'white', justifyContent:'center'}}>
                                    <Text style={{...styles.txt, fontSize:wp('5%')}}>{item.Name}</Text>
                                    <Text style={{...styles.txt, fontSize:wp('5%')}}>{item.Addr1}</Text>
                                    <Text style={{...styles.txt, fontSize:wp('5%')}}>{item.Addr2}</Text>
                                    </View>
                                    </TouchableOpacity>
                                )
                            }}
                            />
                        </View> 
                    </Modal>
                    <Modal
                    animationType='slideInUp'
                    isVisible={uploading}
                    onRequestClose={() => setUploading(false)}
                    onBackdropPress={() => setUploading(false)}
                    backdropOpacity={0.9}
                    hasBackdrop={true}
                    backdropColor='white'
                    >
                        <View style={{width:wp('60%'), height:hp('30%'), alignItems:'center', justifyContent:'center', alignSelf:'center'}}>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', color:'#cead02', fontSize:wp('6%')}}>Images Uploading</Text>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', color:'#cead02', fontSize:wp('6%')}}>Please Wait</Text>
                            <BallIndicator size={50} color='#cead02'/>
                        </View>
                    </Modal>
            </View>
            </ScrollView>
    </KeyboardAwareView>
    </SafeAreaView>
    )
};

CFSInvoices.navigationOptions = ({ navigation }) => {
    return{
        header: null,
    }
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        height:'90%',
        alignItems:'center',
        flexGrow:1,
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    },
    btn:{
        marginVertical:hp('1%'),
        width:wp('60%'),
        alignSelf:'center',
        borderWidth:1, 
        borderColor:'#cdcdcd',
        borderRadius:4,
        backgroundColor:'white',
        shadowOffset: {
          width: 5,
          height: 5,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#cdcdcd',
        elevation:9,
        
    },
    scrollView: {
        marginHorizontal: 20,
        flexGrow:1,
        width:'100%',
        alignSelf:'center'
      },
      txtDesc: {
        color:'#484848',
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:24,
        paddingLeft:wp('2%')
    }
      
});

export default CFSInvoices;