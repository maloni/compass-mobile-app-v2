import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity} from 'react-native';
import  CFSlogo  from '../../components/CFSlogo';
import LoveCompass from '../../components/LoveCompass'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';

const FundingSolutionsScreen = ({ navigation }) => {

    return(
           <View style={styles.container}> 
                <CFSlogo />
                    <TouchableOpacity  
                    onPress={() => navigation.navigate('CFSCompanyLogIn')}>
                        <View style={{...styles.btn, marginTop:hp('10%')}}>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:hp('1.5%')}}>COMPANY LOGIN</Text>
                       </View>
                    </TouchableOpacity>
                    <TouchableOpacity  
                    onPress={() => navigation.navigate('')}>
                        <View style={styles.btn}>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:hp('1.5%'), textAlign:'center'}}>DRIVER LOGIN</Text>
                        </View>
                    </TouchableOpacity>
                <Text style={{...styles.txt, marginTop:'10%'}}>Don't have CPS fuelAccount?</Text>
                <View style={{flexDirection:'row', alignSelf:'center'}}>
                <Text style={{...styles.txt, color:'#cead02'}}>Click here</Text><Text style={styles.txt}>to apply and start earning</Text></View>
                <Text style={{...styles.txt, marginBottom:'10%'}}>discounts today!</Text>
                <View style={{width:wp('80%'), position:'absolute', bottom:hp('7%'), alignSelf:'center'}}>
                    <LoveCompass />
                </View>
            </View>
            
    )
    
};



FundingSolutionsScreen.navigationOptions = ({ navigation }) => {
    return{
        header: null,
    }
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        marginTop: Constants.statusBarHeight,
        height:'100%',
    },
        btn:{
            marginVertical:hp('1.3%'),
        width:'70%',
        alignSelf:'center',
        borderWidth:3, 
        borderColor:'#cead02', 
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
          width: 10,
          height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#484848',
        elevation: 12,
        
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    }
      
});

export default FundingSolutionsScreen;