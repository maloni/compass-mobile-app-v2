import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity, SafeAreaView, ScrollView,Platform} from 'react-native';
import  CFSlogo  from '../../components/CFSlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import CFSInvoiceInput from '../../components/CFSInvoiceInput';
import * as ImagePicker from 'expo-image-picker';
import Axios from 'axios';


const CFSPayments = ({ navigation }) => {

    const [stil, setStil] = useState({widthActive:5, colorActive:'#cead02', widthInactive:2 , colorInactive:'#e5e5e6'});
    const [active , setActive] = useState(true);

    let cfsClientNo = navigation.getParam('cfsClientNo');
    let cfsUserToken = navigation.getParam('cfsUserToken');

    const [totalAmount, setTotalAmount] = useState(100000.01)
    const [aging1, setAging1] = useState(45000.46)
    const [aging2, setAging2] = useState(22402)
    const [aging3, setAging3] = useState(33012.55)
    const [aging4, setAging4] = useState(13123)
    const [aging5, setAging5] = useState(43412)

    console.log(cfsClientNo, cfsUserToken);

    useEffect(() => {
        let forSend = "ClientNo="+cfsClientNo+"&token="+cfsUserToken;
        Axios.post('https://apps.compassaws.net/api/cfs/accountSummary', forSend)
        .then(response => {
            console.log(response)
        })
        .then(err => {
            console.log(err)
        })
    },[]);

    return(
    <SafeAreaView style={{width:'100%', height:'100%'}}>
    <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
        <ScrollView      
            contentContainerStyle={styles.container}           
            automaticallyAdjustContentInsets={true}
            showsVerticalScrollIndicator={false}
            bounces={true}
        >
           <View> 
                <CFSlogo />
                <Text style={{
                        fontFamily:'Myriad-Pro-Bold-Condensed',
                        fontSize:hp('8%'),
                        color:'#484848',
                        alignSelf:'center'}}>${(totalAmount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</Text>
                    <Text style={{
                        fontFamily:'Myriad-Pro-Bold-Condensed',
                        fontSize:hp('3%'),
                        color:'#484848',
                        alignSelf:'center'}}>A/R</Text>

                <View
                style={{ flexDirection:'row', justifyContent:'space-evenly', marginTop:hp('2%')}}
                >
                    <TouchableOpacity
                    onPress={() =>{setActive(true), setStil({widthActive:5, colorActive:'#cead02', widthInactive:2 , colorInactive:'#e5e5e6'})}}
                    style={{alignItems:'center', width:'50%',borderBottomWidth:stil.widthActive, borderBottomColor:stil.colorActive,}}
                    >
                        <Text
                        style={{padding:wp('3%'), fontFamily:'Myriad-Pro-Bold-Condensed',color:'#484848',fontSize:hp('3%'),}}
                        >OPEN INVOICES</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    onPress={() => {setActive(false), setStil({widthActive:2, colorActive:'#e5e5e6', widthInactive:5 , colorInactive:'#cead02'})}}
                    style={{alignItems:'center', width:'50%',borderBottomWidth:stil.widthInactive, borderBottomColor:stil.colorInactive,}}
                    >
                        <Text
                        style={{padding:wp('3%'), fontFamily:'Myriad-Pro-Bold-Condensed',color:'#484848',fontSize:hp('3%'),}}
                        >PAID INVOICES</Text>
                    </TouchableOpacity>
                </View>
                {
                    active
                    ? <CFSOpenInvoices 
                    aging1={aging1}
                    aging2={aging2}
                    aging3={aging3}
                    aging4={aging4}
                    aging5={aging5}
                    totalAmount={totalAmount}
                    />
                    : <CFSPaidInvoices />
                }

                    
            </View>
            </ScrollView>
    </KeyboardAwareView>
    </SafeAreaView>
    )
};

const CFSOpenInvoices = ({navigation, aging1, totalAmount, aging2, aging3, aging4, aging5}) => {
    

    return(
        <View>
            <View>
                <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                    <View style={{flexDirection:'column', justifyContent:'center'}}>
                        <Text style={styles.txt}>AGING 1-30 DAYS</Text>
                        <Text style={styles.txt}>${(aging1).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</Text>
                    </View>
                    <View style={{borderRadius:10 ,backgroundColor:'#cead02', margin:wp('5%')}}>
                        <Text style={{padding:wp('3%'), fontSize:30, fontFamily:'Myriad-Pro-Bold-Condensed', color:'white'}}>{Math.round(aging1/totalAmount*100)}%</Text>
                    </View>
                </View>
                <View style={{height:hp('0.7%'), borderRadius:40, backgroundColor:'#e5e5e6', width:'100%', justifyContent:'center'}}>
                        <View style={{height:hp('1%'), borderRadius:40, backgroundColor:'#cead02', width:`${Math.round(aging1/totalAmount*100)}%`}}></View>
                    </View>
            </View>
            <View>
                <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                    <View style={{flexDirection:'column', justifyContent:'center'}}>
                        <Text style={styles.txt}>AGING 31-45 DAYS</Text>
                        <Text style={styles.txt}>${(aging2).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</Text>
                    </View>
                    <View style={{borderRadius:10 ,backgroundColor:'#cead02', margin:wp('5%')}}>
                        <Text style={{padding:wp('3%'), fontSize:30, fontFamily:'Myriad-Pro-Bold-Condensed', color:'white'}}>{Math.round(aging2/totalAmount*100)}%</Text>
                    </View>
                </View>
                <View style={{height:hp('0.7%'), borderRadius:40, backgroundColor:'#e5e5e6', width:'100%', justifyContent:'center'}}>
                        <View style={{height:hp('1%'), borderRadius:40, backgroundColor:'#cead02', width:`${Math.round(aging2/totalAmount*100)}%`}}></View>
                    </View>
            </View>
            <View>
                <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                    <View style={{flexDirection:'column', justifyContent:'center'}}>
                        <Text style={styles.txt}>AGING 46-60 DAYS</Text>
                        <Text style={styles.txt}>${(aging3).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</Text>
                    </View>
                    <View style={{borderRadius:10 ,backgroundColor:'#cead02', margin:wp('5%')}}>
                        <Text style={{padding:wp('3%'), fontSize:30, fontFamily:'Myriad-Pro-Bold-Condensed', color:'white'}}>{Math.round(aging3/totalAmount*100)}%</Text>
                    </View>
                </View>
                <View style={{height:hp('0.7%'), borderRadius:40, backgroundColor:'#e5e5e6', width:'100%', justifyContent:'center'}}>
                        <View style={{height:hp('1%'), borderRadius:40, backgroundColor:'#cead02', width:`${Math.round(aging3/totalAmount*100)}%`}}></View>
                    </View>
            </View>
        </View>
    )
}

const CFSPaidInvoices = () => {
    return(
        <View>
            <Text>
                Paid
            </Text>
        </View>
    )
}

CFSPayments.navigationOptions = ({ navigation }) => {
    return{
        header: null,
    }
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        height:'90%',
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent'
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    },
    btn:{
        marginVertical:hp('1%'),
        width:wp('60%'),
        alignSelf:'center',
        borderWidth:1, 
        borderColor:'#cdcdcd',
        borderRadius:4,
        backgroundColor:'white',
        shadowOffset: {
          width: 5,
          height: 5,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#cdcdcd',
        elevation:9,
        
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default CFSPayments;