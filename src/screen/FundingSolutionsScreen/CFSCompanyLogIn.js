import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity} from 'react-native';
import  CFSlogo  from '../../components/CFSlogo';
import LoveCompass from '../../components/LoveCompass'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Axios from 'axios';
import Constants from 'expo-constants';


const CFSCompanyLogIn = ({ navigation }) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');



    const send = function (user,pass){
        const data = "username="+user+"&password="+pass
            Axios.post('https://apps.compassaws.net/api/cfs/login', data)
                .then(response => {
                    const res = response.data
                    if(res.StatusResponse == '1' && res.ExtraInfoResponse =='2'){
                        navigation.navigate('CFSCompanyHome',{user : user, cfsUserToken : res.Token, cfsClientNo: res.ClientNo, ClientKey: res.ClientKey})
                        console.log(res)
                    } else {
                        alert(res.InfoResponse)
                    }
                })
    }

    return(
           <View style={styles.container}> 
                <CFSlogo />
                    <TextInput 
                    placeholder='Enter Username'
                    onChangeText={(text) => setUsername(text)}
                    style={{fontSize:20,fontFamily:'Myriad-Pro-Bold-Condensed', borderWidth:3, borderRadius:10, borderColor:'#cead02', padding:hp('1.5%'), width:wp('70%'), alignSelf:'center', margin:hp('2%')}}
                    />
                    <TextInput
                    onChangeText={(text) => setPassword(text)}
                    placeholder='Enter Password'
                    style={{fontSize:20,fontFamily:'Myriad-Pro-Bold-Condensed', borderWidth:3, borderRadius:10, borderColor:'#cead02', padding:hp('1.5%'), width:wp('70%'), alignSelf:'center'}}
                    />
                    <TouchableOpacity  
                    onPress={() => 
                        // navigation.navigate('CFSCompanyHome')
                        send(username,password)
                    }>
                        <View style={{...styles.btn, marginTop:wp('10%')}}>
                            <Text style={{textAlign:'justify' ,fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'white', padding:hp('1%'), textAlign:'center'}}>LOG IN</Text>
                        </View>
                    </TouchableOpacity>
                <Text style={{...styles.txt, marginTop:'10%'}}>Don't have CPS fuelAccount?</Text>
                <View style={{flexDirection:'row', alignSelf:'center'}}>
                <Text style={{...styles.txt, color:'#cead02'}}>Click here</Text><Text style={styles.txt}>to apply and start earning</Text></View>
                <Text style={{...styles.txt, marginBottom:'10%'}}>discounts today!</Text>
                <View style={{alignSelf:'center', width:'80%', position:'absolute', bottom:hp('4%') }}>
                    <LoveCompass />
                </View>
            </View>
            
    )
    
};

CFSCompanyLogIn.navigationOptions = ({ navigation }) => {
    return{
        header: null,
    }
};

const styles = StyleSheet.create({
    container: {
        width:wp('90%'),
        marginHorizontal:'5%',
        height:hp('90%'),
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent',
    },
        btn:{
            marginVertical:hp('1.3%'),
        width:'70%',
        alignSelf:'center',
        borderWidth:3, 
        borderColor:'#cead02', 
        borderRadius:10,
        backgroundColor:'#cead02',
        shadowOffset: {
          width: 10,
          height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#484848',
        elevation: 12,
        
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    }
      
});

export default CFSCompanyLogIn;