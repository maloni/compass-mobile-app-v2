import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity, SafeAreaView, ScrollView,Platform, Picker} from 'react-native';
import  CFSlogo  from '../../components/CFSlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import Axios from 'axios';
import DateTimePicker from '@react-native-community/datetimepicker';
import { FlatList } from 'react-native-gesture-handler';
import moment from 'moment';

const CFSNotification = ({ navigation }) => {


    const [dateFrom, setDateFrom] = useState(new Date()); 
    const [showDatePickerFrom, setShowDatePickerFrom] = useState(false);

    const [dateTo, setDateTo] = useState(new Date());
    const [showDatePickerTo, setShowDatePickerTo] = useState(false);

    const [category, setCategory] = useState();

    const [username, setUsername] = useState(navigation.getParam('username'))
    const [cfsUserToken, setCfsUserToken] = useState(navigation.getParam('cfsUserToken'))
    const [cfsClientNo, setCfsClientNo] = useState(navigation.getParam('cfsClientNo'))

    

    const notificationSearch = () => {

        const dateFromText = moment(dateFrom).format('LL')
        const dateToText = moment(dateTo).format('LL')
    
        var notificationSetUp="ClientNo="+cfsClientNo+"&token="+cfsUserToken+"&username="+username+"&dateFrom="+dateFromText+"&dateTo="+dateToText;

        Axios.post('https://apps.compassaws.net/api/cfs/notifications', notificationSetUp)
        .then((res) => {
            console.log(res)
        })
        .catch((err) =>{
            console.log(err)
        })
    }

    console.log(moment(dateTo).format('LL'))

    return(
    <SafeAreaView style={{width:'100%', height:'100%'}}>
    <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
        <ScrollView      
            contentContainerStyle={styles.container}           
            automaticallyAdjustContentInsets={true}
            showsVerticalScrollIndicator={false}
            bounces={true}
        >
           <View style={{alignItems:"center"}}> 
                <CFSlogo />
                <View
                style={{alignItems:'center', width:wp('80%'), alignSelf:'center', flexDirection:'row', justifyContent:'space-between'}}>
                    <TouchableOpacity
                    style={styles.date}
                    onPress={() => setShowDatePickerFrom(true)}
                    >
                        <Text style={{...styles.txt, color:'#cead02', fontSize:hp('2.5%')}}>Start Date</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    style={styles.date}
                    onPress={() => setShowDatePickerTo(true)}
                    >
                        <Text style={{...styles.txt, color:'#cead02', fontSize:hp('2.5%')}}>End Date</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity
                    style={styles.searchBtn}
                    onPress={() => notificationSearch()}
                    >
                        <Text style={{...styles.txt, color:'#cead02', fontSize:hp('2.5%')}}>Search Notification</Text>
                    </TouchableOpacity>
                    <Picker
                    onValueChange={(value) => setCategory(value)}
                    selectedValue={category}
                    >
                        <Picker.Item label='Categories' value=''/>
                        <Picker.Item label='Alerts' value='ALERT' />
                        <Picker.Item label='No Buy' value='NO BUY' />
                        <Picker.Item label='Short Pay' value='SHORT PAY' />
                        <Picker.Item label='Hold' value='HOLD_NOTIFY' />
                        <Picker.Item label='Original' value='ORIGINAL_NOTIFY' />
                        <Picker.Item label='User Message' value='USER_MESSAGE' />
                    </Picker>
                </View>
                    {showDatePickerFrom
                        ?<DateTimePicker
                        mode="date"
                        display="spinner"
                        defaultDate={new Date()}
                        value={new Date()}
                        onChange={(event, value) => {
                          setShowDatePickerFrom(!showDatePickerFrom);
                          setDateFrom(value);
                        }}
                                    />
                        :null
                    }    
                    {showDatePickerTo
                    ?<DateTimePicker 
                    mode="date"
                    display="spinner"
                    defaultDate={new Date()}
                    value={new Date()}
                    onChange={(event, value) =>{
                        setShowDatePickerTo(!showDatePickerTo);
                        setDateTo(value);
                    }}
                    />
                    :null
                    }                
            </View>
            </ScrollView>
    </KeyboardAwareView>
    </SafeAreaView>
    )
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        height:'90%',
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent',
    },
    searchBtn:{
        width:wp('80%'),
        borderWidth:2,
        marginTop:hp('2%'),
        padding:wp('2%'),
        alignItems:'center',
        borderRadius:10,
        borderColor:'#cead02'
    },
    date:{
        width:(wp('35%')),
        borderWidth:2,
        borderColor:'#cead02',
        padding:wp('2%'),
        borderRadius:10
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    },
    btn:{
        marginVertical:hp('1%'),
        width:wp('60%'),
        alignSelf:'center',
        borderWidth:1, 
        borderColor:'#cdcdcd',
        borderRadius:4,
        backgroundColor:'white',
        shadowOffset: {
          width: 5,
          height: 5,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#cdcdcd',
        elevation:9,
        
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default CFSNotification;