import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity} from 'react-native';
import  CFSlogo  from '../../components/CFSlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import Axios from 'axios';
import { FlatList } from 'react-native-gesture-handler';

const CFSBroker = ({ navigation }) => {

    const [companyName, setCompanyName] = useState('');
    const [mc, setMc] = useState('')
    const username = navigation.getParam('username')
    const [broker, setBroker] = useState([])
    
console.log(username)

    const send = function(name , mc){
        const forSend = "username="+username+"&company="+name+"&mc="+mc;
            Axios.post('https://apps.compassaws.net/api/cfs/brokerSearch', forSend)
            .then(response => {
                setBroker(response.data)
                //console.log(response.data)
            })
            .then(error => {
                console.log(error);
            })
    }

    return(
           <View style={styles.container}> 
                <CFSlogo />
                <TextInput 
                    placeholder='Company Name'
                    onChangeText={(text) => setCompanyName(text)}
                    style={{fontSize:20,fontFamily:'Myriad-Pro-Bold-Condensed', borderWidth:1, borderRadius:10, borderColor:'#cead02', padding:hp('1%'), width:wp('70%'), alignSelf:'center', margin:hp('2%'), backgroundColor:'white',
                    shadowOffset: {
                      width: 10,
                      height: 6,
                    },
                    shadowOpacity: 0.30,
                    shadowRadius: 12,
                    shadowColor:'#484848',
                    elevation: 3,}}
                    />
                    <TextInput
                    onChangeText={(text) => setMc(text)}
                    placeholder='# MC'
                    style={{fontSize:20,fontFamily:'Myriad-Pro-Bold-Condensed', borderWidth:1, borderRadius:10, borderColor:'#cead02', padding:hp('1%'), width:wp('70%'), alignSelf:'center', backgroundColor:'white',
                    shadowOffset: {
                      width: 10,
                      height: 6,
                    },
                    shadowOpacity: 0.30,
                    shadowRadius: 12,
                    shadowColor:'#484848',
                    elevation: 3,}}
                    />
                    <TouchableOpacity  
                    onPress={() =>
                    send(companyName,mc)}>
                        <View style={{...styles.btn, marginTop:wp('4%')}}>
                            <Text style={{textAlign:'justify' ,fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:24 , color:'white', padding:hp('1%'), textAlign:'center'}}>Search Broker</Text>
                        </View>
                    </TouchableOpacity>
                    <FlatList 
                    horizontal
                    data={broker}
                    keyExtractor={(broker) => broker.DebtorKey}
                    renderItem={({item}) =>{
                        return(
                        <View style={{maxHeight:hp('60%'),borderColor:'#e5e5e6', borderWidth:1, borderRadius:10, paddingHorizontal:wp('4%'), width:wp('90%'), paddingTop:hp('2%'), marginRight:wp('10%')}}>
                            <View style={{flexDirection:'row', alignItems:'center'}}><Text style={styles.brokerTxt}>MC # :</Text><Text style={styles.brokerTxt}>{item.MC}</Text></View>
                            <View style={{flexDirection:'row', alignItems:'center'}}><Text style={styles.brokerTxt}>Company :</Text><Text style={styles.brokerTxt}>{item.company}</Text></View>
                            <View style={{flexDirection:'row', alignItems:'center'}}><Text style={styles.brokerTxt}>Address :</Text><Text style={styles.brokerTxt}>{item.addr1}</Text></View>
                            <View style={{flexDirection:'row', alignItems:'center'}}><Text style={styles.brokerTxt}>City :</Text><Text style={styles.brokerTxt}>{item.city}</Text></View>
                            <View style={{flexDirection:'row', alignItems:'center'}}><Text style={styles.brokerTxt}>State :</Text><Text style={styles.brokerTxt}>{item.st}</Text></View>
                            <View style={{flexDirection:'row', alignItems:'center'}}><Text style={styles.brokerTxt}>Phone :</Text><Text style={styles.brokerTxt}>{item.phone1}</Text></View>
                            
                            <Text style={{...styles.brokerTxt, alignSelf:'center', fontSize:20}}>--- Credit Details ---</Text>
                                <View style={{flexDirection:'row', alignItems:'center', alignSelf:'center', marginVertical:hp('1%')}}>
                                    <Text style={styles.brokerTxt}>Credit Rating : </Text>
                                    <View style={{backgroundColor:'green', padding:5,paddingHorizontal:14, borderRadius:10, marginLeft:10}}>
                                        <Text style={{...styles.brokerTxt, fontSize:36, color:'white'}}>{item.CredRate}</Text>
                                    </View>
                                </View>
                            <Text style={{...styles.brokerTxt, fontSize:20, alignSelf:'center'}}>Payindex is {item.Paydex} & Credit Score is {item.CreditScore}</Text>
                            <View style={{flexDirection:'row', alignItems:'center'}}>
                            <View style={{flexDirection:'column', justifyContent:'space-evenly', margin:5}}><Text style={styles.txtTable}>        </Text><Text style={styles.txtTable}>Paydex</Text><Text style={styles.txtTable}>Invoices Number</Text></View>
                            <View style={{flexDirection:'column', justifyContent:'space-evenly'}}><Text style={styles.txtTable}>ALL</Text><Text style={styles.txtTable}>{item.CalcDaysToPay1}</Text><Text style={styles.txtTable}>{item.CalcInvCount1}</Text></View>
                            <View style={{flexDirection:'column', justifyContent:'space-evenly'}}><Text style={styles.txtTable}>90 Days</Text><Text style={styles.txtTable}>{item.CalcDaysToPay2}</Text><Text style={styles.txtTable}>{item.CalcInvCount2}</Text></View>
                            <View style={{flexDirection:'column', justifyContent:'space-evenly'}}><Text style={styles.txtTable}>60 Days</Text><Text style={styles.txtTable}>{item.CalcDaysToPay3}</Text><Text style={styles.txtTable}>{item.CalcInvCount3}</Text></View>
                            </View>

                            </View>
                        )
                    }}
                    />
            </View>
    )
};

CFSBroker.navigationOptions = ({ navigation }) => {
    return{
        header: null,
    }
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        height:'96%',
        marginTop: Constants.statusBarHeight,
    },
        btn:{
            marginVertical:hp('1.3%'),
        width:'70%',
        alignSelf:'center',
        borderWidth:3, 
        borderColor:'#cead02', 
        borderRadius:10,
        backgroundColor:'#cead02',
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    },
    brokerTxt:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:16,
        color:'#484848',

    },
    txtTable:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:18,
        alignSelf:'center',
        padding:4
    }
      
});

export default CFSBroker;