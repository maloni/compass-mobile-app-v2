import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity} from 'react-native';
import  CFSlogo  from '../../components/CFSlogo';
import LoveCompass from '../../components/LoveCompass'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import CFSButton from "../../components/CFSButton";
import Constants from 'expo-constants';
import Modal from "react-native-modal";


const CFSCompanyHome = ({ navigation }) => {

    const username = navigation.getParam('user');
    const [cashReq, setCashReq] = useState(false);
    const [name, setName] = useState('')
    const [amount, setAmount] = useState('')
    const cfsUserToken = navigation.getParam('cfsUserToken')
    const cfsClientNo = navigation.getParam('cfsClientNo')
    const ClientKey = navigation.getParam('ClientKey')
    
    console.log(cfsUserToken)

    return(
           <View style={styles.container}> 
                <CFSlogo />
                    <Modal
                    animationType='slideInUp'
                    isVisible={cashReq}
                    onRequestClose={() => setCashReq(false)}
                    onBackdropPress={() => setCashReq(false)}
                    backdropOpacity={0.8}
                    hasBackdrop={true}
                    backdropColor='black'
                    >
                        <View style={{width:wp('90%'), height:hp('35%'), backgroundColor:'white', borderRadius:10, alignSelf:'center'}}>
                            <View style={{flexDirection:"row", borderWidth:1, width:'90%', borderRadius:5, alignSelf:'center', borderColor:'#cead02', padding:9, marginVertical:20}}>
                            <Text style={styles.txt}>Your Name : </Text>
                                <TextInput
                                value={name}
                                onChangeText={(text) => setName(text)}
                                placeholder='Enter Your Name'
                                />
                                </View>
                            <View style={{flexDirection:"row", borderWidth:1, width:'90%', borderRadius:5, alignSelf:'center', borderColor:'#cead02', padding:9}}>
                                <Text style={styles.txt}>Amount : </Text>
                                <TextInput
                                placeholder='Enter Wanted Amount'
                                onChangeText={(text) => setAmount(text)}
                                value={amount}
                                />
                                </View>
                            <TouchableOpacity
                            style={{backgroundColor:'#cead02', borderRadius:10, width:'80%', alignSelf:'center', marginVertical:'10%', padding:7}}
                            >
                                <Text style={{...styles.txt, color:'white'}}>Send Request</Text>
                            </TouchableOpacity>
                        </View>
                        
                    </Modal>
                    <Text style={{
                        fontFamily:'Myriad-Pro-Bold-Condensed',
                        fontSize:hp('8%'),
                        color:'#484848',
                        alignSelf:'center'}}>$2,063.87</Text>
                    <Text style={{
                        fontFamily:'Myriad-Pro-Bold-Condensed',
                        fontSize:hp('3%'),
                        color:'#484848',
                        alignSelf:'center'}}>Cash reserve</Text>
                    <TouchableOpacity 
                    onPress={() => setCashReq(true)}
                    style={{backgroundColor:'#cead02', borderRadius:10, width:wp('60%'), alignSelf:'center', marginVertical:hp('2%')}}>
                        <Text style={{
                            fontFamily:'Myriad-Pro-Bold-Condensed',
                            fontSize:hp('2%'),
                            color:'white',
                            alignSelf:'center',
                            padding:wp('2%')
                        }}>REQUEST CASH RESERVE</Text>
                    </TouchableOpacity>

                   <CFSButton 
                   navi={() => navigation.navigate('CFSBroker', {username:username})}
                   title='BROKER SEARCH'
                   />
                   <CFSButton 
                   navi={() => navigation.navigate('CFSPayments', {cfsUserToken: cfsUserToken, cfsClientNo:cfsClientNo})}
                   title='INVOICES/PAYMENTS'
                   />
                   <CFSButton 
                   navi={() => navigation.navigate('CFSSchedule', {username:username, cfsUserToken: cfsUserToken, cfsClientNo:cfsClientNo})}
                   title='PURCHASED SCHEDULE'
                   />
                   <CFSButton 
                   navi={() => navigation.navigate('CFSInvoices', {username:username, cfsUserToken: cfsUserToken, cfsClientNo:cfsClientNo, ClientKey:ClientKey})}
                   title='SUBMIT NEW INVOICES'
                   />
                   <CFSButton 
                   navi={() => navigation.navigate('CFSNotification',{cfsClientNo:cfsClientNo, cfsUserToken: cfsUserToken, username:username})}
                   title='NOTIFICATIONS'
                   />
                
            </View>
            
    )
    
};



CFSCompanyHome.navigationOptions = ({ navigation }) => {
    return{
        header: null,
    }
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        marginTop: Constants.statusBarHeight,
        height:'90%',
    },
        btn:{
        marginVertical:hp('1.3%'),
        width:'70%',
        alignSelf:'center',
        borderWidth:3, 
        borderColor:'#cead02', 
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
          width: 10,
          height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#484848',
        elevation: 12,
        
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    }
      
});

export default CFSCompanyHome;