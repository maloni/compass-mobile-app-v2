import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, SafeAreaView, ScrollView,Platform, Picker, Image, FlatList} from 'react-native';
import  CEFlogo  from '../../components/CEFlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import LoveCompass from '../../components/LoveCompass';
import Axios from 'axios';
import numeral from 'numeral';
import { WaveIndicator } from 'react-native-indicators';


const EquipmentFinanceInventory = ({ navigation }) => {
    const [trucks, setTrucks] = useState('')
    const [loading, setLoading] = useState(false)
    const [search, setSearch] = useState(false)
    const [make,setMake] = useState('')
    const [model, setModel] = useState('')
    const [minYear, setMinYear] = useState('0')
    const [maxYear, setMaxYear] = useState('99991')
    const [minMilage, setMinMilage] = useState('0')
    const [maxMIlage, setMaxMilage] = useState('999999999')
    const [minPrice, setMinPrice] = useState('0')
    const [maxPrice, setMaxPrice] = useState('99999991')
    const [makeAll, setMakeAll] = useState()
    const [modelAll, setModelAll] = useState()
    const [modelAll1, setModelAll1] = useState([])
    const [maksId , setMaksId] = useState()
    const [truckMakes, setTruckMakes] = useState()

    const year = ['2000', '2001', '2002' ,'2003', '2004', '2005','2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014','2015','2016','2017','2018','2019','2020']
    const mileage = ['100000', '200000', '300000', '400000', '500000', '600000', '700000', '800000', '900000', '1000000', '1500000', '2000000']
    const price = ['10000', '20000', '30000', '40000', '50000', '60000', '70000', '80000', '90000', '100000', '110000', '120000', '130000', '140000', '150000']

    useEffect(() => {
        const fetchData = async () => {
        const result = await Axios.get('https://apps.compassaws.net/api/card/getInventCEF');
                setTrucks(result.data.trucks)
                setMakeAll(result.data.maks)
                setLoading(true)
        };
        fetchData();
    }, [])

    

    const searchTruck = () => {

        const minPrice1 = minPrice*100;
        const maxPrice1 = maxPrice*100;

        const filter = {
        // makeId:make,
        // modelId:model,
        year:{ $gte:minYear, $lte:maxYear },
        miles:{ $gte:minMilage, $lte:maxMIlage },
        askingPrice:{ $gte:minPrice1, $lte:maxPrice1 }
        }
        console.log(filter)
        const query = "filter=" + encodeURIComponent(JSON.stringify(filter));
        Axios.post('https://apps.compassaws.net/api/card/getInventCEFFilter', query)
        .then((res) => {
            setLoading(true)
            setTrucks(res.data.trucks)
        })
        .then((err) => {
            console.log(err)
        })
    }

    const modelFilter = (item) => {
        setMake(item);
        for(i=0; i<trucks.length; i++){
            if(item == trucks[i].makeId){
                console.log('marko')
                setModelAll1([...modelAll1, trucks[i].modelId_name])
            }
        }
    }

    // console.log(makeAll)

    return(
    <SafeAreaView style={{width:'100%', height:'100%'}}>
    <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
            <View style={styles.container} >
                <CEFlogo />
                {search
                ?<View>
                    <View style={{borderWidth:1, borderColor:'#cead02', width:wp('90%'), borderRadius:10, alignSelf:'center'}}>
                    <Picker
                    style={{width:'100%', alignSelf:'center', borderWidth:1, borderColor:'#cead02'}}
                    selectedValue={make}
                    onValueChange={(item) => modelFilter(item)}
                    >
                        <Picker.Item label='All makes' value='' />
                        {
                            makeAll.map((item, index) => {
                                return ( <Picker.Item label={item.name} value={item._id} key={index} /> )
                            })
                        }
                    </Picker>
                    </View>
                    <View style={{borderWidth:1, borderColor:'#cead02', width:wp('90%'), borderRadius:10, alignSelf:'center', marginTop:hp('1.5%')}}>
                    <Picker
                    style={{width:'100%', alignSelf:'center', borderWidth:1, borderColor:'#cead02'}}
                    selectedValue={model}
                    onValueChange={(item) => setModel(item)}
                    >
                        <Picker.Item label='All models' value='' />
                    {
                        modelAll1.map((item, index) => {
                            return ( <Picker.Item label={item} value={item} key={index} /> )
                        })
                    }
                    </Picker>
                    </View>
                    <View style={{width:wp('90%'),  alignSelf:'center',flexDirection:'row', justifyContent:'space-between', marginTop:hp('1.5%')}}>
                    <View style={{borderWidth:1, borderColor:'#cead02',borderRadius:10, width:'48%'}}>
                    <Picker
                    style={{alignSelf:'center', borderWidth:1, borderColor:'#cead02', width:'100%'}}
                    selectedValue={minYear}
                    onValueChange={(item) => setMinYear(item)}
                    >
                        <Picker.Item label='Choose min year' value='0' />
                        <Picker.Item label='All' value='1' />
                    {
                        year.map((item, index) => {
                            return (<Picker.Item label={item} value={item} key={index} />)
                        })
                    }
                    </Picker>
                    </View>
                    <View style={{borderWidth:1, borderColor:'#cead02',borderRadius:10, width:'48%'}}>
                    <Picker
                    style={{ alignSelf:'center', borderWidth:1, borderColor:'#cead02', width:'100%'}}
                    selectedValue={maxYear}
                    onValueChange={(item) => setMaxYear(item)}
                    >
                        <Picker.Item label='Choose max year' value='99991' />
                        <Picker.Item label='All' value='9999' />
                    {
                        year.map((item, index) => {
                            return (<Picker.Item label={item} value={item} key={index} />)
                        })
                    }
                    </Picker>
                    </View>
                    </View>
                    <View style={{width:wp('90%'),  alignSelf:'center',flexDirection:'row', justifyContent:'space-between', marginTop:hp('1.5%')}}>
                    <View style={{borderWidth:1, borderColor:'#cead02',borderRadius:10, width:'48%'}}>
                    <Picker
                    style={{alignSelf:'center', borderWidth:1, borderColor:'#cead02', width:'100%'}}
                    selectedValue={minMilage}
                    onValueChange={(item) => setMinMilage(item)}
                    >   
                        <Picker.Item label='Choose min mileage' value='0'  />
                        <Picker.Item label='All' value='1'  />
                        <Picker.Item label='Under 100.000' value='1'  />
                        {
                            mileage.map((item, index) => {
                                return(<Picker.Item value={item} label={numeral(item).format('0,0')} key={index} />)
                            })
                        }
                    </Picker>
                    </View>
                    <View style={{borderWidth:1, borderColor:'#cead02',borderRadius:10, width:'48%'}}>
                    <Picker
                    style={{ alignSelf:'center', borderWidth:1, borderColor:'#cead02', width:'100%'}}
                    selectedValue={maxMIlage}
                    onValueChange={(item) => setMaxMilage(item)}
                    >
                        <Picker.Item label='Choose max mileage' value='999999999'  />
                    {
                        mileage.map((item, index) => {
                            return(<Picker.Item value={item} label={item} key={index} />)
                        })
                    }
                    </Picker>
                    </View>
                    </View>
                    <View style={{width:wp('90%'),  alignSelf:'center',flexDirection:'row', justifyContent:'space-between', marginTop:hp('1.5%')}}>
                    <View style={{borderWidth:1, borderColor:'#cead02',borderRadius:10, width:'48%'}}>
                    <Picker
                    style={{alignSelf:'center', borderWidth:1, borderColor:'#cead02', width:'100%'}}
                    selectedValue={minPrice}
                    onValueChange={(item) => setMinPrice(item)}
                    >
                        <Picker.Item label='Choose min price' value='0'  />
                        <Picker.Item label='Under 10.000' value='1'  />
                        {
                            price.map((item, index) => {
                                return(<Picker.Item value={item} label={item} key={index} />)
                            })
                        }
                    </Picker>
                    </View>
                    <View style={{borderWidth:1, borderColor:'#cead02',borderRadius:10, width:'48%'}}>
                    <Picker
                    style={{ alignSelf:'center', borderWidth:1, borderColor:'#cead02', width:'100%'}}
                    selectedValue={maxPrice}
                    onValueChange={(item) => setMaxPrice(item)}
                    >
                        <Picker.Item label='Choose max price' value='99999991'  />
                    {
                        price.map((item, index) => {
                            return(<Picker.Item value={item} label={item} key={index} />)
                            })
                    }                   
                    </Picker>
                    </View>
                    </View>

                    <TouchableOpacity  
                    style={styles.btn}
                    onPress={() => {searchTruck(), setSearch(false), setLoading(false)}}>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:hp('1%'), textAlign:'center'}}>SEARCH</Text>
                </TouchableOpacity>
                </View>
                :<TouchableOpacity  
                    style={styles.btn}
                    onPress={() => setSearch(true)}>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:hp('1%'), textAlign:'center'}}>SEARCH</Text>
                </TouchableOpacity>
                }   
                
                {loading
                ?<FlatList 
                data={trucks}
                keyExtractor={(item) => item._id}
                renderItem={({ item }) => {
                    // console.log(item.truckPhotos1)
                    let price = numeral(Number(item.askingPrice/100)).format('$0,0.00')
                    let miles = numeral(item.miles).format('0,0')
                
                    return(
                        <TouchableOpacity
                        style={{marginBottom:wp('2%')}}
                        onPress={() => navigation.navigate('EquipmentFinanceItem', { truck : item })}
                        >
                        <View style={styles.truckContainer}>
                            { item.truckPhotos1 != ''
                            ?<Image 
                            style={{width:wp('25%'), height:'60%', justifyContent:'center', marginRight:'5%', alignSelf:'center'}}
                            resizeMode='cover'
                            source={{uri: `https://s3.amazonaws.com/${item.truckPhotos1}`}}
                            />
                            :<Image 
                            style={{width:wp('25%'), height:'60%', justifyContent:'center', marginRight:'5%', alignSelf:'center'}}
                            resizeMode='cover'
                            source={require('../../img/noimage.png')}
                            />
                            }
                            <View style={{borderLeftWidth:5, paddingLeft:'2%', borderLeftColor:'#cead02', height:'90%', marginVertical:'2%'}}>
                                <Text style={styles.txt}>{item.year} {item.makeId_name} {item.modelId_name}</Text>
                                <Text style={styles.txtSmall}> </Text>
                                    <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                                        <Text style={styles.txtSmall}>{price}</Text>
                                        <Text style={{...styles.txtSmall, paddingRight:wp('3%')}}>{miles} mi</Text>
                                    </View>
                            </View>
                        </View>
                        </TouchableOpacity>
                    )
                }}
                />
                :<WaveIndicator size={50} color="#cead02"  style={{justifyContent:'center', alignSelf:'center', marginTop:hp('20%')}}/>
            }
                



                {/* <View style={styles.truckContainer}>
                    <Image 
                    style={{width:wp('25%'), height:'60%', justifyContent:'center', marginRight:'5%', alignSelf:'center'}}
                    resizeMode='cover'
                    source={{uri: 'https://s3.amazonaws.com/compass.trucksales/trucksales.inventory.b3faaf7f-27d2-1a10-1566-f3d1fdec571d.jpg'}}
                    />
                    <View style={{borderLeftWidth:5, paddingLeft:'2%', borderLeftColor:'#cead02', height:'90%', marginVertical:'2%'}}>
                        <Text style={styles.txt}> Freightliner - CASCADIA</Text>
                        <Text style={styles.txtSmall}> VIN 191J456UWUH45FDS58RT6</Text>
                            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                                <Text style={styles.txtSmall}>$ 42,600</Text>
                                <Text style={styles.txtSmall}>250,169mi</Text>
                            </View>
                    </View>
                </View>
                <View style={styles.truckContainer}>
                    <Image 
                    style={{width:wp('25%'), height:'60%', justifyContent:'center', marginRight:'5%', alignSelf:'center'}}
                    resizeMode='cover'
                    source={{uri: 'https://s3.amazonaws.com/compass.trucksales/trucksales.inventory.bf7fdd09-ce29-607d-6713-65348627c730.jpg'}}
                    />
                    <View style={{borderLeftWidth:5, paddingLeft:'2%', borderLeftColor:'#cead02', height:'90%', marginVertical:'2%'}}>
                        <Text style={styles.txt}> Freightliner - CASCADIA</Text>
                        <Text style={styles.txtSmall}> VIN 191J456UWUH45FDS58RT6</Text>
                            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                                <Text style={styles.txtSmall}>$ 42,600</Text>
                                <Text style={styles.txtSmall}>482,423 mi</Text>
                            </View>
                    </View>
                </View>
                <View style={styles.truckContainer}>
                    <Image 
                    style={{width:wp('25%'), height:'60%', justifyContent:'center', marginRight:'5%', alignSelf:'center'}}
                    resizeMode='cover'
                    source={{uri: 'https://s3.amazonaws.com/compass.trucksales/trucksales.inventory.84e3cd98-23a6-a837-12de-4710eb7f65bd.jpg'}}
                    />
                    <View style={{borderLeftWidth:5, paddingLeft:'2%', borderLeftColor:'#cead02', height:'90%', marginVertical:'2%'}}>
                        <Text style={styles.txt}> Volvo - VNL64T670</Text>
                        <Text style={styles.txtSmall}> VIN 191J456UWUH45FDS58RT6</Text>
                            <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                                <Text style={styles.txtSmall}>$ 42,600</Text>
                                <Text style={styles.txtSmall}>554,605 mi</Text>
                            </View>
                    </View>
                </View> */}


                    
            </View>
    </KeyboardAwareView>
    </SafeAreaView>
    )
};


EquipmentFinanceInventory.navigationOptions = ({ navigation }) => {
    return{
        header: null,
    }
};

const styles = StyleSheet.create({
    container: {
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent',
        paddingBottom: wp('10%')

    },
    txtSmall:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:18,
        alignSelf:'center',
        textAlign:'justify'
    },
    truckContainer:{
        marginTop:hp('2%'),
        paddingHorizontal:wp('2%'),
        backgroundColor:'white',
        width:wp('88%'), 
        height:hp('16%'), 
        flexDirection:'row', 
        alignSelf:'center', 
        borderRadius:10, 
        padding:'1%',
        shadowOffset: {
            width: 10,
            height: 6,
            },
            shadowOpacity: 0.30,
            shadowRadius: 12,
            shadowColor:'#484848',
            elevation: 8,
            
    },
    inputContainer:{
        flexDirection:'row', 
        borderBottomWidth:2 , 
        width:wp('80%'), 
        alignSelf:'center', 
        marginBottom:hp('1%'), 
        borderBottomColor:'#eaeaea'
    },
    input:{
        alignSelf:'center',
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:wp('4%'),
        padding:wp('1%'),
        width:'60%'
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:wp('4.5%'),
        alignSelf:'flex-start',
        padding:hp('2%'),
        textAlign:'justify'
    },
    btn:{
        marginVertical:hp('2%'),
        width:'60%',
        alignSelf:'center',
        borderWidth:3, 
        borderColor:'#cead02', 
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
        width: 10,
        height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#484848',
        elevation: 8,
        
        },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default EquipmentFinanceInventory;