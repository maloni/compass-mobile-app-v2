import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity, SafeAreaView, ScrollView,Platform} from 'react-native';
import  CEFlogo  from '../../components/CEFlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import LoveCompass from '../../components/LoveCompass';
import Modal from "react-native-modal";
import { WebView } from 'react-native-webview';


const EquipmentFinanceScreen = ({ navigation }) => {

    const[paymentView, setPaymentView] = useState(false)

    return(
    <SafeAreaView style={{width:'100%', height:'100%'}}>
    <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
        <ScrollView      
            contentContainerStyle={styles.container}           
            automaticallyAdjustContentInsets={true}
            showsVerticalScrollIndicator={false}
            bounces={true}
        >
                <CEFlogo />
                <TouchableOpacity  
                    onPress={() => navigation.navigate('EquipmentFinanceInventory')}>
                        <View style={{...styles.btn, marginTop:hp('4%')}}>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:hp('1%')}}>TRUCK INVENTORY</Text>
                       </View>
                    </TouchableOpacity>
                    <TouchableOpacity  
                    onPress={() => navigation.navigate('')}>
                        <View style={styles.btn}>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:hp('1%'), textAlign:'center'}}>ACCOUNT LOGIN</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity  
                    onPress={() => setPaymentView(true)}>
                        <View style={styles.btn}>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:hp('1%'), textAlign:'center'}}>MAKE PAYMENT</Text>
                        </View>
                    </TouchableOpacity>
                <Text style={{...styles.txt, marginTop:hp('2%')}}>Simple and easy equipment financing.</Text>
                <View style={{flexDirection:'row', alignSelf:'center'}}>
                <Text style={{...styles.txt, color:'#cead02'}}>Click here</Text><Text style={styles.txt}>to apply and start to frow</Text></View>
                <Text style={{...styles.txt, marginBottom:hp('2%')}}>your business today!</Text>
                <TouchableOpacity  
                    onPress={() => navigation.navigate('')}>
                        <View style={{...styles.btn, borderColor:'#cdcdcd'}}>
                            <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:hp('1%'), textAlign:'center'}}>MAKE PAYMENT</Text>
                        </View>
                    </TouchableOpacity>                
                <View style={{alignSelf:'center', width:wp('80%'), bottom:hp('2%'), position:'absolute' }}>
                    <LoveCompass />
                </View>
                <Modal
                animationType='slideInUp'
                isVisible={paymentView}
                onRequestClose={() => setPaymentView(false)}
                onBackdropPress={() => setPaymentView(false)}
                // backdropOpacity={0.9}
                hasBackdrop={true}
                backdropColor='black'
                >
                    <WebView source={{ uri: 'https://mysigmapayments.net/ResponsiveWPEP/#!/Login?DealerId=521' }} style={{ width:wp('100%'),height:hp('100%') ,alignSelf:'center' }} />
                </Modal>

            </ScrollView>
    </KeyboardAwareView>
    </SafeAreaView>
    )
};


EquipmentFinanceScreen.navigationOptions = ({ navigation }) => {
    return{
        header: null,
    }
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        height:'90%',
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent',
        flex:1
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    },
    btn:{
        marginVertical:hp('1.3%'),
    width:'80%',
    alignSelf:'center',
    borderWidth:3, 
    borderColor:'#cead02', 
    borderRadius:10,
    backgroundColor:'white',
    shadowOffset: {
      width: 10,
      height: 6,
    },
    shadowOpacity: 0.30,
    shadowRadius: 12,
    shadowColor:'#484848',
    elevation: 12,
    
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default EquipmentFinanceScreen;