import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity, SafeAreaView, ScrollView,Platform, ImageBackground} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import Arenalogo from '../../components/Arenalogo';


const Cafe = ({ navigation }) => {


    return(
        <SafeAreaView style={{width:'100%', height:'100%'}}>
        <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
            <ScrollView      
                contentContainerStyle={styles.container}           
                automaticallyAdjustContentInsets={true}
                showsVerticalScrollIndicator={false}
                bounces={true}
            >
                <Arenalogo />
                <ImageBackground style={{width:wp('100%'), height:hp('70%'), marginTop:hp('4%')}} resizeMode='cover' source={require('../../img/coffee-bar.jpg')}>
                <View style={styles.overlay}>
                    <Text style={styles.txt}>
                        Compass Cafe offers Lavazza {"\n"}
                        coffee, plenty of relaxing {"\n"}
                        working space, and several{"\n"}
                        breakfast options.{"\n"}
                        {"\n"}
                        Hours: Mon-Sun 7am-7pm
                    </Text>
                </View>
                </ImageBackground>
                </ScrollView>
        </KeyboardAwareView>
        </SafeAreaView>
        )
    };
    
const styles = StyleSheet.create({
    container: {
        width:wp('100%'),
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent',
    },
    btnSpaces:{
        alignSelf:'center',
        backgroundColor:'rgb(41,40,112)',
        borderRadius:20,
        width:'40%',
        alignItems:'center',
        paddingVertical:hp('0.5%')
    },
    input:{
        width:'90%',
        borderWidth:0.4,
        borderRadius:10,
        paddingVertical:hp('0.8%'),
        backgroundColor:'#ebebeb',
        paddingLeft:wp('1%')
    },
    bookTxt:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:wp('10%'),
        color:'white',
        alignSelf:'center'
    },
    overlay:{
        width:'100%',
        height:'100%',
        backgroundColor:'rgba(0,0,0,0.8)',
        justifyContent:'center'
    },
    txt: {
        fontFamily:'Myriad-Pro-Condensed',
        color:'rgb(214,177,150)',
        fontSize:wp('5%'),
        textAlign:'center'
    },
    btn:{
        backgroundColor:'white',
        marginVertical:hp('1%'),
        width:'80%',
        alignSelf:'center',
        borderWidth:3, 
        borderColor:'#da212f', 
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
        width: 10,
        height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#484848',
        elevation: 8,
        
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default Cafe;