import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity, SafeAreaView, ScrollView,Platform, ImageBackground} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import Arenalogo from '../../components/Arenalogo';


const WhiskeyBar = ({ navigation }) => {


    return(
        <SafeAreaView style={{width:'100%', height:'100%'}}>
        <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
            <View      
                style={styles.container}           
            >
                <Arenalogo />
                <ImageBackground style={{width:wp('100%'), height:hp('80%')}} resizeMode='cover' source={require('../../img/whiskey-bar.jpg')} blurRadius={1}>
                <View style={styles.overlay}>
                    <Text style={styles.bookTxt}>
                        Sink into the couch and let {'\n'}
                        loose with the sound of jazz {'\n'}
                        playing in the background {'\n'}
                        while enjoying a craft cocktail {'\n'}
                        in our whiskey bar.{'\n'}
                        {'\n'}
                        Hours: Fri - Sat 5pm-1am
                    </Text>
                    <TouchableOpacity
                    onPress={() => navigation.navigate('FairPlayBook')}
                    style={styles.btnSpaces}
                    >
                            <Text style={styles.txt}>Book Event</Text>
                    </TouchableOpacity>
                </View>
                </ImageBackground>
                </View>
        </KeyboardAwareView>
        </SafeAreaView>
        )
    };
    
const styles = StyleSheet.create({
    container: {
        width:wp('100%'),
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent',
        paddingBottom:hp('5%')
    },
    btnSpaces:{
        alignSelf:'center',
        backgroundColor:'#812d28',
        borderRadius:12,
        width:'40%',
        alignItems:'center',
        paddingVertical:hp('1.5%'),
        marginTop:hp('6%')
    },
    input:{
        width:'90%',
        borderWidth:0.4,
        borderRadius:10,
        paddingVertical:hp('0.8%'),
        backgroundColor:'#ebebeb',
        paddingLeft:wp('1%')
    },
    bookTxt:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:wp('5%'),
        color:'#302a25',
        alignSelf:'center',
        textAlign:'center'
    },
    overlay:{
        width:'100%',
        height:'100%',
        backgroundColor:'rgba(172, 138, 111, 0.8)',
        justifyContent:'center'
    },
    txt: {
        fontFamily:'Myriad-Pro-Condensed',
        color:'white',
        fontSize:wp('5%'),
    },
    btn:{
        backgroundColor:'white',
        marginVertical:hp('1%'),
        width:'80%',
        alignSelf:'center',
        borderWidth:3, 
        borderColor:'#da212f', 
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
        width: 10,
        height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#484848',
        elevation: 8,
        
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default WhiskeyBar;