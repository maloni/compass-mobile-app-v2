import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity, SafeAreaView, ScrollView,Platform} from 'react-native';
import  CTLlogo  from '../../components/CTLlogo';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import LoveCompass from '../../components/LoveCompass';
import Arenalogo from '../../components/Arenalogo';

const ArenaScreen = ({ navigation }) => {

    return(
        <SafeAreaView style={{width:'100%', height:'100%'}}>
        <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
            <ScrollView      
                contentContainerStyle={styles.container}           
                automaticallyAdjustContentInsets={true}
                showsVerticalScrollIndicator={false}
                bounces={true}
            >
                    <Arenalogo />
                    <TouchableOpacity 
                    onPress={() => navigation.navigate('Reservation')}
                    style={{...styles.btn, marginTop:hp('4%')}}
                        >
                                <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:hp('1%')}}>RESERVATION</Text>
                        </TouchableOpacity>
                        <TouchableOpacity  
                        onPress={() => navigation.navigate('ArenaSpaces')}
                        style={styles.btn}
                        >
                                <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:hp('1%'), textAlign:'center'}}>SPACES</Text>
                        </TouchableOpacity>
                        <TouchableOpacity  
                        onPress={() => navigation.navigate('FairPlay')}
                        style={styles.btn}
                        >
                                <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:hp('1%'), textAlign:'center'}}>EVENTS</Text>
                        </TouchableOpacity>
                        <TouchableOpacity  
                        onPress={() => navigation.navigate('Gallery')}
                        style={{...styles.btn, borderColor:'#da212f'}}
                        >
                                <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:hp('1%'), textAlign:'center'}}>GALLERY</Text>
                        </TouchableOpacity>
                        <TouchableOpacity  
                        style={{...styles.btn, borderColor:'#da212f'}}
                        >
                                <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', fontSize:28 , color:'#484848', padding:hp('1%'), textAlign:'center'}}>ABOUT</Text>
                        </TouchableOpacity>
                    <View style={{alignSelf:'center', width:wp('80%'), position:'absolute', bottom:hp('2%')}}>
                        <LoveCompass />
                    </View>

                </ScrollView>
        </KeyboardAwareView>
        </SafeAreaView>
        )
    };
    
const styles = StyleSheet.create({
    container: {
        width:wp('90%'),
        marginHorizontal:'5%',
        height:hp('90%'),
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent',
        flex:1
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:22,
        alignSelf:'center'
    },
    btn:{
        backgroundColor:'white',
        marginVertical:hp('1%'),
        width:'80%',
        alignSelf:'center',
        borderWidth:3, 
        borderColor:'#da212f', 
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
        width: 10,
        height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#484848',
        elevation: 8,
        
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default ArenaScreen;