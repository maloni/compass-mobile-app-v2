import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, TextInput, TouchableOpacity, SafeAreaView, ScrollView,Platform, ImageBackground} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import Arenalogo from '../../components/Arenalogo';


const Field = ({ navigation }) => {


    return(
        <SafeAreaView style={{width:'100%', height:'100%'}}>
        <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
            <View style={styles.container}>
                <Arenalogo />
                <ImageBackground style={{width:wp('100%')}} resizeMode='cover' source={require('../../img/foot.jpg')}>
                <View style={styles.overlay}>
                    <Image style={{width:wp('40%'), height:hp('20%'), alignSelf:'center'}} resizeMode='contain' source={{uri:'https://cdn1.sportngin.com/attachments/logo_graphic/5f89-127096451/CompassUnited_small.png'}} />
                    <Text
                    style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:hp('6%'), color:'white', alignSelf:'center', marginBottom:hp('5%')}}
                    >JOIN US!</Text>
                    <TouchableOpacity
                    style={styles.btnSpaces}
                    >
                            <Text style={styles.txt}>BOOK FIELD TIME</Text>
                    </TouchableOpacity>
                    <TouchableOpacity 
                    style={styles.btnSpaces}
                    >
                            <Text style={styles.txt}>ADULT LEAGUES</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    style={styles.btnSpaces}
                    >
                            <Text style={{...styles.txt, textAlign:'center'}}>JOIN COMPASS UTD{'\n'}TRAVEL SOCCER CLUB</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                    style={styles.btnSpaces}
                    >
                            <Text style={styles.txt}>FREE CLINICS</Text>
                    </TouchableOpacity>
                </View>
                </ImageBackground>
                </View>
        </KeyboardAwareView>
        </SafeAreaView>
        )
    };
    
const styles = StyleSheet.create({
    container: {
        width:wp('100%'),
        marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
        backgroundColor:'transparent',
        paddingBottom:hp('5%')
    },
    btnSpaces:{
        alignSelf:'center',
        backgroundColor:'rgba(41,40,112,0.9)',
        borderRadius:12,
        width:'50%',
        alignItems:'center',
        paddingVertical:hp('1%'),
        marginBottom:hp('3%'),
    },
    input:{
        width:'90%',
        borderWidth:0.4,
        borderRadius:10,
        paddingVertical:hp('0.8%'),
        backgroundColor:'#ebebeb',
        paddingLeft:wp('1%')
    },
    bookTxt:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:wp('10%'),
        color:'white',
        alignSelf:'center'
    },
    overlay:{
        width:'100%',
        height:'100%',
        backgroundColor:'rgba(0,0,0,0.5)',
    },
    txt: {
        fontFamily:'Myriad-Pro-Condensed',
        color:'white',
        fontSize:wp('5%'),
    },
    btn:{
        backgroundColor:'white',
        marginVertical:hp('1%'),
        width:'80%',
        alignSelf:'center',
        borderWidth:3, 
        borderColor:'#da212f', 
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
        width: 10,
        height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#484848',
        elevation: 8,
        
    },
    scrollView: {
        flexGrow:1,
        width:'100%',
        height:'100%'
      },
      
});

export default Field;