import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import CompassInsuranceLogo from '../../components/CompassInsuranceLogo';
import InsuranceGroupButton from '../../components/InsuranceGroupButton';
import { MaterialCommunityIcons, Feather } from '@expo/vector-icons';
import LoveCompass from '../../components/LoveCompass';
import Constants from 'expo-constants';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const InsuranceGroupScreen = ({ navigation }) => {
    
    
    return(
        <View style={styles.container}>
            <CompassInsuranceLogo />
                <Text style={styles.txt}>Need insurance for your equipment,</Text>
                <Text style={styles.txt}>drivers or company?</Text>
                <View style={{flexDirection:'row'}}>
                    <Text style={{...styles.txt, color:'#cead02'}}>Click here</Text>
                    <Text style={styles.txt}> to start a quote today!</Text>
                </View>
                <InsuranceGroupButton 
                title='Auto Liability'
                navigation={() => navigation.navigate('AutoLiability')}
                />
                <InsuranceGroupButton 
                title='Cargo Liability'
                navigation={() => navigation.navigate('CargoLiability')}
                />
                <InsuranceGroupButton 
                title='PD & NTL'
                navigation={() => navigation.navigate('PdNtl')}
                />
                <InsuranceGroupButton 
                title='Occupational Accident'
                navigation={() => navigation.navigate('OccupationalAccident')}
                />
                <View style={{flexDirection:'row', width:'80%', justifyContent:'space-evenly', marginTop:20}}>
                    <TouchableOpacity style={{width:'32%'}}>
                        <View style={styles.blackBtn}>
                            <MaterialCommunityIcons
                            name='email-outline'
                            size={20}
                            color='#eaeaea'
                            />
                            <Text style={styles.blackBtnTxt}>EMAIL US</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'30%'}}>
                        <View style={styles.blackBtn}>
                            <Feather 
                            name='phone-call'
                            size={20}
                            color='#eaeaea'
                            />
                            <Text style={styles.blackBtnTxt}>CALL US</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:'30%'}}>
                        <View style={styles.blackBtn}>
                            <Feather 
                            name='star'
                            size={20}
                            color='#eaeaea'
                            />
                            <Text style={styles.blackBtnTxt}>REVIEW</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View   style={{width:wp('80%'), position:'absolute', bottom:hp('2%')}}>
                    <LoveCompass 
                    nav={() => navigation.navigate('LoveCompassScreen')}
                    />
                </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        width:'90%',
        marginHorizontal:'5%',
        height:'90%',
        flex:1,
        alignItems:'center',
        marginTop: Constants.statusBarHeight,
    },
    txt:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:20,
        color:'#484848'
    },
    blackBtn:{
        backgroundColor:'#484848',
        width:'100%',
        padding:7,
        borderRadius:10,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center'
        
    },
    blackBtnTxt:{
        color:'#eaeaea',
        fontFamily:'Myriad-Pro-Bold-Condensed',
        fontSize:18,
        paddingLeft:5,
        textAlign:'center'
    }
});

export default InsuranceGroupScreen;