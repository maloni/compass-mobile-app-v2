import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, SafeAreaView, ScrollView, Dimensions, TouchableOpacity, Linking, TextInput, KeyboardAvoidingView } from 'react-native';
import CompassInsuranceLogo from '../../components/CompassInsuranceLogo';
import Constants from 'expo-constants';
import InsuranceInput from '../../components/InsuranceInput';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const OccupationalAccident = () => {
    const [companyName, setCompanyName] = useState('');
    const [mcNumber, setMcNumber] = useState('');
    const [state, setState] = useState('');
    const [yearsInBusiness, setYearsInBusiness] = useState('');
    const [contactPerson, setContactPerson] = useState('');
    const [businessPhone, setBusinessPhone] = useState('');
    const [email, setEmail] = useState('');
    const [checkMail, setCheckMail] = useState('');

    var content ="Company Name : " + companyName + "<br/> MC Number : " + mcNumber + "<br/> State : " + state + "<br/> How long in Business : " + yearsInBusiness + "<br/> Contact Person : " + contactPerson + "<br/> Business Phone : " + businessPhone + "<br/> Email : " + email;

    return(
      <SafeAreaView style={{width:'100%', height:'100%'}}>
      <KeyboardAwareView behavior={"padding"} enabled  style={{width:'100%', height:'100%'}} animated={true}>
          <ScrollView      
              contentContainerStyle={styles.container}           
              automaticallyAdjustContentInsets={true}
              showsVerticalScrollIndicator={false}
              bounces={true}
          >
         <CompassInsuranceLogo />
                 <Text style={styles.text}>
                 Truckers Occupational Accident Insurance is customized for motor carriers, owner-operators and contract drivers. This state-of-the-art coverage helps motor carriers contain costs while offering competitive benefits that help attract and retain dependable drivers.
                 </Text>
                 <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:24, color:'#484848', marginBottom:hp('2%')}}>Contact Us to recive a quote</Text>
             <InsuranceInput 
             onChangeText={(Text) => setCompanyName(Text)}
             value={companyName}
             placeholder='Company Name'
             icon='office-building'
             color='#cead02'
             validate={false}
             />
             <InsuranceInput 
             onChangeText={(Text) => setMcNumber(Text)}
             value={mcNumber}
             placeholder='MC Number'
             icon='truck'
             color='#cead02'
             validate={false}
             />
             <InsuranceInput 
             onChangeText={(Text) => setState(Text)}
             value={state}
             placeholder='State'
             icon='flag-variant'
             color='#cead02'
             validate={false}
             />
             <InsuranceInput 
             onChangeText={(Text) => setYearsInBusiness(Text)}
             value={yearsInBusiness}
             placeholder='Years in Business'
             icon='calendar-question'
             color='#cead02'
             validate={false}
             />
             <InsuranceInput 
             onChangeText={(Text) => setContactPerson(Text)}
             value={contactPerson}
             placeholder='Contact Person'
             icon='account-circle'
             color='#cead02'
             validate={false}
             />
             <InsuranceInput 
             onChangeText={(Text) => setBusinessPhone(Text)}
             value={businessPhone}
             placeholder='Business Phone'
             icon='phone'
             color='#cead02'
             validate={false}
             />
            <InsuranceInput 
                onChangeText={(Text) => setEmail(Text)}
                value={email}
                placeholder='Email'
                icon='email'
                color='#cead02'
                validate={false}
                />
             <TouchableOpacity 
                style={{width:wp('50%'), backgroundColor:'#cead02', borderRadius:10, marginTop:20, alignSelf:'center'}}
                onPress={() => Linking.openURL('mailto:contactus@compassholding.net?subject=Apply for Financing from mobile app&body='+content)}
             >
                 <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:wp('6%'), color:'white', padding:wp('2%'), alignSelf:'center'}}>
                   Submit
                 </Text>
             </TouchableOpacity>
             </ScrollView>
             </KeyboardAwareView>
     </SafeAreaView>
 );
}
 


const styles = StyleSheet.create({
  container: {
    width:'90%',
    marginHorizontal:'5%',
    marginTop: Platform.OS === 'ios' ? 2 : Constants.statusBarHeight,
    paddingBottom: wp('7%'),
    alignItems:'center'
},
scrollView: {
     marginHorizontal: 20,
     flex:1
     
     
   },
   text: {
     fontSize: 18,
     fontFamily:'Myriad-Pro-Bold-Condensed',
     color:'#484848',
     marginBottom:20,
   },
});

export default OccupationalAccident;