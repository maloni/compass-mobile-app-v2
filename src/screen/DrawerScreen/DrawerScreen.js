import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, Platform, Linking} from 'react-native';
import DrawerOptions from '../../components/DrawerOptions';
import LoveCompass from '../../components/LoveCompass';
import { PulseIndicator } from 'react-native-indicators';
import * as StoreReview from 'expo-store-review';
import Constants from 'expo-constants';

const DrawerScreen = ({ navigation }) => {
    
    return(
        <View style={{flex:1, marginTop:Constants.statusBarHeight}}>
        <View style={styles.container}>
            <Text style={styles.heading}>More information</Text>
            <LoveCompass 
            nav={() => navigation.navigate('LoveCompassScreen')}
            />
            <View>
                <DrawerOptions title='Contact Us' navigacija={() => navigation.navigate('Contact')}/>
                <DrawerOptions title='Rate the App' navigacija={() => StoreReview.requestReview()}/>
                <DrawerOptions title='FAQs' navigacija={() => navigation.navigate('')}/>
                <DrawerOptions title='Privacy Policy' navigacija={() => navigation.navigate('')}/>
                <DrawerOptions title='Terms of Service' navigacija={() => navigation.navigate('')}/>
            </View>
            <View style={{borderTopWidth:6, borderTopColor:'#eaeaea', paddingTop:50}}>
                <Text style={{alignSelf:'center', color:'#484848', fontSize:16 , fontFamily:'Myriad-Pro-Bold-Condensed'}}>COPYRIGHT 2019 COMPASS MANAGEMENT,INC.</Text>
                <Text style={{alignSelf:'center', color:'#484848', fontSize:16, fontFamily:'Myriad-Pro-Bold-Condensed'}}>VERSION 2.01</Text>
            </View>
        </View>
        
        </View>
    )
};

const styles = StyleSheet.create({
    container:{
        width:'90%',
        height:'80%',
        margin:'5%',
        marginTop:30
    },
    heart:{
        borderTopWidth:6,
        borderTopColor:'#eaeaea',
        flexDirection:'row',
        alignItems:'center',
        padding:0,
        height:'20%',
    },
    heading:{
        fontSize:30,
        color:'#484848',
        marginBottom:10,
        fontFamily:'Myriad-Pro-Bold-Condensed'
    },
    img:{
        width:'30%',
        marginVertical:0,
    }
});

export default DrawerScreen;