import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import LogisticScreen from '../screen/LogisticScreen/LogisticHome'


const LogisticNavigation = createStackNavigator(
    {
        LogisticHome : LogisticScreen
    },
               
    {
        defaultNavigationOptions: {
            header: null
    },
        
      
    }
);


export default createAppContainer(LogisticNavigation);