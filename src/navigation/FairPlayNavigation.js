import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import FairPlayHome from '../screen/FairPlay/FairPlayHome';
import Reservation from '../screen/FairPlay/Reservation';
import FairPlayEvents from '../screen/FairPlay/FairPlayEvents';
import FairPlayBook from '../screen/FairPlay/FairPlayBook';

const FairPlayNavigation = createStackNavigator({
    
        FairPlayHome : FairPlayHome,
        Reservation : Reservation,
        FairPlayEvents : FairPlayEvents,
        FairPlayBook: FairPlayBook,
    },{
        defaultNavigationOptions: {
            header: null
        }
    }
    
);


export default createAppContainer(FairPlayNavigation);