import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import PaymentServiceDriverScreen from '../screen/PaymentServicesScreen/PaymentServiceDriverScreen';
import CPSCompanyNavigation from '../navigation/CPSCompanyNavigation';

import PaymentServicesScreen from '../screen/PaymentServicesScreen/PaymentServicesScreen';

const PaymentServicesNavigation = createStackNavigator(
    {
        PaymentScreen : PaymentServicesScreen,
        PaymentDriver : PaymentServiceDriverScreen,
        PaymentCompany : CPSCompanyNavigation,
        
    },{
        defaultNavigationOptions: {
            header: null
        }
    }
    
);


export default createAppContainer(PaymentServicesNavigation);