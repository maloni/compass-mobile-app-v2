import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import SmartBoardHome from '../screen/SmartBoard/SmartBoardHome'


const SmartBoardNavigation = createStackNavigator(
    {
        SmartBoardHome : SmartBoardHome
    },
               
    {
        defaultNavigationOptions: {
            header: null
    },
        
      
    }
);


export default createAppContainer(SmartBoardNavigation);