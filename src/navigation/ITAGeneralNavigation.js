import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import ItaCarrierServiceHome from '../screen/ItaCustomerServiceScreen/ItaCarrierServiceHome';
import ItaCarrier from '../screen/ItaCustomerServiceScreen/ItaCarrier';
import ItaMembership from '../screen/ItaCustomerServiceScreen/ItaMembership';
import ItaVideos from '../screen/ItaCustomerServiceScreen/ItaVideos';
import ItaMagazine from '../screen/ItaCustomerServiceScreen/ItaMagazine';
import ItaEvenets from '../screen/ItaCustomerServiceScreen/ItaEvents';
import ItaNotifications from '../screen/ItaCustomerServiceScreen/ItaNotifications';

const ITAGeneralNavigation = createStackNavigator(
    {
        ItaHome : ItaCarrierServiceHome,
        ItaCarrier : ItaCarrier,
        ItaMembership : ItaMembership,
        ItaVideos : ItaVideos,
        ItaMagazine : ItaMagazine,
        ItaEvenets : ItaEvenets,
        ItaNotifications : ItaNotifications
    },
               
    {
        defaultNavigationOptions: {
            header: null
    },
        
      
    }
);


export default createAppContainer(ITAGeneralNavigation);