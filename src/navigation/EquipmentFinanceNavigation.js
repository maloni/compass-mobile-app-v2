import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import EquipmentFinanceScreen from '../screen/EquipmentFinanceScreen/EquipmentFinanceScreen'
import EquipmentFinanceInventory from '../screen/EquipmentFinanceScreen/EquipmentFinanceInventory';
import EquipmentFinanceItem from '../screen/EquipmentFinanceScreen/EquipmentFinanceItem'

const EquipmentFinanceNavigation = createStackNavigator(
    {
        EquipmentFinanceHome : EquipmentFinanceScreen,
        EquipmentFinanceInventory: EquipmentFinanceInventory,
        EquipmentFinanceItem: EquipmentFinanceItem
    },
               
    {
        defaultNavigationOptions: {
            header: null
    },
        
      
    }
);


export default createAppContainer(EquipmentFinanceNavigation);