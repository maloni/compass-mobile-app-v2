import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import FuelScreen from '../screen/FuelScreen/FuelScreen'


const FuelNavigation = createStackNavigator(
    {
        FuelScreen : FuelScreen
    },
               
    {
        defaultNavigationOptions: {
            header: null
    },
        
      
    }
);


export default createAppContainer(FuelNavigation);