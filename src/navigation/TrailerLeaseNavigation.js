import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import TrailerLeaseScreen from '../screen/TrailerLeaseScreen/TrailerLeaseScreen';
import TrailerLeaseAbout from '../screen/TrailerLeaseScreen/TrailerLeaseAbout';
import TrailerLeaseInventory from '../screen/TrailerLeaseScreen/TrailerLeaseInventory';
import TrailerLeaseRefer from '../screen/TrailerLeaseScreen/TrailerLeaseRefer';
import TrailerLeaseApply from '../screen/TrailerLeaseScreen/TrailerLeaseApply';
import TrailerLeaseLocations from '../screen/TrailerLeaseScreen/TrailerLeaseLocations';
import TrailerLeaseContact from '../screen/TrailerLeaseScreen/TrailerLeaseContact'

const TrailerLeaseNavigation = createStackNavigator(
    {        
        TrailerLeaseHome : TrailerLeaseScreen,
        About : TrailerLeaseAbout,
        Inventory : TrailerLeaseInventory,
        Refer : TrailerLeaseRefer,
        Apply : TrailerLeaseApply,
        Locations : TrailerLeaseLocations,
        TrailerLeaseContact : TrailerLeaseContact

    },
    {
        defaultNavigationOptions: {
            header: null
    }
}
);


export default createAppContainer(TrailerLeaseNavigation);