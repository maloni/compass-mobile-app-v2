import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import NavigationScreen from '../screen/NavigationScreen/NavigationScreen';
import Locations from '../screen/NavigationScreen/Locations';
import Arena from '../navigation/ArenaNavigation';
import Filter from '../screen/NavigationScreen/Filter';

const NavigationNavigator = createStackNavigator(
    {
        NavigationScreen : NavigationScreen,
        Locations : Locations,
        Arena : Arena,
        Filter : Filter

    },
    {
        defaultNavigationOptions: {
            header: null
    },
        
      
    }
);


export default createAppContainer(NavigationNavigator);