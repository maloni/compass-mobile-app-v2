import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import ItaRegisterScreen from '../screen/ItaCustomerServiceScreen/ItaRegisterScreen';
import ItaLogInHome from '../screen/ItaCustomerServiceScreen/ItaLogInHome';
import ItaLogIn from '../screen/ItaCustomerServiceScreen/ItaLogIn';
import ItaAccountScreen from '../screen/ItaCustomerServiceScreen/ItaAccountScreen';

const ITAnavigation = createStackNavigator(
    {
        ItaLogInHome : ItaLogInHome,
        ItaLogIn : ItaLogIn,
        ItaRegisterScreen : ItaRegisterScreen,
        ItaAccountScreen : ItaAccountScreen
    },
               
    {
        defaultNavigationOptions: {
            header: null
    },
        
      
    }
);


export default createAppContainer(ITAnavigation);