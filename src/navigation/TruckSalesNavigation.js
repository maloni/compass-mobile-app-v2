import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import TruckSalesScreen from '../screen/TruckSalesScreen/TruckSalesScreen';
import TruckRefer from '../screen/TruckSalesScreen/TruckRefer';
import TruckSalesInventory from '../screen/TruckSalesScreen/TruckSalesInventory';
import TruckSalesItem from '../screen/TruckSalesScreen/TruckSalesItem'
import TruckSalesRefer from '../screen/TruckSalesScreen/TruckSalesRefer';
import TruckSalesApply from '../screen/TruckSalesScreen/TruckSalesApply';

const TruckSalesNavigation = createStackNavigator(
    {        
        TruckHome : TruckSalesScreen,
        TruckRefer : TruckRefer,
        TruckSalesInventory : TruckSalesInventory,
        TruckSalesItem : TruckSalesItem,
        TruckSalesRefer : TruckSalesRefer,
        TruckSalesApply : TruckSalesApply
    }, 
    {
        defaultNavigationOptions: {
            header: null
    }
}
);


export default createAppContainer(TruckSalesNavigation);