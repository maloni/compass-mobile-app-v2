import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import PaymentServiceCompanyScreen from '../screen/PaymentServicesScreen/PaymentServiceCompanyScreen';
import CPSCompanyHome from '../screen/PaymentServicesScreen/CPSCompanyHome';
import CompanyCardManagement from '../screen/PaymentServicesScreen/CompanyCardManagement';
import CompanyTransaction from '../screen/PaymentServicesScreen/CompanyTransaction';
import CompanyCashAdvances from '../screen/PaymentServicesScreen/CompanyCashAdvances';
import CompanyChecks from '../screen/PaymentServicesScreen/CompanyChecks';
import CompanyStatements from '../screen/PaymentServicesScreen/CompanyStatements';
import CompanyDrivers from '../screen/PaymentServicesScreen/CompanyDrivers';
import DriverHome from '../screen/PaymentServicesScreen/DriverHome';
import DriverMerchantLocation from '../screen/PaymentServicesScreen/DriverMerchantLoactions'
import DriverCashRequest from '../screen/PaymentServicesScreen/DriverCashRequest';
import DriverFuelReceipts from '../screen/PaymentServicesScreen/DriverFuelReceipts';
import LoveCompassScreen from '../screen/HomeScreen/LoveCompassScreen';
import StoredCards from '../screen/PaymentServicesScreen/StoredCards';

const CPSCompanyNavigation = createStackNavigator(
    {
        PaymentCompany : PaymentServiceCompanyScreen,
        CompanyHome: CPSCompanyHome,
        CardManagement: CompanyCardManagement,
        Transaction : CompanyTransaction,
        CashAdvances : CompanyCashAdvances,
        Checks : CompanyChecks,
        Statements : CompanyStatements,
        Drivers : CompanyDrivers,
        DriverHome : DriverHome,
        DriverMerchantLocation : DriverMerchantLocation,
        DriverCashRequest : DriverCashRequest,
        DriverFuelReceipts : DriverFuelReceipts,
        LoveCompassScreen : LoveCompassScreen,
        StoredCards : StoredCards

    },{
        defaultNavigationOptions: {
            header: null
        }
    }
    
);


export default createAppContainer(CPSCompanyNavigation);