import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import ArenaScreen from '../screen/ArenaScreen/ArenaScreen';
import FairPlayNavigation from '../navigation/FairPlayNavigation'
import ArenaSpaces from '../screen/ArenaScreen/ArenaSpaces';
import Cafe from '../screen/ArenaScreen/Cafe';
import SportsBar from '../screen/ArenaScreen/SportsBar';
import Field from '../screen/ArenaScreen/Field';
import WhiskeyBar from '../screen/ArenaScreen/WhiskeyBar';
import PremierLounge from '../screen/ArenaScreen/PremierLounge';
import Reservation from '../screen/FairPlay/Reservation';
import ArenaGallery from '../screen/ArenaScreen/ArenaGallery';

const ArenaNavigation = createStackNavigator({

        ArenaHome : ArenaScreen,
        FairPlay : FairPlayNavigation,
        ArenaSpaces: ArenaSpaces,
        Cafe : Cafe,
        SportsBar : SportsBar,
        Field : Field,
        WhiskeyBar : WhiskeyBar,
        PremierLounge : PremierLounge,
        Reservation : Reservation,
        Gallery : ArenaGallery


    },{
        defaultNavigationOptions: {
            header: null
        }
    }
    
);


export default createAppContainer(ArenaNavigation);