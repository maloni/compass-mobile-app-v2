import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import InsuranceGroupScreen from '../screen/InsuranceGroupScreen/InsuranceGroupScreen';
import AutoLiability from '../screen/InsuranceGroupScreen/AutoLiability';
import CargoLiability from '../screen/InsuranceGroupScreen/CargoLiability';
import PdNtl from '../screen/InsuranceGroupScreen/PdNtl';
import OccupationalAccident from '../screen/InsuranceGroupScreen/OccupationalAccident';

const InsuranceGroupNavigation = createStackNavigator(
    {
        Home : InsuranceGroupScreen,
        AutoLiability : AutoLiability,
        CargoLiability : CargoLiability,
        PdNtl : PdNtl,
        OccupationalAccident : OccupationalAccident,
    },
               
    {
        defaultNavigationOptions: {
            header: null
        },
      
    }
);


export default createAppContainer(InsuranceGroupNavigation);