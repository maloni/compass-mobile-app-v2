import { createAppContainer } from 'react-navigation'
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import HomeScreenNavigation from '../navigation/HomeScreenNavigation';
import ITAnavigation from '../navigation/ITAnavigation';
import DrawerScreen from '../screen/DrawerScreen/DrawerScreen';
import NavigationScreen from '../screen/NavigationScreen/NavigationScreen';
import NavigationNavigator from '../navigation/NavigationNavigator';
import { View } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import React from 'react'
import {TabBarIconsHome, TabBarIconsRoad, TabBarIconsAccount, TabBarIconsMore} from '../components/TabBarIcons';

const TabNavigator = createMaterialBottomTabNavigator(
    {
        Home: { 
            screen: HomeScreenNavigation,
            navigationOptions:{
                tabBarIcon: () => (
                    <View style={{width:'150%', height:'150%'}}>
                        <TabBarIconsHome
                        />
                    </View>
                )
            }
        },
        Navigation : {
            screen: NavigationNavigator,
            navigationOptions:{
                tabBarIcon: () => (
                    <View style={{width:'150%', height:'150%'}}>
                        <TabBarIconsRoad
                        />
                    </View>
                )
            }
        
        },
        Ita: {
            screen: ITAnavigation,
            navigationOptions:{
                tabBarIcon: () => (
                    <View style={{width:'150%', height:'150%'}}>
                        <TabBarIconsAccount
                        />
                    </View>
                )
            }
        },
        Drawer: {
            screen: DrawerScreen,
            navigationOptions:{
                tabBarIcon: () => (
                    <View style={{width:'150%', height:'150%'}}>
                        <TabBarIconsMore
                        />
                    </View>
                ),
            tabBarOnPress: ({ navigation }) => navigation.openDrawer()
            }},
    },
    {
        initialRouteName: 'Home',
        activeColor:'#000000',
        inactiveColor:'#000000',
        barStyle: { backgroundColor: '#eaeaea', height:60 },
        labeled: false,        
    }
)


export default createAppContainer(TabNavigator);