import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import TruckRentalAndLeasingScreen from '../screen/TruckRentalAndLeasingScreen/TruckRentalAndLeasingScreen';
import TruckRentalAbout from '../screen/TruckRentalAndLeasingScreen/TruckRentalAbout';
import TruckRentalRent from '../screen/TruckRentalAndLeasingScreen/TruckRentalRent';
import TruckRentalRefer from '../screen/TruckRentalAndLeasingScreen/TruckRentalRefer';
import TruckRentalLocations from '../screen/TruckRentalAndLeasingScreen/TruckRentalLoactions';
import TruckRentalLease from '../screen/TruckRentalAndLeasingScreen/TruckRentalLease';

const TruckRentalAndLeasingNavigation = createStackNavigator(
    {        
        TruckRentalHome : TruckRentalAndLeasingScreen,
        TruckRentalAbout : TruckRentalAbout,
        TruckRentalRent : TruckRentalRent,
        TruckRentalLease : TruckRentalLease,
        TruckRentalRefer : TruckRentalRefer,
        TruckRentalLocations : TruckRentalLocations

    }, 
    {
        defaultNavigationOptions: {
            header: null
    }
}
);


export default createAppContainer(TruckRentalAndLeasingNavigation);


