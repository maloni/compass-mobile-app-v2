import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import FundingSolutionsScreen from '../screen/FundingSolutionsScreen/FundingSolutionsScreen';
import CFSCompanyLogIn from '../screen/FundingSolutionsScreen/CFSCompanyLogIn';
import CFSCompanyHome  from "../screen/FundingSolutionsScreen/CFSCompanyHome";
import CFSBroker from "../screen/FundingSolutionsScreen/CFSBroker";
import CFSInvoices from "../screen/FundingSolutionsScreen/CFSInvoices";
import CFSPayments from '../screen/FundingSolutionsScreen/CFSPayments';
import CFSSchedule from '../screen/FundingSolutionsScreen/CFSSchedule';
import CFSNotification from '../screen/FundingSolutionsScreen/CFSNotification';

const CFSNavigation = createStackNavigator(
    {
        CFSHome : FundingSolutionsScreen,
        CFSCompanyLogIn : CFSCompanyLogIn,
        CFSCompanyHome : CFSCompanyHome,
        CFSBroker : CFSBroker,
        CFSInvoices : CFSInvoices,
        CFSPayments : CFSPayments,
        CFSSchedule:CFSSchedule,
        CFSNotification:CFSNotification

    },{
        defaultNavigationOptions: {
            header: null
        }
    }
    
);


export default createAppContainer(CFSNavigation);