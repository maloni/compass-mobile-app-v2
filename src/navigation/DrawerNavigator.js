import { createDrawerNavigator } from 'react-navigation-drawer';
import {createAppContainer} from 'react-navigation';
import BottomTabNavigator from '../navigation/BottomTabNavigator';
import DrawerScreen from '../screen/DrawerScreen/DrawerScreen';
import React from 'react';
import ContactScreen from '../screen/ContactScreen/ContactScreen';
import LoveCompassScreen from '../screen/HomeScreen/LoveCompassScreen';

const DrawerNavigator = createDrawerNavigator(
    {
        TabNavigator: BottomTabNavigator,
        DrawerScreen: DrawerScreen,
        Contact : ContactScreen,
        LoveCompassScreen : LoveCompassScreen

    },
    {
        drawerType: 'slide',
        drawerPosition: 'right',
        drawerWidth: '70%',
        contentComponent : props => <DrawerScreen {...props}/>
    }
)

export default createAppContainer(DrawerNavigator);