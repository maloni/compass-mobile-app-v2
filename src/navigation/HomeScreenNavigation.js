import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import ITAnavigation from '../navigation/ITAnavigation';
import HomeScreen from '../screen/HomeScreen/HomeScreen';
import ArenaScreen from '../screen/ArenaScreen/ArenaScreen';
import PaymentServicesNavigation from './PaymentServicesNavigation';
import ContactScreen from '../screen/ContactScreen/ContactScreen';
import InsuranceGroupNavigation from '../navigation/InsuranceGroupNavigation';
import ITAGeneralNavigation from '../navigation/ITAGeneralNavigation';
import CFSNavigation from '../navigation/CFSNavigation';
import EquipmentFinanceNavigation from '../navigation/EquipmentFinanceNavigation';
import FuelNavigation from './FuelNavigation';
import TrailerLeaseNavigation from '../navigation/TrailerLeaseNavigation';
import TruckSalesNavigation from '../navigation/TruckSalesNavigation';
import TruckRentalAndLeasingNavigation from '../navigation/TruckRentalAndLeasingNavigation';
import LoveCompassScreen from '../screen/HomeScreen/LoveCompassScreen';
import ArenaNavigation from '../navigation/ArenaNavigation';
import LogisticNavigation from '../navigation/LogisticNavigation';
import SmartBoardNavigation from '../navigation/SmartBoardNavigation';

const HomeScreenNavigation = createStackNavigator(
    {
        Home : HomeScreen,
        Arena : ArenaNavigation,
        PaymentServices : PaymentServicesNavigation,
        Contact : ContactScreen,
        InsuranceGroup : InsuranceGroupNavigation,
        ITA : ITAGeneralNavigation,
        ITAnavigation : ITAnavigation,
        CFS : CFSNavigation,
        SmartBoard:SmartBoardNavigation,
        Logistic: LogisticNavigation,
        EquipmentFinance : EquipmentFinanceNavigation,
        Fuel : FuelNavigation,
        TrailerLease : TrailerLeaseNavigation,
        TruckSales : TruckSalesNavigation,
        TruckRentalNavigation : TruckRentalAndLeasingNavigation,
        LoveCompassScreen : LoveCompassScreen
        
    },
               
    {
        defaultNavigationOptions: {
            header: null
        },
      
    }
);


export default createAppContainer(HomeScreenNavigation);