import React,{ useState, useEffect } from 'react';
import * as Font from 'expo-font';
import { AsyncStorage } from 'react-native';

const CompassContext = React.createContext();

export const CompassProvider = ({ children }) => {
    const [itaId, setItaId] = useState('');


    console.log(itaId)
    return <CompassContext.Provider value={{itaId,setItaId}}>
        {children}
    </CompassContext.Provider>
};

export default CompassContext;
