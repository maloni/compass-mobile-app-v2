import axios from 'axios';

export default axios.create({
    baseURL: 'https://test.compasspaymentservices.com/api/v1'
})