import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';


const ItaCarrierButton = ({ navigation, Title }) => {
    return(
            <View style={styles.btnContainer}>
                <TouchableOpacity
                style={{height:'100%'}}
                onPress={navigation}
                >
                    <View style={styles.btn}>
                        <Text style={styles.btnTxt}>
                            {Title}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>  
       
    )
};

const styles = StyleSheet.create({
    btnContainer:{
        width:'80%',
        marginTop:10,
        justifyContent:'center',
        height:'11%'

    },
    btn:{
        borderWidth:2, 
        borderRadius:10, 
        alignItems:'center',
        borderColor:'#20316b',
        height:'100%',
        justifyContent:'center'
    },
    btnTxt:{
        fontSize:25,
        color:'#20316b',
        fontFamily:'Myriad-Pro-Bold-Condensed',
        textAlign:'center',
        textAlignVertical:'center'
    },

});

export default ItaCarrierButton;