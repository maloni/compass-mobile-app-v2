import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { PulseIndicator } from 'react-native-indicators';
import * as Font from 'expo-font';
import { Feather, Octicons } from '@expo/vector-icons';
import Axios from 'axios';
import { FlatList } from 'react-native-gesture-handler';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { BallIndicator } from 'react-native-indicators';
import { Linking } from 'expo';

const CPSDrivers = ({ cpsUserOK }) =>{
    
    const[active, setActive] = useState(true);
    const[stil, setStil] = useState({boja1:'#49a43a', debljina1:5, boja2:'#e5e5e6', debljina2:3});

    return(
        <View style={{width:wp('90%'), height:hp('100%'), alignSelf:'center'}}>
            <View style={{flexDirection:'row', width:'100%'}}> 
            <View style={{flex:1, flexDirection:'row', marginTop:15, width:'100%'}}>
                <TouchableOpacity
                onPress={() => {setActive(true), setStil({boja1:'#49a43a', debljina1:5, boja2:'#e5e5e6', debljina2:3})}}
                style={{width:'50%', borderBottomWidth:stil.debljina1, borderBottomColor:stil.boja1}}
                >
                    <View style={{width:'100%'}}>
                        <Text style={styles.txtFont}>ACTIVE</Text>    
                    </View>                
                </TouchableOpacity>
                <TouchableOpacity
                onPress={() => {setActive(false), setStil({boja1:'#e5e5e6', debljina1:3, boja2:'#49a43a', debljina2:5})}}
                style={{width:'50%', borderBottomColor:stil.boja2, borderBottomWidth:stil.debljina2}}
                >
                    <View style={{width:'100%'}}>
                        <Text style={styles.txtFont}>TERMINATED</Text>    
                    </View>                
                </TouchableOpacity>
            </View>
        </View>
        
        {active
        ? <CPSDriversActive 
        cpsUserOK={cpsUserOK}
        />
        : <CpsDriversTerminated 
        cpsUserOK={cpsUserOK}
        />
        }
        </View>
    )
}

const CPSDriversActive = ({ cardNumber, driverName, cpsUserOK }) => {
    const [visible, setVisible] = useState(false)
    const [data, setData] = useState()

    useEffect(() => {
        Axios.get('https://apps.compassaws.net/api/card/companyDrivers/'+cpsUserOK)
        .then(res => {
            setData(res.data.result)
            setVisible(true)
        })
        .catch(err =>{
            console.log(err)
        })
    }, [])

    console.log(data)

    return(<View>
        {visible
        ?<FlatList
            showsVerticalScrollIndicator={false}
            data={data}
            keyExtractor={(item) => item._id}
            renderItem={({ item }) => {
                if(item.active == true){
                return(
                    <View style={{borderBottomWidth:5, borderBottomColor:'#CCCCCC', marginVertical:12, paddingBottom:12, flexDirection:'row', width:'100%', justifyContent:'space-between'}}>
                        <View style={{width:'65%'}}>
                            <View style={{flexDirection:'row'}}><Text style={styles.txt}>Card Number :</Text><Text style={styles.infoTxt}></Text>xxxx xx10 0091</View>
                            <View style={{flexDirection:'row'}}><Text style={styles.txt}>Driver :</Text><Text style={styles.infoTxt}> {item.name}</Text></View>
                            <View style={{flexDirection:'row'}}><Text style={styles.txt}>UnitNumber :</Text><Text style={styles.infoTxt}>111</Text></View>
                            <View style={{flexDirection:'row'}}><Text style={{...styles.txt, marginTop:hp('1%')}}>Hire Date :</Text><Text style={{...styles.infoTxt, marginTop:hp('1%')}}>{item.hireDate}</Text></View>
                            <View style={{flexDirection:'row'}}><Text style={styles.txt}>DL#  :</Text><Text style={styles.infoTxt}> </Text></View>
                            <View style={{flexDirection:'row'}}><Text style={styles.txt}>CDL Holder Since  :</Text><Text style={styles.infoTxt}> </Text></View>
                            <View style={{flexDirection:'row'}}><Text style={styles.txt}>Medical Card Expiration Date :</Text><Text style={styles.infoTxt}> </Text></View>
                        </View>
                        <View style={{marginLeft:10, width:'35%'}}>
                            <View style={{...styles.dugme, backgroundColor:'#484848', width:'80%'}}>
                                <TouchableOpacity 
                                onPress={() => Linking.openURL(`tel: ${item.phoneNumber}`)}
                                style={{flexDirection:'row'}}>
                                    <Feather name='phone-call' size={25} color='white' style={{padding:5}} /><Text  numberOfLines={1} style={{fontFamily:'Myriad-Pro-Bold-Condensed',color:'white', fontSize:19, padding:6, alignSelf:'center', marginHorizontal:8, paddingRight:10}}>CALL</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{...styles.dugme, backgroundColor:'#484848', width:'80%'}}>
                                <TouchableOpacity 
                                onPress={() => Linking.openURL(`mailto: ${item.email}`)}
                                style={{flexDirection:'row'}}>
                                    <Octicons name='mail' size={25} color='white' style={{padding:5}} /><Text numberOfLines={1} style={{fontFamily:'Myriad-Pro-Bold-Condensed',color:'white', fontSize:19,padding:6, alignSelf:'center',marginHorizontal:8,paddingRight:10}}>EMAIL</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                 )}
                 else null
                }}
            />
            :<View style={{alignItems:'center', marginTop:hp('20%'), position:'absolute', alignSelf:'center'}}>
                <BallIndicator size={50} color='#49a43a'/>
            </View> 
            }
            </View>
    )
}

const CpsDriversTerminated = ({ cpsUserOK }) =>{

    const [visible, setVisible] = useState(false)
    const [data, setData] = useState()

    useEffect(() => {
        Axios.get('https://apps.compassaws.net/api/card/companyDrivers/'+cpsUserOK)
        .then(res => {
            setData(res.data.result)
            setVisible(true)
        })
        .catch(err =>{
            console.log(err)
        })
    }, [])

    return(<View>
        {visible
        ?<FlatList
            showsVerticalScrollIndicator={false}
            data={data}
            keyExtractor={(item) => item._id}
            renderItem={({ item }) => {
                if(item.active == false){
                return(
                    <View style={{borderBottomWidth:5, borderBottomColor:'#CCCCCC', marginVertical:12, paddingBottom:12, flexDirection:'row', width:'100%', justifyContent:'space-between'}}>
                        <View style={{width:'65%'}}>
                            <View style={{flexDirection:'row'}}><Text style={styles.txt}>Card Number :</Text><Text style={styles.infoTxt}> xxxx xx10 0091</Text></View>
                            <View style={{flexDirection:'row'}}><Text style={styles.txt}>Driver :</Text><Text style={styles.infoTxt}> {item.name}</Text></View>
                            <View style={{flexDirection:'row'}}><Text style={styles.txt}>UnitNumber :</Text><Text style={styles.infoTxt}>111</Text></View>
                            <View style={{flexDirection:'row'}}><Text style={{...styles.txt, marginTop:hp('1%')}}>Hire Date :</Text><Text style={{...styles.infoTxt, marginTop:hp('1%')}}>{item.hireDate}</Text></View>
                            <View style={{flexDirection:'row'}}><Text style={styles.txt}>DL#  :</Text><Text style={styles.infoTxt}> </Text></View>
                            <View style={{flexDirection:'row'}}><Text style={styles.txt}>CDL Holder Since  :</Text><Text style={styles.infoTxt}>{item.cdlIssued} </Text></View>
                            <View style={{flexDirection:'row'}}><Text style={styles.txt}>Medical Card Expiration Date :</Text><Text style={styles.infoTxt}>{item.medicalExpiration}</Text></View>
                        </View>
                        <View style={{marginLeft:10, width:'35%'}}>
                            <View style={{...styles.dugme, backgroundColor:'#484848', width:'80%'}}>
                                <TouchableOpacity 
                                onPress={() => Linking.openURL(`tel: ${item.phoneNumber}`)}
                                style={{flexDirection:'row'}}>
                                    <Feather name='phone-call' size={25} color='white' style={{padding:5}} /><Text  numberOfLines={1} style={{fontFamily:'Myriad-Pro-Bold-Condensed',color:'white', fontSize:19, padding:6, alignSelf:'center', marginHorizontal:8, paddingRight:10}}>CALL</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{...styles.dugme, backgroundColor:'#484848', width:'80%'}}>
                                <TouchableOpacity 
                                onPress={() => Linking.openURL(`mailto: ${item.email}`)}
                                style={{flexDirection:'row'}}>
                                    <Octicons name='mail' size={25} color='white' style={{padding:5}} /><Text numberOfLines={1} style={{fontFamily:'Myriad-Pro-Bold-Condensed',color:'white', fontSize:19,padding:6, alignSelf:'center',marginHorizontal:8,paddingRight:10}}>EMAIL</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                 )}
                 else null
                }}
            />
            :<View style={{alignItems:'center', marginTop:hp('20%'), position:'absolute', alignSelf:'center'}}>
                <BallIndicator size={50} color='#49a43a'/>
            </View> 
            }
            </View>
       )
    }

const styles = StyleSheet.create({
    infoTxt:{
        fontSize:wp('3.5%'), 
        alignSelf:'center',
        color:'#484848'
    },
    txtFont:{
        color:'#49a43a',
        fontSize:28, 
        alignSelf:'center',
        fontFamily:'Myriad-Pro-Bold-Condensed',
        paddingBottom:12
    },
    txtViewStyle:{
        flexDirection:'row',
        
    },
    txtStyle:{
        fontSize:22,
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848'
    },
    btn:{
        width:'45%',
        alignSelf:'center',
        backgroundColor:'#49a43a',
        borderRadius:10,
        marginTop:7
    },
    txt:{
        fontSize:wp('4%'),
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848'
    },
    txt1:{
        fontSize:17,
        color:'#484848'
    },
    dugme:{
        width:'80%',
        borderRadius:5,
        marginVertical:5,
    }
});

export default CPSDrivers;