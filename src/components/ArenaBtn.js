import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import  Entypo  from '@expo/vector-icons/Entypo'

const ArenaBtn = ({ title, navigacija }) => {
    return(
        <View style={styles.options}>
            <TouchableOpacity
            onPress={navigacija}
            >
                <View style={{flexDirection:'row', alignItems:'center'}}>
                    <Text style={styles.txtOptions}>{title}</Text>
                        <View>
                            <Entypo 
                            name='chevron-right'
                            color='#e32b35'
                            size={50}
                            />
                        </View>
                </View>
            </TouchableOpacity>
        </View>
    )
};

const styles = StyleSheet.create({
    options:{
        width:'100%',
        borderBottomColor:'#e09195',
        borderBottomWidth:3,
    },
    txtOptions:{
        textAlignVertical:'center',
        fontSize:26,
        color:'#4d4d4d',
        width:'90%',
        fontFamily:'Myriad-Pro-Bold-Condensed',
    }
});

export default ArenaBtn;
