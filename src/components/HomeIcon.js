import React from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';

const HomeIcon = ({ img, loading }) => {


    
    return(
            <Image 
                style={styles.image}
                source={img}
                onLoadEnd={() =>loading}
            />
    )
};

const styles = StyleSheet.create({
    image: {
        resizeMode:'contain',
        maxWidth:'80%',
        maxHeight:'80%',
        flex:1,
        alignSelf:'center'
    },});

export default HomeIcon;