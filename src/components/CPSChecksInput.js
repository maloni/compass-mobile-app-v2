import React from 'react';
import { Text, View, StyleSheet, TextInput } from 'react-native';

const CPSChecksInput = ({naslov, vrednost, postaviVrednost}) => {
    return(
        <View style={styles.container}>
            <Text style={styles.txt}>{naslov}</Text>
            <TextInput 
                value={vrednost}
                onChangeText={postaviVrednost}
                style={{width:'80%'}}  
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        alignItems:'center',
        width:'90%', 
        height:50, 
        borderColor:'white', 
        borderWidth:3,
        marginVertical:10,
        borderRadius:5,
        backgroundColor:'white',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.57,
        shadowRadius: 16,
        shadowColor:'green',

        elevation: 12,
        borderColor:'#d8d8d8',
        borderWidth:0.2,    
        },
        txt:{
            fontFamily:'Myriad-Pro-Bold-Condensed',
            fontSize:20,
            color:'#8a8a8a',
            paddingLeft:10
        }
});

export default CPSChecksInput;