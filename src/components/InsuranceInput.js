import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, TextInput, Keyboard } from 'react-native';
import {MaterialCommunityIcons} from '@expo/vector-icons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


const InsuranceInput = ({ placeholder, value, onChangeText, icon, color}) => {

    return(
        <View style={{...styles.container, borderColor:color}}>
            <MaterialCommunityIcons 
            name={icon}
            size={25}
            color={color}
            />
            <TextInput 
            placeholder={placeholder}
            onEndEditing={() => Keyboard.dismiss()}
            value={value}
            onChangeText={onChangeText}
            style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:hp('2.2%'), paddingLeft:5, padding:wp('1%'), width:'100%', height:hp('4%')}}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        width:'90%',
        borderRadius:10,
        borderBottomWidth:1,
        padding:5,
        flexDirection:'row',
        marginVertical:'1%',
        alignItems:'center',
        alignSelf:'center'
    }
});

export default InsuranceInput;