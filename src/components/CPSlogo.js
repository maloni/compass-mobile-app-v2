import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';


const CPSlogo = () => {
    return(
        <View style={{marginTop:Constants.statusBarHeight,paddingTop:0, height:hp('13%'), alignSelf:'center', width:wp('90%')}}>
            <Image style={{width:'100%', height:'100%'}} resizeMode='contain' source={require('../../src/img/CPSlogo.png')} />
        </View>
    )
};

const styles = StyleSheet.create({

});

export default CPSlogo;