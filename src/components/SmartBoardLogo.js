import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


const SBlogo = () => {
    return(
        <View style={{height:hp('20%'), alignSelf:'center', width:wp('80%')}}>
            <Image style={{width:'100%', height:'100%'}} resizeMode='contain' source={require('../../src/img/SBlogo.png')} />
        </View>
    )
};

const styles = StyleSheet.create({

});

export default SBlogo;