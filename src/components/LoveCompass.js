import React from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, Platform, Share} from 'react-native';
import { Entypo} from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';
import { heightPercentageToDP } from 'react-native-responsive-screen';


const LoveCompass = ({visina, nav}) => {

    const shareThis = (title, message, url) => {
        Share.share({
            title: title,
            message: title + ': ' + message + ' ' +url,
            url : url
        }, {
            dialogTitle: 'Share ' + title
        });
    }    

    
    return(
        <View style={{...styles.Container}}>
            <TouchableOpacity 
            style={{width:'100%', flexDirection:'row', alignItems:'center',height:'100%'}}
            onPress={() => shareThis('Compass Holding App', 'Hey, check out this new safety app incorporated by Compass Holding!', Platform =='iOS' ? 'https://apps.apple.com/us/app/compass-holding/id1181587901'  : 'https://play.google.com/store/apps/details?id=com.compassHolding' )}
            >
            <View style={{flexDirection:'row', alignItems:'center',height:'100%',width:'80%'}}>
                <AntDesign  name='hearto' size={60} color='#ed1944'/>
                <View style={{marginLeft:'5%'}}>
                    <Text style={{fontSize:19,color:'#484848', fontFamily:'Myriad-Pro-Bold-Condensed'}}>LOVE COMPASS?</Text>
                    <Text style={{fontSize:18,color:'#484848', fontFamily:'Myriad-Pro-Bold-Condensed'}}>Share with friends.</Text>
                </View>
            </View>
            <Entypo 
                name='chevron-right'
                color='#484848'
                size={60}
                />
            </TouchableOpacity>
        </View>
    )
};

const styles = StyleSheet.create({
    Container:{
        alignItems:'center',
        padding:0,
        alignSelf:'center',
        position:'absolute',
        bottom:5,
    },
    img:{
        width:'30%',
        marginVertical:0,
    }
});

export default LoveCompass;