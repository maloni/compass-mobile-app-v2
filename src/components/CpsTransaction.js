import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { BallIndicator } from 'react-native-indicators';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { FlatList } from 'react-native-gesture-handler';
import moment from 'moment';
import Axios from 'axios';


const CpsTransaction = ({ dateFrom, dateTo, cpsUserOK, transaction, transactionVisible, indicator }) =>{

    const[active, setActive] = useState(true);
    const[stil, setStil] = useState({boja1:'#49a43a', debljina1:5, boja2:'#e5e5e6', debljina2:3});
    const[result, setResult] = useState([])
    const[amount, setAmount] = useState()
    const[amountVisible, setAmountVisible] = useState(false)
    const[amountTransaction, setAmountTransaction] = useState()
    const[gallons, setGallons] = useState()
    const[gallonsVisible, setGallonsVisible] = useState(false)
    const[cash, setCash] = useState()
    const[cashVisible,setCashVisible] = useState(false)

    const summary = () =>{
        var filter = {carrierId:cpsUserOK,tdate:{ $gte:dateFrom, $lte:dateTo}}
        var options ={fields:"fuel cash info"}

        let query = "filter=" + encodeURIComponent(JSON.stringify(filter))+"&options="+encodeURIComponent(JSON.stringify(options));
        Axios.post('https://apps.compassaws.net/api/card/getTransactions', query)
        .then(res => {
            
            // console.log(res.data.result)
            setResult(res.data.result)
            sumFuel(res.data.result)
            sumGalons(res.data.result)
            setAmountTransaction(res.data.result.length)
            sumCash(res.data.result)

        })
        .catch(err => {
            console.log(err)
        })
    }

    // const saberi = (niz) => {
    //     for(let i=0; i<niz.length; i++){
    //         let total=niz[i].fuel.amount
    //         console.log(total + niz[i].fuel.amount++)
    //     }
    // }
  
    const sumFuel = (niz) => {
        const sum = async () => {
        let sum = 0
        for(var i = 0; i<niz.length; i++){
           sum = sum + niz[i].fuel.amount
       }
       let sumTotal = await sum;       
       setAmount(sumTotal)
       setAmountVisible(true)
    }
    sum()
    }

    const sumGalons = (niz)=> {
        const sum = async () => {
            let sum = 0
            for(var i = 0; i<niz.length; i++){
               sum = sum + niz[i].fuel.quantity
           }
           let sumTotal = await sum;       
           setGallons(sumTotal)
           setGallonsVisible(true)
        }
        sum()
        }

    const sumCash = (niz) => {
        const sum = async () => {
            let sum = 0
            for(var i = 0; i<niz.length; i++){
               sum = sum + niz[i].cash
           }
           let sumTotal = await sum;       
           setCash(sumTotal)
           setCashVisible(true)
        }
        sum()
        }
    console.log(cash)
    return(
        <View>
            <View style={{flexDirection:'row', width:wp('90%')}}> 
            <View style={{flex:1, flexDirection:'row', marginTop:15}}>
                <TouchableOpacity
                onPress={() => {setActive(true), setStil({boja1:'#49a43a', debljina1:5, boja2:'#e5e5e6', debljina2:3})}}
                style={{width:'50%', borderBottomWidth:stil.debljina1, borderBottomColor:stil.boja1}}
                >
                    <View >
                        <Text style={styles.txtFont}>Transaction</Text>    
                    </View>                
                </TouchableOpacity>
                <TouchableOpacity
                onPress={() => {setActive(false), setStil({boja1:'#e5e5e6', debljina1:3, boja2:'#49a43a', debljina2:5}), summary()}}
                style={{width:'50%', borderBottomColor:stil.boja2, borderBottomWidth:stil.debljina2}}
                >
                    <View>
                        <Text style={styles.txtFont}>Summary</Text>    
                    </View>                
                </TouchableOpacity>
            </View>
        </View>
        
        {active
        ? <CpsTransactionBar 
        dateFrom={dateFrom}
        dateTo={dateTo}
        cpsUserOK={cpsUserOK}
        transactionVisible={transactionVisible}
        transaction={transaction}
        indicator={indicator}
        />
        : <CpsSummaryBar 
        amount={amount}
        amountVisible={amountVisible}
        amountTransaction={amountTransaction}
        gallons={gallons}
        gallonsVisible={gallonsVisible}
        cash={cash}
        cashVisible={cashVisible}
        />
        }
        </View>
    )
}

const CpsTransactionBar = ({ dateFrom, dateTo, cpsUserOK, transaction, transactionVisible, indicator }) => {

    // useEffect(() => {
    //     const info = "carrierId="+cpsUserOK+"&startDate="+dateFrom+"&endDate="+dateTo;
    //     Axios.post('https://apps.compassaws.net/api/card/getNTransactions', info)
    //     .then(res =>{
    //         setTransaction(res.data.result)
    //         console.log(res)
    //     })
    //     .catch(err =>{
    //         console.log(err)
    //     })
    // })


    return(
        <View>{!transactionVisible && !indicator
            ?<Text style={{fontSize:wp('5%'), marginTop:hp('1%'), alignSelf:'center', fontFamily:'Myriad-Pro-Bold-Condensed', color:'#484848'}}>
                Please enter Start Date and End date
            </Text>
            :null}
            {transactionVisible
            ?<FlatList
            showsVerticalScrollIndicator={false}
            data={transaction}
            keyExtractor={item => item._id}
            renderItem={({ item }) => {
                return(
                    <View style={{borderBottomWidth:5, borderBottomColor:'#CCCCCC', marginVertical:12, paddingBottom:12}}>
                        <View style={{flexDirection:'row', alignItems:'center'}}><Text style={styles.txt}>Date :</Text><Text style={styles.podatci}> {moment(item.createdOn).format('MMMM Do YYYY, h:mm:ss a')}</Text></View>
                <View style={{flexDirection:'row'}}><Text style={styles.txt}>Card Number :</Text><Text style={styles.podatci}>**** **** **{item.cardNumber.substring(8,16)}</Text></View>
                        <View style={{flexDirection:'row'}}><Text style={styles.txt}>Driver :</Text><Text style={styles.podatci}> Kristina Trnovska</Text></View>
                    <View style={{flexDirection:'row'}}><Text style={styles.txt}>UnitNumber :</Text><Text style={styles.podatci}>{item.info.unit}</Text></View>
                <View style={{flexDirection:'row'}}><Text style={{...styles.txt, marginVertical:10}}>Loaction :</Text><Text style={styles.podatci}> {item.merchant.eftAccount.accountName}, {item.merchant.address.street1}, {item.merchant.address.city}</Text></View>
                        <View style={{flexDirection:'row'}}><Text style={styles.txt}> Transaction Details :</Text><Text></Text></View>
                            <View style={{flexDirection:'row', justifyContent:'space-between', marginVertical:6}}>
                                <Text style={styles.txt1}>{item.fuel.quantity/100} gal</Text>
                                <Text style={styles.txt1}>PPG: $ {item.fuel.ppg/1000}</Text>
                                <Text style={styles.txt1}>Total: ${item.fuel.amount/100}</Text>
                            </View>
                        <Text style={styles.txt}>Additional Transaction Details:</Text>
                            <View style={{flexDirection:'row',justifyContent:'space-between', marginTop:6}}>
                                <Text style={styles.txt1}>Cash: $100</Text>
                                <Text style={styles.txt1}>Oil: ${item.oil}</Text>
                                <Text style={styles.txt1}>Other: ${item.other}</Text>
                            </View>
                    </View>
                )
            }}
            />
        :null}
        {indicator
        ?<View style={{alignItems:'center', marginTop:hp('20%'), position:'absolute', alignSelf:'center'}}>
            <BallIndicator size={50} color='#49a43a'/>
        </View>
        :null
        }
        </View>
    )
}

const CpsSummaryBar = ({ amount, amountVisible, amountTransaction, gallons, gallonsVisible, cash, cashVisible }) =>{

    return(
    <View>
    {amountVisible & gallonsVisible & cashVisible
        ?<View  style={{borderBottomWidth:5, borderBottomColor:'#CCCCCC', marginVertical:12, paddingBottom:12}} >
            <View style={{flexDirection:'row'}}><Text style={styles.txt}>UnitNumber :</Text><Text style={styles.podatci}> 104</Text></View>
            <View style={{flexDirection:'row'}}><Text style={styles.txt}>Number of Transaction :</Text><Text style={styles.podatci}> {amountTransaction}</Text></View>
            <View style={{flexDirection:'row'}}><Text style={styles.txt}>Fuel Amount :</Text><Text style={styles.podatci}> $ {amount/100}</Text></View>
            <View style={{flexDirection:'row'}}><Text style={styles.txt}>Gallons :</Text><Text style={styles.podatci}> {gallons/100}</Text></View>
            <View style={{flexDirection:'row'}}><Text style={styles.txt}>Cash Amount :</Text><Text style={styles.podatci}> $ {cash/100}</Text></View>
        </View>
        :<View style={{alignItems:'center', marginTop:hp('20%'), position:'absolute', alignSelf:'center'}}>
            <BallIndicator size={50} color='#49a43a'/>
        </View>
    }
    </View>
    )
}

const styles = StyleSheet.create({
    txtFont:{
        color:'#49a43a',
        fontSize:28, 
        alignSelf:'center',
        fontFamily:'Myriad-Pro-Bold-Condensed',
        paddingBottom:12
    },
    txtViewStyle:{
        flexDirection:'row',
        
    },
    txtStyle:{
        fontSize:22,
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848'
    },
    btn:{
        width:'45%',
        alignSelf:'center',
        backgroundColor:'#49a43a',
        borderRadius:10,
        marginTop:7
    },
    txt:{
        fontSize:wp('4%'),
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848'
    },
    txt1:{
        fontSize:wp('4%'), 
        color:'#484848',
        fontFamily:'Myriad-Pro-Condensed'

    },
    podatci:{
        fontSize:wp('3.5%'), 
        alignSelf:'center',
        color:'#484848'
    }
});

export default CpsTransaction;