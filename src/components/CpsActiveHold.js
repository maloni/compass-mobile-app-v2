import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, ScrollView, ToastAndroid } from 'react-native';
import * as Font from 'expo-font';
import { PulseIndicator } from 'react-native-indicators';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Axios from 'axios';



const CpsActive = ({ allCards, getCards, search }) => {

    
    const onHold = (act,card) => {

        let active=!act;
        let onHold=act;
        let info = "card="+card+"&onhold="+onHold+"&active="+active;

        console.log(info)
        Axios.post('https://apps.compassaws.net/api/card/changeCardState', info)
        .then(res => {
            // console.log(res)
            ToastAndroid.show('Card On Hold', ToastAndroid.LONG)
            getCards()
        })
        .catch(err =>{
            console.log(err)
        })
    }


    return(
        <View> 
        { search ===''
        ? allCards.map((item, index) => {
            if (item.active === true)
            return (
            <View key={item._id} style={{borderBottomWidth:3, borderBottomColor:'#CCCCCC', marginVertical:12, paddingBottom:12}}>
                    <View style={styles.txtViewStyle}><Text style={styles.txtStyle}>Card Number:  </Text><Text style={styles.podatci}>{item.cardNumber}</Text></View>
                    <View style={styles.txtViewStyle}><Text style={styles.txtStyle}>Driver:  </Text><Text style={styles.podatci}>{item.cardHolder}</Text></View>
                    <View style={styles.txtViewStyle}><Text style={styles.txtStyle}>Unit Number:  </Text><Text style={styles.podatci}>{item.infoCapture.unit}</Text></View>
                    <View style={styles.btn}>
                        <TouchableOpacity
                        onPress={() => onHold(item.active ,item.cardNumber, getCards)}
                        >
                            <Text style={{color:'white', fontSize:wp('5%'), fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', padding:8}}>ON HOLD</Text>
                        </TouchableOpacity>
                    </View>
            </View>)
        })
        :allCards.filter(item => {
            if(item.infoCapture.unit == search){
                return item }
        }).map((item, index) => {
        if (item.active === true)
        return (
        <View key={item._id} style={{borderBottomWidth:3, borderBottomColor:'#CCCCCC', marginVertical:12, paddingBottom:12}}>
                <View style={styles.txtViewStyle}><Text style={styles.txtStyle}>Card Number:  </Text><Text style={styles.podatci}>{item.cardNumber}</Text></View>
                <View style={styles.txtViewStyle}><Text style={styles.txtStyle}>Driver:  </Text><Text style={styles.podatci}>{item.cardHolder}</Text></View>
                <View style={styles.txtViewStyle}><Text style={styles.txtStyle}>Unit Number:  </Text><Text style={styles.podatci}>{item.infoCapture.unit}</Text></View>
                <View style={styles.btn}>
                    <TouchableOpacity
                    onPress={() => onHold(item.active ,item.cardNumber, getCards)}
                    >
                        <Text style={{color:'white', fontSize:wp('5%'), fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', padding:8}}>ON HOLD</Text>
                    </TouchableOpacity>
                </View>
        </View>)
    })
    } 
    </View>
    )
};

const CpsHold = ({ allCards, getCards, search }) => {

    const onHold = (act,card) => {

        let active=!act;
        let onHold=act;
        let info = "card="+card+"&onhold="+onHold+"&active="+active;

        console.log(info)
        Axios.post('https://apps.compassaws.net/api/card/changeCardState', info)
        .then(res => {
            // console.log(res)
            ToastAndroid.show('Card Activated', ToastAndroid.LONG)
            getCards()
        })
        .catch(err =>{
            console.log(err)
        })
    }
    return(
        <View>
        { search ===''
        ? allCards.map((item, index) => {
            if (item.active === false)
            return (
            <View key={item._id} style={{borderBottomWidth:3, borderBottomColor:'#CCCCCC', marginVertical:12, paddingBottom:12}}>
                <View style={styles.txtViewStyle}><Text style={styles.txtStyle}>Card Number:  </Text><Text style={styles.podatci}>{item.cardNumber}</Text></View>
                <View style={styles.txtViewStyle}><Text style={styles.txtStyle}>Driver:  </Text><Text style={styles.podatci}>{item.cardHolder}</Text></View>
                <View style={styles.txtViewStyle}><Text style={styles.txtStyle}>Unit Number:  </Text><Text style={styles.podatci}>{item.infoCapture.unit}</Text></View>
                <View style={styles.btn}>
                    <TouchableOpacity
                    onPress={() =>onHold(item.active ,item.cardNumber, getCards)}
                    >
                        <Text style={{color:'white', fontSize:wp('5%'), fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', padding:8}}>ACTIVE</Text>
                    </TouchableOpacity>
                </View>
            </View>
            )
        })
        :allCards.filter(item => {
            if(item.infoCapture.unit == search){
                return item }
        }).map((item, index) => {
        if (item.active === false)
        return (
            <View key={item._id} style={{borderBottomWidth:5, borderBottomColor:'#CCCCCC', marginVertical:12, paddingBottom:12}}>
                <View style={styles.txtViewStyle}><Text style={styles.txtStyle}>Card Number:  </Text><Text style={styles.podatci}>{item.cardNumber}</Text></View>
                <View style={styles.txtViewStyle}><Text style={styles.txtStyle}>Driver:  </Text><Text style={styles.podatci}>{item.cardHolder}</Text></View>
                <View style={styles.txtViewStyle}><Text style={styles.txtStyle}>Unit Number:  </Text><Text style={styles.podatci}>{item.infoCapture.unit}</Text></View>
                <View style={styles.btn}>
                    <TouchableOpacity
                    onPress={() =>onHold(item.active ,item.cardNumber, getCards)}
                    >
                        <Text style={{color:'white', fontSize:wp('5%'), fontFamily:'Myriad-Pro-Bold-Condensed', alignSelf:'center', padding:8}}>ACTIVE</Text>
                    </TouchableOpacity>
                </View>
            </View>
            )    } )}
        </View>
        )}

const CpsActiveHold = ({allCards, getCards, search}) => {


    const[active, setActive] = useState(true);
    const[stil, setStil] = useState({boja1:'#49a43a', debljina1:5, boja2:'#e5e5e6', debljina2:3});

    return(         
    <ScrollView style={{marginBottom:hp('7%'),width:'90%'}}>
        <View style={{flexDirection:'row'}}> 
            <View style={{flex:1, flexDirection:'row', marginTop:15}}>
                <TouchableOpacity
                onPress={() => {setActive(true), setStil({boja1:'#49a43a', debljina1:5, boja2:'#e5e5e6', debljina2:3})}}
                style={{width:'50%', borderBottomWidth:stil.debljina1, borderBottomColor:stil.boja1}}
                >
                    <View >
                        <Text style={styles.txtFont}>ACTIVE</Text>    
                    </View>                
                </TouchableOpacity>
                <TouchableOpacity
                onPress={() => {setActive(false), setStil({boja1:'#e5e5e6', debljina1:3, boja2:'#49a43a', debljina2:5})}}
                style={{width:'50%', borderBottomColor:stil.boja2, borderBottomWidth:stil.debljina2}}
                >
                    <View>
                        <Text style={styles.txtFont}> ON HOLD</Text>    
                    </View>                
                </TouchableOpacity>
            </View>
        </View>
        {active 
        ?<CpsActive 
        search={search}
        allCards={allCards}
        getCards={getCards}
        />
        :<CpsHold 
        search={search}
        allCards={allCards}
        getCards={getCards}
        />
        }
    </ScrollView>
    ) 
}

const styles = StyleSheet.create({
    txtFont:{
        color:'#49a43a',
        fontSize:28, 
        alignSelf:'center',
        fontFamily:'Myriad-Pro-Bold-Condensed',
        paddingBottom:12
    },
    txtViewStyle:{
        flexDirection:'row',
        
    },
    txtStyle:{
        fontSize:22,
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848'
    },
    btn:{
        width:'45%',
        alignSelf:'center',
        backgroundColor:'#49a43a',
        borderRadius:10,
        marginTop:15
    },
    podatci:{
        fontSize:18,
        color:'#484848',
        alignSelf:'center',
        
    }
});

export default CpsActiveHold ;

