import React from 'react';
import { Text, View, TextInput, StyleSheet } from 'react-native';

const ITAInput = ({ placeholder, onChangeText, value, secure}) => {
    return(
        <View style={styles.btnContainer}>
            <TextInput
            value={value}
            placeholder={placeholder} 
            onChangeText={onChangeText}
            style={{padding:10, fontFamily:'Myriad-Pro-Condensed', fontSize:20}}
            placeholderTextColor='#20316b'
            secureTextEntry={secure}
            />
        </View>
    )
};

const styles = StyleSheet.create({
    btnContainer:{
        borderWidth:2,
        width:'80%',
        marginTop:25,
        borderRadius:10,
        borderColor:'#20316b'
    }

})

export default ITAInput;