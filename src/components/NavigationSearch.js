import React, { useState } from 'react';
import { Text, View, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { Feather, Entypo, FontAwesome } from '@expo/vector-icons';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Ionicons } from '@expo/vector-icons'; 

const NavigationSearch = ({ navigation }) => {

    const [filter,setFilter] = useState(false)

    return(
        <View style={styles.btnContainer}>
            <View style={styles.searchContainer}>
                <Feather 
                size={30} 
                name='search'
                style={{color:'#868686', marginHorizontal:10}}
                />
                <TextInput 
                placeholder='Current location' 
                style={{width:'60%'}}
                />
                    <View style={{width:'25%', height:'90%', flexDirection:'row-reverse'}}>
                        <TouchableOpacity 
                        onPress={() => !filter?setFilter(true): setFilter(false)}
                        >
                        <View style={{backgroundColor:'#cbae00', flex:1, justifyContent:'center', paddingHorizontal:8, borderBottomRightRadius:5, borderTopRightRadius:5}}><Entypo name='location-pin' size={30} style={{color:'white', alignItems:'center'}} /></View> 
                                </TouchableOpacity>
                        <TouchableOpacity>
                        <View style={{backgroundColor:'#868686', flex:1, justifyContent:'center', paddingHorizontal:12, borderBottomLeftRadius:5, borderTopLeftRadius:5}}><FontAwesome name='bars' size={30} style={{color:'white'}} /></View>
                        </TouchableOpacity>
                    </View>
            </View>
            {filter
            ?<View style={{width:'100%', flexDirection:'row', height:'30%'}}>
                <View style={{...styles.filterView, width:'20%'}}>
                    <Ionicons name="ios-arrow-down" size={wp('6%')} color="#cbae00" />
                    <Text style={styles.filterTxt}>Parking</Text>
                </View>
                <View style={{...styles.filterView, width:'15%'}}>
                    <Ionicons name="ios-arrow-down" size={wp('6%')} color="#cbae00" />
                    <Text style={styles.filterTxt}>Fuel</Text>
                </View>
                <View style={{...styles.filterView, width:'20%'}}>
                    <Ionicons name="ios-arrow-down" size={wp('6%')} color="#cbae00" />
                    <Text style={styles.filterTxt}>Amenities</Text>
                </View>
                <View style={{...styles.filterView, width:'17%'}}>
                    <Ionicons name="ios-arrow-down" size={wp('6%')} color="#cbae00" />
                    <Text style={styles.filterTxt}>Radius</Text>
                </View>
                <TouchableOpacity 
                onPress={() => navigation.navigate('Locations')}
                style={{...styles.filterView, width:'28%'}}>
                    <Ionicons name="ios-arrow-down" size={wp('6%')} color="#cbae00" />
                    <Text style={styles.filterTxt}>Location Type</Text>  
                </TouchableOpacity>
            </View>
            :null
            
            }
            

        </View>
    )
}

const styles = StyleSheet.create({
    btnContainer:{
        flexDirection:'column',
        alignItems:'center',
        width:'90%', 
        height:hp('10%'), 
        marginTop:hp('2.3%')
        },
    searchContainer:{
        width:'100%', 
        flexDirection:'row', 
        height:'70%', 
        alignItems:'center',
        borderColor:'white', 
        borderWidth:3,
        borderRadius:5,
        backgroundColor:'white',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.57,
        shadowRadius: 16,
        elevation: 24,
        borderColor:'#d8d8d8',
        borderWidth:0.2,   
        
    },
    filterView:{
        flexDirection:'row',
        alignItems:'center',
        marginVertical:0,
        justifyContent:'space-evenly',
        borderRadius:6,
        borderTopLeftRadius:0,
        borderTopRightRadius:0,
        backgroundColor:'#ececec',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.57,
        shadowRadius: 16,
        elevation: 24,
        borderColor:'white',
        borderWidth:0.2,   
    },
    filterTxt:{
        fontSize:wp('3.5%'),
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848'
    }
});

export default NavigationSearch;