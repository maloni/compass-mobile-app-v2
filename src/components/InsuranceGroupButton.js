import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';

const InsuranceGroupButton = ({ title, navigation }) => {
    return(
        <View style={{width:'80%', height:'7%', marginTop:10}}>
            <TouchableOpacity onPress={navigation}>
                <View style={{width:'100%', height:'100%', backgroundColor:'#cead02', borderRadius:10, justifyContent:'center'}}>
                    <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', color:'white', alignSelf:'center', fontSize:24}}>{title}</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({

});

export default InsuranceGroupButton;