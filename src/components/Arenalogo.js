import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


const Arenalogo = () => {
    return(
        <View style={{height:hp('18%'), alignSelf:'center', width:wp('80%')}}>
            <Image style={{width:'100%', height:'80%'}} resizeMode='contain' source={require('../../src/img/logo-test.png')} />
        </View>
    )
};

const styles = StyleSheet.create({

});

export default Arenalogo;