import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


const TrailerList = ({ title, imge, desc, nav }) => {
    

    return(
        <View style={{alignItems:'center', borderWidth:3, borderRadius:15, borderColor:'#da212f', padding:wp('2%'), marginTop:hp('2%')}}>
            <Text style={styles.title}>{title}</Text>
                <Image style={{width:wp('85%'), padding:wp('1%')}} source={imge} resizeMode='contain'/>
            <Text style={styles.desc}>{desc}</Text>
            <TouchableOpacity
            style={styles.btn}
            onPress={nav}
            >
                <Text style={{...styles.title, fontSize:wp('5%')}}>
                    Contact Us About Leasing This Trailer!
                </Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    title:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:wp('6%'),
        alignSelf:'center',
        textAlign:'justify'

    },
    desc:{
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848',
        fontSize:wp('4%'),
        alignSelf:'center',
        textAlign:'justify'
    },
    btn:{
        justifyContent:'center',
        paddingVertical:wp('2%'),
        marginVertical:hp('2%'),
        width:'90%',
        alignSelf:'center',
        borderWidth:2, 
        borderColor:'#da212f', 
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
        width: 10,
        height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'#484848',
        elevation: 8,
        
    }
});

export default TrailerList;