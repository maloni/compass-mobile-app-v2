import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, TextInput, RefreshControl} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


const CFSInvoiceInput = ({ value, setValue , title, placeholder, onFocus }) => {

    return(
           <View style={styles.container}> 
                <Text
                style={{
                    color:'#484848',
                    fontFamily:'Myriad-Pro-Bold-Condensed',
                    fontSize:24,
                    paddingLeft:wp('2%')
                }}
                >{title}</Text>
                <TextInput 
                placeholder={placeholder}
                value={value}
                onChangeText={setValue}
                style={{padding:3, fontSize:24, width:'70%', fontFamily:'Myriad-Pro-Condensed'}}
                onFocus={onFocus}
                />
            
            </View>
            
    )
    
};

styles = StyleSheet.create({
    container:{
        width:'100%',
        borderBottomWidth:2,
        borderBottomColor:'#cdcdcd',
        flexDirection:'row',
        marginVertical:hp('1%'),
        alignItems:'center'
    }
});

export default CFSInvoiceInput