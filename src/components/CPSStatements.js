import React, { useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { PulseIndicator } from 'react-native-indicators';
import * as Font from 'expo-font';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Constants from 'expo-constants';
import { FlatList } from 'react-native-gesture-handler';
import Axios from 'axios';


const CpsStatement = ({ result, dateFromSend, dateToSend, cpsUserOK }) =>{
    
    const[active, setActive] = useState(true);
    const[stil, setStil] = useState({boja1:'#49a43a', debljina1:5, boja2:'#e5e5e6', debljina2:3});

    const summary = () => {
        let filter = {carrierId:cpsUserOK,tdate:{ $gte:dateFromSend, $lte:dateToSend}};
        var options ={fields:"fuel cash info carrierCashAdvanceFee carrierFee oil other"};
        let query = "filter=" + encodeURIComponent(JSON.stringify(filter))+"&options="+encodeURIComponent(JSON.stringify(options));
        Axios.post('https://apps.compassaws.net/api/card/getTransactions', query)
        .then(res => {
            console.log(res)
        })
        .catch(err => {
            console.log(err)
        })
    }

    return(
        <View style={{width:'100%', height:'100%'}}>
            <View style={{flexDirection:'row', width:'100%'}}> 
            <View style={{flex:1, flexDirection:'row', marginTop:15}}>
                <TouchableOpacity
                onPress={() => {setActive(true), setStil({boja1:'#49a43a', debljina1:5, boja2:'#e5e5e6', debljina2:3})}}
                style={{width:'50%', borderBottomWidth:stil.debljina1, borderBottomColor:stil.boja1}}
                >
                    <View >
                        <Text style={styles.txtFont}>STATEMENTS</Text>    
                    </View>                
                </TouchableOpacity>
                <TouchableOpacity
                onPress={() => {setActive(false), setStil({boja1:'#e5e5e6', debljina1:3, boja2:'#49a43a', debljina2:5}), summary()}}
                style={{width:'50%', borderBottomColor:stil.boja2, borderBottomWidth:stil.debljina2}}
                >
                    <View>
                        <Text style={styles.txtFont}>BY UNIT NUMBER</Text>    
                    </View>                
                </TouchableOpacity>
            </View>
        </View>
        
        {active
        ? <CpsStatements 
        result={result}
        />
        : <CpsByUnitNumber />
        }
        </View>
    )
}

const CpsStatements = ({ result }) => {


    return(
        <View style={{width:'100%', height:'100%', paddingBottom:hp('40%')}}>
            <FlatList
            data={result}
            keyExtractor={(item) => item._id}
            renderItem={({ item }) => {
                return(
                    <View style={{width:'100%', paddingBottom:hp('7%'), marginVertical:hp('1%'), backgroundColor:'#ececec', padding:wp('3%'), borderRadius:10}}>
                        <View style={{borderBottomWidth:3, borderBottomColor:'#CCCCCC', marginVertical:12, paddingBottom:12}}>
                        <View style={{flexDirection:'row'}}><Text style={styles.txt}>Date :</Text><Text style={styles.podatci}> {item.stmtDate} </Text></View>
                            <View style={{width:120, height:120, alignSelf:'center', marginVertical:10}}>
                                <View style={{borderRadius:200, backgroundColor:'#49a43a', width:120, height:120, alignSelf:'center', justifyContent:'center'}}>
                                    <View style={{borderRadius:200, backgroundColor:'white', width:100, height:100, alignSelf:'center'}}>
                                        <View style={{justifyContent:'center', alignSelf:'center'}}>
                                            <Text style={{alignSelf:'center', fontSize:70, fontFamily:'Myriad-Pro-Bold-Condensed', color:'#484848', height:'70%'}}>{item.transactionCount}</Text>
                                            <Text style={{alignSelf:'center', paddingBottom:25, fontFamily:'Myriad-Pro-Bold-Condensed', color:'#484848'}}>Transactions</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        <View style={{flexDirection:'row'}}><Text style={styles.txt}>Fuel Amount:</Text><Text style={styles.podatci}> $ {item.fuel/100}</Text></View>
                        <View style={{flexDirection:'row'}}><Text style={styles.txt}>Transaction Fee:</Text><Text style={styles.podatci}> $ {item.cardGroupTransactionFees/100}</Text></View>
                        <View style={{flexDirection:'row'}}><Text style={styles.txt}>Cash Amount:</Text><Text style={styles.podatci}> $ {item.cash}</Text></View>
                        <View style={{flexDirection:'row'}}><Text style={styles.txt}>Cash Fee:</Text><Text style={styles.podatci}> $ {item.cashAdvanceFees}</Text></View>
                        </View>
                        <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:22, alignSelf:'center', color:'#484848'}}>AMOUNT DUE: ${item.amount/100 }</Text>
                        <View style={{width:'70%', backgroundColor:'#49a43a', borderRadius:10, paddingVertical:10, paddingHorizontal:15, alignSelf:'center', marginTop:15}}>
                            <TouchableOpacity>
                                <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', color:'white', fontSize:24, alignSelf:'center', marginBottom:0}}>SEND STATEMENT</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                )
            }}
            />
           
        </View>
    )
}

const CpsByUnitNumber = () =>{
    return(
        <View>
            <Text>By Unit Number</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    txtFont:{
        color:'#49a43a',
        fontSize:24, 
        alignSelf:'center',
        fontFamily:'Myriad-Pro-Bold-Condensed',
        paddingBottom:12
    },
    txtViewStyle:{
        flexDirection:'row',
        
    },
    txtStyle:{
        fontSize:22,
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848'
    },
    btn:{
        width:'45%',
        alignSelf:'center',
        backgroundColor:'#49a43a',
        borderRadius:10,
        marginTop:7
    },
    txt:{
        fontSize:20,
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848'
    },
    txt1:{
        fontSize:17,
        color:'#484848'
    },
    podatci:{
        fontSize:16,
        color:'#484848',
        alignSelf:'center',
        fontWeight:'200'
    }
});

export default CpsStatement;