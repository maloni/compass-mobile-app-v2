import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { ProgressBar } from 'react-native-paper'
const BalanceBar = ({ balance, minBalance, maxBalance, currentBalance }) => {

    console.log(1/maxBalance*currentBalance)
    return(
        <View>
            <Text style={{...styles.txt, alignSelf:'center'}}>{balance}</Text>
                <ProgressBar 
                style={styles.progressBar}
                progress={1/maxBalance*currentBalance}
                color='#49a43a'
                />
                <View style={styles.balance}>
                    <Text style={{...styles.txt, flex:1}}>$ {minBalance}</Text>
                    <Text style={styles.curentBalance}>$ {currentBalance}</Text>
                    <Text style={{...styles.txt, flex:1, textAlign:'right'}}>$ {maxBalance}</Text>
                </View>
        </View>
    )
};
// marko Test
const styles = StyleSheet.create({
    container:{
        marginTop:50,
        margin:20
    },
    progressBar:{
        height:20,
        backgroundColor:'#e6e6ea',
        borderRadius:10
    },
    balance:{
        flexDirection:'row',
        width:'100%'
    },
    txt:{
        color:'#8c8c8c',
        fontSize:16,
    },
    curentBalance:{
        fontSize:40,
        color:'#49a43a',
        fontWeight:'bold',
    }
});

export default BalanceBar;