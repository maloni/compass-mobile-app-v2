import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


const CFSButton = ({ navi, title }) => {

    return(
           <View style={styles.container}> 
                    <TouchableOpacity 
                    style={{...styles.btn}} 
                    onPress={navi}>
                            <Text style={styles.txt}>{title}</Text>
                    </TouchableOpacity>
            </View>
            
    )
    
};




const styles = StyleSheet.create({
    container: {
        width:wp('80%'),
        marginHorizontal:'5%',
    },
        btn:{
        marginVertical:hp('1%'),
        width:'100%',
        alignSelf:'center',
        borderWidth:1.5, 
        borderColor:'#cead02', 
        borderRadius:10,
        backgroundColor:'white',
        shadowOffset: {
          width: 10,
          height: 6,
        },
        shadowOpacity: 0.30,
        shadowRadius: 12,
        shadowColor:'black',
        elevation: 3,
        
    },
    txt: {
        fontFamily:'Myriad-Pro-Bold-Condensed', 
        alignSelf:'center', 
        fontSize:26 , 
        color:'#484848', 
        padding:hp('1%')
    }
      
});

export default CFSButton;