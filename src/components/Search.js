import React, { useState } from 'react';
import { Text, View, StyleSheet, Image, TextInput } from 'react-native';
import {Feather} from '@expo/vector-icons';

const SearchBar = ({ Navigation , vrednost, postaviVrednost}) =>{

    return(
            <View style={styles.btnContainer}>
                <Feather 
                    name='search' 
                    size={30} 
                    style={{color:'#d8d8d8', padding:8}} />
                <TextInput 
                    placeholder='Filter by Unit Number'
                    value={vrednost}
                    onChangeText={postaviVrednost}
                />
                
            </View>
    )
};

const styles = StyleSheet.create({
    btnContainer:{
        flexDirection:'row',
        alignItems:'center',
        width:'80%', 
        height:50, 
        borderColor:'white', 
        borderWidth:3,
        marginVertical:20,
        borderRadius:5,
        backgroundColor:'white',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.57,
        shadowRadius: 16,
        shadowColor:'green',

        elevation: 24,
        borderColor:'#d8d8d8',
        borderWidth:0.2,    
        },
});

export default SearchBar;