import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { PulseIndicator } from 'react-native-indicators';
import * as Font from 'expo-font';


const CashAdvance = () =>{
    
        
    const[active, setActive] = useState(true);
    const[stil, setStil] = useState({boja1:'#49a43a', debljina1:5, boja2:'#e5e5e6', debljina2:3});

    return(
        <View>
            
            <View style={{flexDirection:'row', width:'90%'}}> 
            <View style={{flex:1, flexDirection:'row', marginTop:15}}>
                <TouchableOpacity
                onPress={() => {setActive(true), setStil({boja1:'#49a43a', debljina1:5, boja2:'#e5e5e6', debljina2:3})}}
                style={{width:'50%', borderBottomWidth:stil.debljina1, borderBottomColor:stil.boja1}}
                >
                    <View >
                        <Text style={styles.txtFont}>Active</Text>    
                    </View>                
                </TouchableOpacity>
                <TouchableOpacity
                onPress={() => {setActive(false), setStil({boja1:'#e5e5e6', debljina1:3, boja2:'#49a43a', debljina2:5})}}
                style={{width:'50%', borderBottomColor:stil.boja2, borderBottomWidth:stil.debljina2}}
                >
                    <View>
                        <Text style={styles.txtFont}>History</Text>    
                    </View>                
                </TouchableOpacity>
            </View>
        </View>
        
        {active
        ? <CashAdvanceActive />
        : <CashAdvanceHistory />
        }
        </View>
    )
}

const CashAdvanceActive = () => {

    return(
        <View style={{borderBottomWidth:5, borderBottomColor:'#CCCCCC', marginVertical:12, paddingBottom:12, flexDirection:'row'}}>
            <View >
                <View style={{flexDirection:'row'}}><Text style={styles.txt}>Card Number :</Text><Text style={{fontSize:18, alignSelf:'center',color:'#484848'}}> xxxxxx1000091</Text></View>
                <View style={{flexDirection:'row'}}><Text style={styles.txt}>Driver :</Text><Text style={{fontSize:18, alignSelf:'center',color:'#484848'}}> Kristina Trnkovska</Text></View>
                <View style={{flexDirection:'row'}}><Text style={styles.txt}>UnitNumber :</Text><Text style={{fontSize:18, alignSelf:'center',color:'#484848'}}> 105</Text></View>
                <View style={{flexDirection:'row'}}><Text style={{...styles.txt, marginTop:15}}>Amount: :</Text><Text style={{fontSize:18,alignSelf:'center',color:'#484848',marginTop:15}}> $50.00</Text></View>
                <View style={{flexDirection:'row'}}><Text style={styles.txt}>Reason :</Text><Text style={{fontSize:18, alignSelf:'center',color:'#484848'}}> Lumper Recepit</Text></View>
            </View>
            <View style={{paddingTop:0, paddingRight:0}}>
                <View style={{...styles.dugme, backgroundColor:'#49a43a'}}>
                    <TouchableOpacity>
                        <Text  numberOfLines={1} style={{color:'white', fontSize:24, padding:5, paddingHorizontal:7,  alignSelf:'center', fontFamily:'Myriad-Pro-Bold-Condensed'}}>Approve</Text>
                    </TouchableOpacity>
                </View>
                <View style={{...styles.dugme, backgroundColor:'red'}}>
                    <TouchableOpacity>
                        <Text numberOfLines={1} style={{color:'white', fontSize:24,padding:5, paddingHorizontal:7,  alignSelf:'center', fontFamily:'Myriad-Pro-Bold-Condensed'}}>Reject</Text>
                    </TouchableOpacity>
                </View>
                <View style={{...styles.dugme, backgroundColor:'#ffdc42'}}>
                    <TouchableOpacity>
                        <Text numberOfLines={1} style={{color:'white', fontSize:24, padding:5, paddingHorizontal:7,  alignSelf:'center', fontFamily:'Myriad-Pro-Bold-Condensed'}}>Modify</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const CashAdvanceHistory = () =>{
    return(
        <View style={{borderBottomWidth:5, borderBottomColor:'#CCCCCC', marginVertical:12, paddingBottom:12}}>
            <View style={{flexDirection:'row'}}><Text style={styles.txt}>Card Number :</Text><Text style={{fontSize:18, alignSelf:'center',color:'#484848'}}> xxxxxx1000091</Text></View>
            <View style={{flexDirection:'row'}}><Text style={styles.txt}>Driver :</Text><Text style={{fontSize:18, alignSelf:'center',color:'#484848'}}> Kristina Trnkovska</Text></View>
            <View style={{flexDirection:'row'}}><Text style={styles.txt}>UnitNumber :</Text><Text style={{fontSize:18, alignSelf:'center',color:'#484848'}}> 105</Text></View>
            <View style={{flexDirection:'row'}}><Text style={{...styles.txt, marginTop:15}}>Amount: :</Text><Text style={{fontSize:18,alignSelf:'center',color:'#484848',marginTop:15}}> $50.00</Text></View>
            <View style={{flexDirection:'row'}}><Text style={styles.txt}>Reason :</Text><Text style={{fontSize:18, alignSelf:'center',color:'#484848'}}> Lumper Recepit</Text></View>
        </View>
    )
}

const styles = StyleSheet.create({
    txtFont:{
        color:'#49a43a',
        fontSize:28, 
        alignSelf:'center',
        fontFamily:'Myriad-Pro-Bold-Condensed',
        paddingBottom:12
    },
    txtViewStyle:{
        flexDirection:'row',
        
    },
    txtStyle:{
        fontSize:22,
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848'
    },
    btn:{
        width:'45%',
        alignSelf:'center',
        backgroundColor:'#49a43a',
        borderRadius:10,
        marginTop:7
    },
    txt:{
        fontSize:24,
        fontFamily:'Myriad-Pro-Bold-Condensed',
        color:'#484848'
    },
    txt1:{
        fontSize:17,
        color:'#484848'
    },
    dugme:{
        width:'100%',
        borderRadius:5,
        marginVertical:5,
        
    }
});

export default CashAdvance;