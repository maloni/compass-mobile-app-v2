import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { Entypo } from '@expo/vector-icons';

const DrawerOptions = ({ title, navigacija }) => {

    return(
        <View style={styles.container}>
            <TouchableOpacity
            onPress={navigacija}
            >
                <View style={styles.list}>
                    <View style={styles.title}>
                        <Text style={styles.txt}>{title}</Text>
                    </View>
                    <View>
                        <Entypo 
                            name='chevron-right'
                            color='#484848'
                            size={60}
                        />
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    )
};

const styles = StyleSheet.create({
    container:{
        borderTopWidth:6,
        borderTopColor:'#eaeaea'
    },
    list:{
        flexDirection:'row',
        width:'90%',
        alignItems:'center'
        
    },
    title: {
        width:'90%'
    },
    txt:{
        fontSize:22,
        color:'#484848',
        fontFamily:'Myriad-Pro-Bold-Condensed'
    }
});

export default DrawerOptions;