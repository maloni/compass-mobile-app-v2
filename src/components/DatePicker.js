import React, { useState } from "react";
import { View, TouchableOpacity, Text } from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";
 
const DatePicker = ({ title }) => {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [datum, setDatum] = useState()

  console.log(datum)

  return (
    <View style={{borderWidth:1, marginHorizontal:'4%', borderColor:'#969696'}}>
      <TouchableOpacity
        onPress={() => setDatePickerVisibility(true)}
      >
        <Text style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:20, padding:10, color:'#969696'}}>{title}</Text>
      </TouchableOpacity>
          <DateTimePickerModal
            isVisible={isDatePickerVisible}
            mode={'date'}
            onConfirm={(date) => {setDatum(date), setDatePickerVisibility(false)}}
            onCancel={() => setDatePickerVisibility(false)}
            isDarkModeEnabled={true}
          />
    </View>
  );
};
 
export default DatePicker;



