import React from 'react';
import { View, StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import { Entypo, MaterialIcons } from '@expo/vector-icons';

const TripPlanSearch = ({ vrednost, postaviVrednost, boja, nav, placeholder}) => {

    return(
            <View style={styles.btnContainer}>
                <View style={{width:'90%', height:'100%', flexDirection:'row', alignItems:'center'}}>
                <Entypo 
                    name='location-pin' 
                    size={40} 
                    style={{color:boja}} />
                <TextInput 
                    placeholder={placeholder}
                    value={vrednost}
                    onChangeText={postaviVrednost}
                    style={{fontFamily:'Myriad-Pro-Bold-Condensed', fontSize:20}}
                />
                </View>
                {nav
                ?<TouchableOpacity style={{paddingRight:5}}>
                    <MaterialIcons
                    name='navigation'
                    size={30}
                    style={{color:'#00BFFF'}}
                    />
                </TouchableOpacity>
                :null
                }
            </View>
    )
};

const styles = StyleSheet.create({
    btnContainer:{
        flexDirection:'row',
        alignItems:'center',
        width:'80%', 
        height:50, 
        borderColor:'white', 
        borderWidth:3,
        marginVertical:10,
        borderRadius:5,
        backgroundColor:'white',
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.57,
        shadowRadius: 16,
        shadowColor:'green',

        elevation: 24,
        borderColor:'#d8d8d8',
        borderWidth:0.2,  
        },
});

export default TripPlanSearch;